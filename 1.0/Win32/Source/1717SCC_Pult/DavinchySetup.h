#pragma once
#include <RayanceFunctions.h>
#include "PultConfig.h"
#include <string>
using namespace std;
namespace Roentgepropm {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// ������ ��� DavinchySetup
	/// </summary>
	public ref class DavinchySetup : public System::Windows::Forms::Form
	{
	public:
		DavinchySetup(void)
		{
			InitializeComponent();
			ApplicationConfig = new PultConfig();
			if (ApplicationConfig->Open())
			{
				ApplicationConfig->ReadConfig();
				DavinchyFolder->Text = Marshal::PtrToStringAnsi((IntPtr) ApplicationConfig->GetPDavinchyPath());


			}

			//
			//TODO: �������� ��� ������������
			//
		}



		virtual void WndProc(Message% m) override
		{

			switch (m.Msg)
			{

			case VADAV_MessageCode_ERR:
			{
				MessageBox::Show("������ " + m.WParam.ToString());
				break;
			}

			default:
				break;
			}
			Form::WndProc(m);

		}


	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~DavinchySetup()
		{
			
			if (ApplicationConfig)
			{
				delete ApplicationConfig;
					 
			}


			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::Button^  WriteToConfig;
    protected: 

    private: System::Windows::Forms::TextBox^  DavinchyFolder;

    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::FolderBrowserDialog^  DavinchyFolderSelector;
    protected: 

	private:
		PultConfig* ApplicationConfig;
		String^ PathM;

		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->WriteToConfig = (gcnew System::Windows::Forms::Button());
			this->DavinchyFolder = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->DavinchyFolderSelector = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->SuspendLayout();
			// 
			// WriteToConfig
			// 
			this->WriteToConfig->Location = System::Drawing::Point(12, 109);
			this->WriteToConfig->Name = L"WriteToConfig";
			this->WriteToConfig->Size = System::Drawing::Size(75, 23);
			this->WriteToConfig->TabIndex = 0;
			this->WriteToConfig->Text = L"WriteToConfig";
			this->WriteToConfig->UseVisualStyleBackColor = true;
			this->WriteToConfig->Click += gcnew System::EventHandler(this, &DavinchySetup::WriteToConfig_Click);
			// 
			// DavinchyFolder
			// 
			this->DavinchyFolder->Location = System::Drawing::Point(12, 70);
			this->DavinchyFolder->Name = L"DavinchyFolder";
			this->DavinchyFolder->ReadOnly = true;
			this->DavinchyFolder->Size = System::Drawing::Size(180, 20);
			this->DavinchyFolder->TabIndex = 1;
			this->DavinchyFolder->Click += gcnew System::EventHandler(this, &DavinchySetup::DavinchyFolder_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 54);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(124, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"���� � ����� Davinchy]";
			// 
			// DavinchySetup
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(204, 157);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->DavinchyFolder);
			this->Controls->Add(this->WriteToConfig);
			this->Name = L"DavinchySetup";
			this->Text = L"DavinchySetup";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		


    private: System::Void DavinchyFolder_Click(System::Object^  sender, System::EventArgs^  e) {

		DavinchyFolderSelector->ShowDialog();
	    PathM = DavinchyFolderSelector->SelectedPath;
		DavinchyFolder->Text = PathM;

             }
    private: System::Void WriteToConfig_Click(System::Object^  sender, System::EventArgs^  e) {
		int W, H;

		char* Path = (char*)Marshal::StringToHGlobalAnsi(PathM).ToPointer();
		ApplicationConfig->SetPDavinchyPath(Path);
		ApplicationConfig->WriteConfig();
		Application::Restart();
             }
    };
}
