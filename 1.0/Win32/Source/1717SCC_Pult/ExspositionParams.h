#pragma once
#include "PultConfig.h"
namespace Roentgepropm {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� ExspositionParams
	/// </summary>
	public ref class ExspositionParams : public System::Windows::Forms::Form
	{
	public:
		ExspositionParams(void)
		{
			InitializeComponent();
			Conf = new PultConfig();

			if (Conf->Open())
			{
				Conf->ReadConfig();
				CoffCurent->Text = Conf->GetPCoffAnnodeCurrent().ToString();
				CoffHV->Text = Conf->GetPCoffHV().ToString();
				CoffTime->Text = Conf->GetPCoffTime().ToString();
			

			}

			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		PultConfig* Conf;
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~ExspositionParams()
		{
			if (components)
			{
				delete components;
			}

			if (Conf)
			{
				delete Conf;

			}


		}
    private: System::Windows::Forms::Button^  button1;
    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Label^  label2;
    private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  CoffTime;
	private: System::Windows::Forms::TextBox^  CoffHV;
	private: System::Windows::Forms::TextBox^  CoffCurent;



    protected: 


	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->CoffTime = (gcnew System::Windows::Forms::TextBox());
			this->CoffHV = (gcnew System::Windows::Forms::TextBox());
			this->CoffCurent = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 171);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"���������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &ExspositionParams::button1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(23, 52);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(40, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"�����";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(23, 83);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(115, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"������� ����������";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(23, 118);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(75, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"�������  ���";
			// 
			// CoffTime
			// 
			this->CoffTime->Location = System::Drawing::Point(167, 52);
			this->CoffTime->Name = L"CoffTime";
			this->CoffTime->Size = System::Drawing::Size(100, 20);
			this->CoffTime->TabIndex = 5;
			// 
			// CoffHV
			// 
			this->CoffHV->Location = System::Drawing::Point(167, 83);
			this->CoffHV->Name = L"CoffHV";
			this->CoffHV->Size = System::Drawing::Size(100, 20);
			this->CoffHV->TabIndex = 6;
			// 
			// CoffCurent
			// 
			this->CoffCurent->Location = System::Drawing::Point(167, 115);
			this->CoffCurent->Name = L"CoffCurent";
			this->CoffCurent->Size = System::Drawing::Size(100, 20);
			this->CoffCurent->TabIndex = 7;
			// 
			// ExspositionParams
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(333, 214);
			this->Controls->Add(this->CoffCurent);
			this->Controls->Add(this->CoffHV);
			this->Controls->Add(this->CoffTime);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->Name = L"ExspositionParams";
			this->Text = L"��������� ����������";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {


		float vCoffCurrent, vCoffHv,vCoffTime;
		float::TryParse(CoffCurent->Text, vCoffCurrent);
		float::TryParse(CoffHV->Text,vCoffHv);
		float::TryParse(CoffTime->Text, vCoffTime);
		Conf->SetPCoffAnnodeCurrent(&vCoffCurrent);
		Conf->SetPCoffHV(&vCoffHv);
		Conf->SetPCoffTime(&vCoffTime);
		Conf->WriteConfig();





	}
};
}
