﻿========================================================================
    ПРИЛОЖЕНИЕ : Обзор проекта KY5_Control
========================================================================

Это приложение KY5_Control создано автоматически с помощью мастера 
приложений.  

Здесь приведены краткие сведения о содержимом каждого из файлов, использованных 
при создании приложения KY5_Control.

KY5_Control.vcxproj
    Это основной файл проекта VC++, автоматически создаваемый с помощью мастера 
    приложений. 
    Он содержит данные о версии языка Visual C++, использованной для создания 
    файла, а также сведения о платформах, настройках и свойствах проекта, 
    выбранных с помощью мастера приложений.

KY5_Control.vcxproj.filters
    Это файл фильтров для проектов VC++, созданный с помощью мастера 
    приложений. 
    Он содержит сведения о сопоставлениях между файлами в вашем проекте и 
    фильтрами. Эти сопоставления используются в среде IDE для группировки 
    файлов с одинаковыми расширениями в одном узле (например файлы ".cpp" 
    сопоставляются с фильтром "Исходные файлы").

KY5_Control.cpp
    Это основной исходный файл приложения.
    Код для отображения формы.

Form1.h
    Реализация класса формы и функции InitializeComponent().

AssemblyInfo.cpp
    Пользовательские атрибуты для изменения метаданных сборки.

/////////////////////////////////////////////////////////////////////////////
Другие стандартные файлы:

StdAfx.h, StdAfx.cpp
    Эти файлы используются для построения файла предкомпилированного заголовка 
    (PCH) с именем KY5_Control.pch и файла предкомпилированных типов 
    с именем StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
