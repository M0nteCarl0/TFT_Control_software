

//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 29.11.04
//    ver. 2.00
//-----------------------------------------------------------------------------

#ifndef __CCD_CALIBRATE_H__
#define __CCD_CALIBRATE_H__

#pragma warning(disable:4786)

#include "CCD_ImgFile.h"
#include "CCD_FitStruct.h"
#include "Posiz_Rect.h"
#include <time.h>
#include <map>

using namespace std ;

class   CCD_Calibrate : public CCD_Object
{
	public:
		int m_FragmNum;
		Posix_Rect m_RectMean;

		typedef map <double,CCD_ImgFile*> MAP_DOUB_IMGFILE;
		typedef map <double,CCD_FitStruct*> MAP_DOUB_FITSTRUCT;

		MAP_DOUB_IMGFILE m_ImgFileMap;
		MAP_DOUB_FITSTRUCT m_FitStructMap;

		CCD_FitStruct m_FitStruct;

		bool m_FlgINL;

	public:
		CCD_Calibrate();

		virtual ~CCD_Calibrate();
      
	public:
		void Clear();

	public:
		int GetNumPoints() { return m_ImgFileMap.size(); }
		void SetImageSize(int X0,int Y0,int Width,int Height);
                                                                
		void SetRectMean(int l,int t,int r,int b) { m_RectMean.SetRect(l, t, r, b); }
		void SetINL(bool EnableINL) { m_FlgINL = EnableINL; }

		int PreloadAllImages(char* ImgPath);
		int Calibrate(int Fokus,int Binning,float HV);
		int WriteFitStruct(char* NameFileCalibr);

		int PreLoadFitStructures(char* ImgPath);
		int Repair(WORD* AddrIn,float HV,int Binning,WORD* AddrOut,float* HVcalibr);
		int GetNoiseVersusAmpl(double* Ampl,double* Noise,int NumIn,int* NumOut,float HV);

		void PrintFitStructMap();
		void PrintImgFileMap();
		void ClearImgFileMap();
		void ClearFitStructMap();
	protected:
		

		bool FitPol2(double* X,double* Y,double* Er,int N,float* P0,float* P1);

		void SaveFitPar(int Points,double* X,double* Y,CCD_FitStruct::CFitParam* P,int J);

		
		void RepairImage(WORD* AddrIn,WORD* AddrOut,CCD_FitStruct* Ptr);
		void ImgInterpolation(WORD* AddrOut0,float HV0,WORD* AddrOut1,float HV1,WORD* AddrOut,float HV);
};

#endif 
