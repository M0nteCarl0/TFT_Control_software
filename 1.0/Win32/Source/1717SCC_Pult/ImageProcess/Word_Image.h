//-----------------------------------------------------------------------------
//    Novikov  V
//    date 28.01.2005
//    ver. 1.00
//-----------------------------------------------------------------------------
#ifndef __WORD_IMAGE__
#define __WORD_IMAGE__



//-----------------------------------------------------------------------------
#pragma warning(disable:4786)
#include <vector>
#include <Windows.h>
using namespace std ;
//-----------------------------------------------------------------------------
class  Word_Image
{
   public:
      enum {PIX_EMPTY = 0x0000, PIX_FULL = 0x7FFF};

      WORD   **m_Addr;
      int      m_Nx;
      int      m_Ny;
      bool     m_FlgAlias;
      
      Word_Image ();
      Word_Image (int Nx, int Ny);
      Word_Image (int Nx, int Ny, WORD *Addr);
      virtual ~Word_Image ();

      void Create (int Nx, int Ny, WORD *Addr = NULL);
      void Erase ();
      void Clear ();
      void Check ();

      void Copy      (Word_Image *Img);
      void CopyZoom  (int K, Word_Image *Img);
      void Invert    ();
      void Subtract     (Word_Image *Img);
      void SubtractAbs  (Word_Image *Img);
      void Add       (Word_Image *Img);
      void Add       (WORD Val);
      void Multiply  (double Val);
      void Binary    (WORD Level);
      void Eros      ();
      void CombineLabeles  (int Mask1, int Mask2, int Y);
      int  Labeling        ();

      void GetMeanRms (RECT *Rect, double *Mean, double *Rms);
      void GetMeanRms (double *Mean, double *Rms);
      WORD GetMax     ();
      WORD GetMin     ();
      double GetSumm    ();
      double GetSumm2   ();
      double GetAverage ();
      
      void GetImageNormMax (int *Nx, int *Ny, WORD *Addr);
      void PutImage (int  Nx, int  Ny, WORD *Addr);

      void Print ();
};
//-----------------------------------------------------------------------------
#endif