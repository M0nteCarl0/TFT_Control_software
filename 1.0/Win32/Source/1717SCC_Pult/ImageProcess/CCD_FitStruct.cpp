//#include "stdafx.h"
//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 29.11.04
//    ver. 2.00
//-----------------------------------------------------------------------------

#include "CCD_FitStruct.h"

#include <stdio.h>
#include <math.h>
//-----------------------------------------------------------------------------
CCD_FitStruct::CCD_FitStruct (): M_CALIBR_KEY (100)
{
   Constructor (1, 1);
};
//-----------------------------------------------------------------------------
CCD_FitStruct::CCD_FitStruct (int Width, int Height): M_CALIBR_KEY (100)
{
   Constructor (Width, Height);
};
//-----------------------------------------------------------------------------
void CCD_FitStruct::Constructor (int Width, int Height)
{
   m_Header.W  = Width;
   m_Header.H = Height;
   m_HF    = 0;

   memset (&m_Header, 0, sizeof (CFitHeader));
   m_Param = NULL;

   m_InlSumm  = new double [LENGTH_OF_WORD];
   m_InlNum   = new int    [LENGTH_OF_WORD];
   m_InlAmpl  = new int    [LENGTH_OF_WORD];
};
//-----------------------------------------------------------------------------
CCD_FitStruct::~CCD_FitStruct ()
{
   Close ();
   Clear ();

   delete [] m_InlSumm;
   delete [] m_InlNum ;
   delete [] m_InlAmpl;
};
//-----------------------------------------------------------------------------
void CCD_FitStruct::Clear ()
{
   m_Aver.Clear ();
   m_Header.Clear ();
   FreeMemory ();
};
//--------------------------------------------------------------------------
void CCD_FitStruct::AllocMemory ()
{
   FreeMemory ();

   m_Param = new CFitParam [m_Header.W * m_Header.H];
};
//--------------------------------------------------------------------------
void CCD_FitStruct::FreeMemory ()
{
   if (m_Param){
      delete [] m_Param;
      m_Param = NULL;
   }
};
//--------------------------------------------------------------------------
int  CCD_FitStruct::OpenRead	(char *Name)
{
   strcpy (m_FileName, Name);

   m_HF = CreateFile   (m_FileName, GENERIC_READ,
			    0,
			    NULL,
			    OPEN_EXISTING,
			    FILE_ATTRIBUTE_NORMAL,
			    NULL);
   if (INVALID_HANDLE_VALUE == m_HF){
      return OPEN_ER;
   }

   return OK;
};
//--------------------------------------------------------------------------
int  CCD_FitStruct::OpenWrite  (char *Name)
{
   strcpy (m_FileName, Name);

   m_HF = CreateFile   (m_FileName, GENERIC_READ | GENERIC_WRITE,
			    0,
			    NULL,
			    CREATE_ALWAYS,
			    FILE_ATTRIBUTE_NORMAL,
			    NULL);
   if (INVALID_HANDLE_VALUE == m_HF){
      return OPEN_ER;
   }

   return OK;
};
//--------------------------------------------------------------------------
void CCD_FitStruct::Close ()
{
   if (m_HF){
      CloseHandle (m_HF);
      m_HF          = 0;
   }
};
//--------------------------------------------------------------------------
int CCD_FitStruct::ReadHeader ()
{
   SetFilePointer (m_HF, 0, NULL, FILE_BEGIN);

   unsigned long   Rev;
   ReadFile (m_HF, &m_Header,  sizeof (CFitHeader), &Rev, NULL);

   if (!(Rev == sizeof (CFitHeader) && m_Header.Key == M_CALIBR_KEY))
        return ER;
                                                                        //--- Average
   int R = m_Aver.Read (m_HF);
   if (R) 
      return RW_ER;
                                                                        //--- Average
   return R;
};
//--------------------------------------------------------------------------
void CCD_FitStruct::GetHeaderHW (int *Width, int * Height)
{
   *Width   = m_Header.W;
   *Height  = m_Header.H;
};
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int CCD_FitStruct::Write ()
{
   if (!m_Param)     return ER;
   if (!m_HF)        return ER;

   int Ret = OK;
   int R;

   m_Header.Key  = M_CALIBR_KEY;
//   m_Header.W    = m_Header.W;
//   m_Header.H    = m_Header.H;
   int Len;
                                                                        //--- Header of FitStruc
   unsigned long Rev;
   WriteFile (m_HF, &m_Header, sizeof (CFitHeader), &Rev, NULL);
   if (Rev != sizeof (CFitHeader)){ Ret = RW_ER; goto _Error;}

                                                                        //--- Average
   R = m_Aver.Write (m_HF);
   if (R){ Ret = RW_ER; goto _Error;}
                                                                        //--- Fiting Parameters of FitStruc

   Len = m_Header.W * m_Header.H * sizeof(CFitParam);
   WriteFile (m_HF, m_Param, Len, &Rev, NULL);
   if (Rev != Len){ Ret = RW_ER; goto _Error;}

_Error:

   return Ret;
};
//-----------------------------------------------------------------------------
int CCD_FitStruct::Write (char *NameFileCalibr)
{
   int Ret = OK;

   char drive[_MAX_DRIVE];
   char dir[_MAX_DIR];
   char fname[_MAX_FNAME];
   char ext[_MAX_EXT];

   _splitpath (NameFileCalibr, drive, dir, fname, ext );
   char NameINL [_MAX_PATH];
   _makepath (NameINL, drive, dir, fname, ".INL");
   
   SaveInlPar (NameINL);

//-
   if ((Ret = OpenWrite  (NameFileCalibr))) { Ret = OPEN_ER; goto _Error;}
   if ((Ret = Write ())) { Ret = OPEN_ER; goto _Error;}
_Error:
   Close ();

   return Ret;
};
//-----------------------------------------------------------------------------
int CCD_FitStruct::Read ()
{
   if (!m_HF)        return ER;
   int Ret = OK;

   int y;
   int Len;
   CFitParam * Addr;
   int SizeParam;
   int R;

   unsigned long   Rev;
   ReadFile (m_HF, &m_Header, sizeof (CFitHeader), &Rev, NULL);
   if (Rev != sizeof (CFitHeader)){ Ret = RW_ER; goto _Error;}

//   m_Header.W  = m_Header.W;                                               // Novik
//   m_Header.H = m_Header.H;                                               // Novik   

/*
   if (m_Header.W < m_Header.W || m_Header.H < m_Header.H){
      Ret = Return::RW_ER; 
      goto _Error;
   }
*/                                                                        //--- Average
   R = m_Aver.Read (m_HF);
   if (R){ Ret = RW_ER; goto _Error;}
                                                                        //--- Average
   FreeMemory  ();
   AllocMemory ();

   SizeParam = sizeof(CFitParam);

   Len = SizeParam * m_Header.W;

   Addr = m_Param;
   for (y = 0; y < m_Header.H; y++){

      ReadFile (m_HF, Addr, Len, &Rev, NULL);

      if (Rev != Len) return ER;

      Addr += m_Header.W;
      SetFilePointer (m_HF, (m_Header.W-m_Header.W)*SizeParam , NULL, FILE_CURRENT);
   }
_Error:

   return OK;
};
//-----------------------------------------------------------------------------
int CCD_FitStruct::Read (char *NameFileCalibr)
{
   int Ret = OK;
   if (NameFileCalibr == NULL)
      if ((Ret = OpenRead  ())) { Ret = OPEN_ER; goto _Error;}else{;}
   else
      if ((Ret = OpenRead  (NameFileCalibr))) { Ret = OPEN_ER; goto _Error;}else{;}

   if ((Ret = Read ())) { Ret = OPEN_ER; goto _Error;}
_Error:
   Close ();

   return Ret;
};
//-----------------------------------------------------------------------------
void CCD_FitStruct::PrintHeader ()
{
   printf ("--- FitStruct Header ---\n");
   printf ("X0      = %d\n",        m_Header.X0);
   printf ("Y0      = %d\n",        m_Header.Y0);
   printf ("Width   = %d\n",        m_Header.W);
   printf ("Height  = %d\n",        m_Header.H);
   printf ("Fokus   = %d  (0 - Small, 1 - Big)\n",        m_Header.Fokus);
   printf ("Binning = %d\n",        m_Header.Binning);
   printf ("HV      = %6.0f\n",     m_Header.HV);
   printf ("Background  = %6.0f\n", m_Header.BkAverage);
};
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void CCD_FitStruct::ClearINL ()
{
   memset (m_InlSumm, 0, sizeof (double) * LENGTH_OF_WORD);
   memset (m_InlNum,  0, sizeof (int)    * LENGTH_OF_WORD);
   memset (m_InlAmpl, 0, sizeof (int)    * LENGTH_OF_WORD);
};
//-----------------------------------------------------------------------------
//  Line
void CCD_FitStruct::AccumulateINL (double *X, double*Y, int Points, float P1, int Bk)
{
   if (!P1) return;

   WORD Max = 0xFFFF;
   double  V;
   int I;

   for (int i = 0; i < Points; i++){
/*
      double A = P1 * X[i];
      V = A - Y[i];
      V = V / P1;
      
      I = (int) X[i] + Bk;

      if (I < 0 || I > Max){
      }
      else{
         m_InlSumm [I] += V;
         m_InlNum [I]++;
      }
*/
      double X0 = Y[i] / P1;
      V = (X[i] - X0);
      
      I = (int) X0 + Bk;

      if (I < 0 || I > Max){
      }
      else{
         m_InlSumm [I] += V;
         m_InlNum [I]++;
      }

//Debug if (I == 4108){
//Debug printf ("V=%e, i = %d, X=%e, Bk=%d, x=%d, y=%d, %e\n", V, i, X[i], Bk, x, y, m_InlSumm [I]); 
//Debug }
   }
};
//-----------------------------------------------------------------------------
void CCD_FitStruct::AverageINL ()
{
   int i;
   double *Addr = new double [LENGTH_OF_WORD];

   int K0 = 5;
   double Size  = K0 * 2 + 1;
   double Summ;
   for (i = K0; i < LENGTH_OF_WORD-K0-1; i++){
      Summ = 0;
      for (int k = -K0; k <= K0; k++){
         Summ += m_InlSumm [i+k];
         Addr [i] = Summ / Size;
      }
   }
   for (i = 0; i < LENGTH_OF_WORD; i++)
      m_InlSumm
      [i] = Addr [i];

   delete Addr;

//---
   for (i = 0; i < LENGTH_OF_WORD; i++){
      if (m_InlNum [i]){
         if (m_InlNum [i] < 100){
            m_InlAmpl [i] = 0;
         }
         else{
            double V = m_InlSumm [i] / (double) m_InlNum [i];
            if (V > 0)
               m_InlAmpl [i] = (int) (V + 0.5);
            else
               m_InlAmpl [i] = (int) (V - 0.5);
         }
      }
      else
         m_InlAmpl [i] = 0;
   }
};
//-----------------------------------------------------------------------------
void CCD_FitStruct::RepairINL (WORD * Addr)
{
   int I;
   int V;
   int Max = 0xFFFF;

   for (int i = 0; i < m_Header.W * m_Header.H; i++){
      I = (int) Addr [i];

      V = m_InlAmpl [I];
      V = Addr [i] - V;
      if (V < 0){
         V = 0;
      }
      else if (V > Max) {
         V = Max;
      }
      Addr [i] = V;
   }
};
//-----------------------------------------------------------------------------
void CCD_FitStruct::SaveInlPar (char *Name)
{
   char Txt  [256];

   FILE * hFile = fopen (Name, "w");

   if (!hFile){
      ::MessageBox (0, "", "Error: Open INL File",  MB_OK);
      return;
   }

   for (int i = 0; i < LENGTH_OF_WORD; i++){
      sprintf (Txt, "%d \t %d \t %d\n", i, m_InlAmpl [i], m_InlNum [i]);
      fprintf (hFile, Txt);
   }

   fclose (hFile);
};
//-----------------------------------------------------------------------------
void CCD_FitStruct::ReadInlPar (char *NameCOF)
{
   char drive[_MAX_DRIVE];
   char dir[_MAX_DIR];
   char fname[_MAX_FNAME];
   char ext[_MAX_EXT];

   _splitpath (NameCOF, drive, dir, fname, ext );
   char Name [_MAX_PATH];
   _makepath (Name, drive, dir, fname, ".INL");

   FILE * hFile = fopen (Name, "r");
   if (!hFile){
      for (int i = 0; i < LENGTH_OF_WORD; i++)
         m_InlAmpl[i] = 0;
      return;
   }

   bool Ret = true;
   int V;
   for (int i = 0; i < LENGTH_OF_WORD; i++){
      int Fields = fscanf (hFile, "%d %d %d", &V, &(m_InlAmpl[i]), &V);
      if (Fields != 3){
         Ret = false;
         break;
      }
   }

   if (!Ret)
      for (int i = 0; i < LENGTH_OF_WORD; i++)
         m_InlAmpl[i] = 0;

   fclose (hFile);
};
//-----------------------------------------------------------------------------
