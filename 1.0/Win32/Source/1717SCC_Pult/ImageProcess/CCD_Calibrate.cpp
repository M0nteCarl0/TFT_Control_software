//#include "stdafx.h"
//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 29.11.04
//    ver. 2.00
//-----------------------------------------------------------------------------

#include "CCD_Average.h"
#include "CCD_Calibrate.h"

#include <math.h>
#include "Shlwapi.h"

//-----------------------------------------------------------------------------
CCD_Calibrate::CCD_Calibrate ()
{
   m_FlgINL   = false;
   m_FragmNum = 1;
};
//-----------------------------------------------------------------------------
CCD_Calibrate::~CCD_Calibrate ()
{
   Clear ();
};
//-----------------------------------------------------------------------------
void CCD_Calibrate::Clear ()
{
   m_FitStruct.Clear ();

   ClearFitStructMap  ();
   ClearImgFileMap ();
};
//-----------------------------------------------------------------------------
void CCD_Calibrate::SetImageSize (int X0, int Y0, int Width, int Height)
{
   m_FitStruct.m_Header.X0 = X0;    
   m_FitStruct.m_Header.Y0 = Y0;
   m_FitStruct.m_Header.W  = Width; 
   m_FitStruct.m_Header.H  = Height;
};
//-----------------------------------------------------------------------------
int CCD_Calibrate::PreloadAllImages (char * ImgPath)
{
      int Ret = OK;
      ClearImgFileMap ();

		HANDLE hFind;
		WIN32_FIND_DATA finddata;

		char strPath [_MAX_PATH];

      strcpy (strPath, ImgPath);
      strcat (strPath, "\\");
      strcat (strPath, PathFindFileName (ImgPath));
      strcat (strPath, ".ARM");
      Ret = m_FitStruct.m_Aver.Read (strPath);

      strcpy (strPath, ImgPath);
      strcat (strPath, "\\*.flr");

		hFind =  ::FindFirstFile (strPath, &finddata);

		BOOL bMore = (hFind != (HANDLE) -1);
		if(!bMore) {
			return false;
		}

		while (bMore) {
			CCD_ImgFile * pImgFile = new CCD_ImgFile ();

         pImgFile->SetOffset      (0, 0);
         pImgFile->SetFragmNum    (m_FragmNum);

         strcpy (strPath, ImgPath);
         strcat (strPath, "\\");
         strcat (strPath, finddata.cFileName);
         if (pImgFile->OpenRead (strPath)){
            if (!pImgFile->ReadHeader ()) { Ret = ER; goto _Error;}

            pImgFile->SetWidthHeight  (m_FitStruct.m_Header.W, m_FitStruct.m_Header.H);

            WORD *Addr = new WORD [m_FitStruct.m_Header.W * m_FitStruct.m_Header.H];
            bool R = pImgFile->ReadImg (Addr);
            if (!R){
               delete Addr;
               Ret = ER; 
               goto _Error;
            }
            double Mean;
			RECT ROI;
			ROI.bottom = m_RectMean.bottom;
			ROI.left = m_RectMean.left;
			ROI.right = m_RectMean.right;
			ROI.top = m_RectMean.top;

            MakeMean (Addr, m_FitStruct.m_Header.W, m_FitStruct.m_Header.H, &ROI, &Mean);
            pImgFile->SetMean (Mean);
            delete Addr;

            m_ImgFileMap [Mean] = pImgFile;

			}
_Error:
         pImgFile->Close ();

			bMore = ::FindNextFile (hFind, &finddata);
		}

		::FindClose(hFind);

	return Ret;
};
//-----------------------------------------------------------------------------
int CCD_Calibrate::Calibrate (int Fokus, int Binning, float HV)
// 2 - Not enough calibration points
// 3 - Error: ReadImgFragm
{
   int Ret = OK;
      m_FitStruct.ClearINL ();

		bool bRet = false;
		int Points = m_ImgFileMap.size() - 1;

		if (Points < CCD_FitStruct::NUM_POL) {
         ::MessageBox (0, "", "Not enough calibration points",  MB_OK);
         return ER+1;
		}

      m_FitStruct.FreeMemory ();
      m_FitStruct.AllocMemory ();
      CCD_FitStruct::CFitParam * FitParam = m_FitStruct.GetFitParam ();

      MAP_DOUB_IMGFILE::iterator It;
      for (It = m_ImgFileMap.begin (); It != m_ImgFileMap.end (); It++){
         It->second->OpenRead ();
         It->second->AllocMemory ();
      }

      double* X  = new double [Points];
		double* Y  = new double [Points];
		double* Er = new double [Points];

      m_FitStruct.SetBkAverage((float)m_ImgFileMap.begin()->second->GetMean());
      m_FitStruct.m_Header.HV       = HV;
      m_FitStruct.m_Header.Binning  = Binning;
      m_FitStruct.m_Header.Fokus    = Fokus;

      int J = 0;
      for (int f = 0; f < m_FragmNum; f++){

         int Row;
         for (It = m_ImgFileMap.begin (); It != m_ImgFileMap.end (); It++){
            if (!It->second->ReadImgFragm (f, &Row))
			{
               return ER+2;
            }
         }
         int  k = 0;
         int SizeStrip = Row * m_FitStruct.m_Header.W;
         for (int p = 0; p < SizeStrip; p++){

            It = m_ImgFileMap.begin ();
            WORD * AddrBk = It->second->GetAddrMemory ();
            It++;

            k = 0;
            for (; It != m_ImgFileMap.end (); It++){
               WORD * Addr = It->second->GetAddrMemory ();
               double A = (double) Addr [p] - (double) AddrBk [p];
					double B = It->second->GetMean () - m_FitStruct.GetBkAverage ();
               Y[k] = B;
					X[k] = A; 
               Er[k] = m_FitStruct.m_Aver.FindRMS ((double) Addr [p]);
               k++;
				}

            bRet = FitPol2 (X, Y, Er, Points, 
                              &FitParam [J].Par [0], 
                              &FitParam [J].Par [1]);

            if (!bRet){
				Ret = ER;
            }

            int y = p / m_FitStruct.m_Header.W;
            int x = p - y * m_FitStruct.m_Header.W;
//            if (1200 == x && (y > 1800 && y < 2200))
//               SaveFitPar (Points, X, Y, &FitParam [J], x);

            FitParam [J].Par [0] = (float) AddrBk [p];

            int Delta = 100;
            if ((y > Delta && y <  m_FitStruct.m_Header.H-Delta) && (x > Delta && x < m_FitStruct.m_Header.W-Delta))
               m_FitStruct.AccumulateINL (X, Y, Points, FitParam [J].Par[1], AddrBk [p]);
            J++;
         }
      }
      
      m_FitStruct.AverageINL ();

      delete [] X;
      delete [] Y;
      delete [] Er;

      for (It = m_ImgFileMap.begin (); It != m_ImgFileMap.end (); It++){
         It->second->Close ();
         It->second->FreeMemory ();
      }

	return Ret;
};
//-----------------------------------------------------------------------------
int CCD_Calibrate::WriteFitStruct (char *NameFileCalibr)
{
   return m_FitStruct.Write  (NameFileCalibr);
};
//-----------------------------------------------------------------------------
int CCD_Calibrate::PreLoadFitStructures (char * ImgPath)
{
      int Ret = OK;

      ClearFitStructMap  ();

		HANDLE hFind;
		WIN32_FIND_DATA finddata;

		char strPath [_MAX_PATH];

      strcpy (strPath, ImgPath);
      strcat (strPath, "\\*.COF");

		hFind =  ::FindFirstFile (strPath, &finddata);

      if ((hFind == (HANDLE) -1))
         return ER;

      BOOL bMore = TRUE;

		while (bMore) {
//         CCD_FitStruct * Ptr = new CCD_FitStruct (m_FitStruct.m_Header.W, m_FitStruct.m_Header.H);    // Novik
         CCD_FitStruct * Ptr = new CCD_FitStruct ();

         strcpy (strPath, ImgPath);
         strcat (strPath, "\\");
         strcat (strPath, finddata.cFileName);
         if (!Ptr->OpenRead (strPath)){
            if (Ptr->ReadHeader ()) { Ret = ER; goto _Error;}

            Ptr->ReadInlPar (strPath);

            double Index = Ptr->m_Header.HV * 10 + Ptr->m_Header.Binning;
            m_FitStructMap [Index] = Ptr;
			}
_Error:
         Ptr->Close ();

			bMore = ::FindNextFile (hFind, &finddata);
		}

		::FindClose(hFind);

	return Ret;
};
//-----------------------------------------------------------------------------
int CCD_Calibrate::Repair (WORD * AddrIn, float HV, int Binning, WORD * AddrOut, float *HVcalibr)
{
   *HVcalibr = -1;

   int Ret = OK;
   if (m_FitStructMap.size () == 0) return ER;
   
   CCD_FitStruct * Ptr0 = NULL;
   float HV0            = 0;
   int   Binning0       = 0;

   MAP_DOUB_FITSTRUCT::iterator It;
   MAP_DOUB_FITSTRUCT::iterator ItMem;
   const float MyMax = 1.e+19f;
   float dHV = MyMax;
   for (It = m_FitStructMap.begin (); It != m_FitStructMap.end (); It++){

      HV0 = (float)(It->first / 10);
      Binning0 = (int) (It->first) - (int)(It->first/10)*10;

      if (Binning0 == Binning){
         if (dHV > fabs (HV0 - HV)){
            dHV = fabs (HV0 - HV);
            ItMem = It;
         }
      }
   }
   if (dHV == MyMax)  return ER;

   Ptr0 = ItMem->second;
   *HVcalibr = (float) (ItMem->first / 10);

   if (Ptr0 == NULL) return CLBR_ER1;

   m_FitStruct.m_Header.W  = Ptr0->m_Header.W;
   m_FitStruct.m_Header.H  = Ptr0->m_Header.H;

   if (
       m_FitStruct.m_Header.W  != Ptr0->m_Header.W    ||
       m_FitStruct.m_Header.H  != Ptr0->m_Header.H){
       return CLBR_ER2;
   }
   if (m_FitStruct.m_Header.X0 != -1   &&
       m_FitStruct.m_Header.X0 != Ptr0->m_Header.X0)  return CLBR_ER2;


   if (m_FitStruct.m_Header.Y0 != -1   &&
       m_FitStruct.m_Header.Y0 != Ptr0->m_Header.Y0)  return CLBR_ER2;


   int Size = m_FitStruct.m_Header.W * m_FitStruct.m_Header.H;
   WORD * AddrOut0 = new WORD [Size];

   Ret = Ptr0->Read (NULL);
   if (Ret){ goto _Error;}

   RepairImage (AddrIn, AddrOut0, Ptr0);

   memcpy (AddrOut, AddrOut0, Size * sizeof (WORD));

_Error:

   Ptr0->FreeMemory ();

   delete [] AddrOut0;

   return Ret;
};
//-----------------------------------------------------------------------------
int CCD_Calibrate::GetNoiseVersusAmpl (double *Ampl, double *Noise, int NumIn, int *NumOut, float HV)
{
   int Ret = OK;
   if (m_FitStructMap.size () == 0) return ER;

   
   MAP_DOUB_FITSTRUCT::iterator It;

   CCD_FitStruct * Ptr0 = NULL;
   CCD_FitStruct * Ptr1 = NULL;
   CCD_FitStruct * Ptr  = NULL;
   float HV0;
   float HV1;

   if (m_FitStructMap.size () == 1){
      Ptr = m_FitStructMap.begin ()->second;
   }
   else{
      It = m_FitStructMap.begin ();
      It++;
      for (; It != m_FitStructMap.end (); It++){
         Ptr1 = It->second;
         HV1  = (float)It->first;
         It--;
         Ptr0 = It->second;
         HV0  = (float)It->first;
         It++;

         if (HV < It->first)
            break;
      }
      if (fabs (HV0 - HV) < fabs (HV1 - HV))
         Ptr = Ptr0;
      else
         Ptr = Ptr1;

   }

   int N = Ptr->m_Aver.m_MapSize;

   if (N > NumIn)  N = NumIn;

   for (int i = 0; i < N; i++){
      Ampl[i]  = Ptr->m_Aver.m_X[i];
      Noise[i] = sqrt (Ptr->m_Aver.m_Y[i]);

      //printf ("Ampl %7.3f, Noise %7.3f\n", Ampl[i], Noise[i]);
   }

   *NumOut = N;

   return OK;
};
//---------------------------------------------------------------------------
void CCD_Calibrate::PrintFitStructMap ()
{
   printf ("m_FitStructMap.size = %d\n", m_FitStructMap.size ());
   MAP_DOUB_FITSTRUCT::iterator It;
   int J = 0;
   for (It = m_FitStructMap.begin (); It != m_FitStructMap.end (); It++){
      printf ("[%d] X0 = %d, Y0 = %d,  W = %d, H = %d, High Voltage = %f KV, Binning = %d, Time = %u\n", 
                        J, 
                        It->second->m_Header.X0, 
                        It->second->m_Header.Y0, 
                        It->second->m_Header.W, 
                        It->second->m_Header.H, 
                        It->second->m_Header.HV, 
                        It->second->m_Header.Binning, 
                        It->second->m_Header.Time);
   }
};
//-----------------------------------------------------------------------------
void CCD_Calibrate::PrintImgFileMap ()
{
   printf ("m_ImgFileMap.size = %d\n", m_ImgFileMap.size ());
   MAP_DOUB_IMGFILE::iterator It;
   for (It = m_ImgFileMap.begin (); It != m_ImgFileMap.end (); It++){
      printf ("Mean Ampl = %8.2f\n", It->first);
   }
};
//-----------------------------------------------------------------------------
// PROTECTED   PROTECTED   PROTECTED   PROTECTED   PROTECTED   PROTECTED   
//-----------------------------------------------------------------------------
void CCD_Calibrate::ClearImgFileMap ()
{
   MAP_DOUB_IMGFILE::iterator It;
   for (It = m_ImgFileMap.begin (); It != m_ImgFileMap.end (); It++){
      delete It->second;   
   }
   m_ImgFileMap.clear();
};
//-----------------------------------------------------------------------------
bool CCD_Calibrate::FitPol2 (double *X, double *Y, double *Er, int N, float *P0, float *P1)
{
	*P0 = 0;
	*P1 = 0;
	double YX  = 0;
	double X2  = 0;

	for(int i=0;i < N;i++)
		if(Er[i])
		{
			double V = Y[i] * X[i];
			YX  += V / Er[i];

			V = X[i] * X[i]; 
			X2  += V / Er[i];
		}

	if(X2 == 0)
		return false;

	*P1 = (float)(YX / X2);

	return true;
/*
   double YX2 = 0;
   double YX  = 0;
   double X2  = 0;
   double X3  = 0;
   double X4  = 0;

   for (int i = 0; i < N; i++){
      double V = Y[i] * X[i];
      YX  += V;
      YX2 += V * X[i];

      V = X[i]*X[i]; 

      X2  += V;
      X3  += V * X[i];
      X4  += V * V;
   }

   double Det = X4*X2 - X3*X3;
   if (!Det) return false;

   *A = (YX2*X2 - YX*X3)  / Det;
   *B = (YX *X4 - YX2*X3) / Det;
*/
};
//-----------------------------------------------------------------------------
void CCD_Calibrate::SaveFitPar (int Points, double *X, double *Y, CCD_FitStruct::CFitParam *P, int J)
{
   char Txt  [256];
   char Name [_MAX_PATH];

   sprintf (Name, "Fit_%d.fit", J);
   FILE * hFile = fopen (Name, "w");

   if (!hFile){
      ::MessageBox (0, "", "Error: Open Log File",  MB_OK);
      return;
   }

   sprintf (Txt, "%d\n", Points);
   fprintf (hFile, Txt);
   for (int i = 0; i < Points; i++){
      sprintf (Txt, "%f    %f\n", X[i], Y[i]);
      fprintf (hFile, Txt);
   }

   sprintf (Txt, "%e \t %e\n", P->Par[0], P->Par[1]);
   fprintf (hFile, Txt);

   fclose (hFile);
};
//-----------------------------------------------------------------------------
void CCD_Calibrate::ClearFitStructMap ()
{
   MAP_DOUB_FITSTRUCT::iterator It;
   for (It = m_FitStructMap.begin (); It != m_FitStructMap.end (); It++){
      delete It->second;   
   }
   m_FitStructMap.clear();
};
//-----------------------------------------------------------------------------
void CCD_Calibrate::RepairImage (WORD * AddrIn, WORD * AddrOut, CCD_FitStruct * Ptr)
{
   WORD Max = 0xFFFF;
   CCD_FitStruct::CFitParam * FitParam = Ptr->GetFitParam ();
   double Bk = (double) Ptr->GetBkAverage ();
   int * InlAmpl = Ptr->GetInlAddr ();

   int I;
   int A;
   for (int i = 0; i < m_FitStruct.m_Header.W * m_FitStruct.m_Header.H; i++){
      if (m_FlgINL){
         I = (int) AddrIn [i];

         A = InlAmpl [I];
         A = AddrIn [i] - A;
         if (A < 0){
            A = 0;
         }
         else if (A > Max) {
            A = Max;
         }
      }
      else{
         A = (int) AddrIn[i];
      }

      double V = (double) A - (double) FitParam [i].Par[0];

      double Ampl =  (double) FitParam [i].Par[1] * V;  

      Ampl += Bk;

      if (Ampl < 0)           AddrOut [i] = 0;
      else if (Ampl <= Max)   AddrOut [i] = (WORD) Ampl;
      else if (Ampl  > Max)   AddrOut [i] = Max;
   }
};
//-----------------------------------------------------------------------------
void CCD_Calibrate::ImgInterpolation (WORD *AddrOut0, float HV0, WORD *AddrOut1, float HV1, WORD *AddrOut, float HV)
{
   double dHV10 = HV1 - HV0;
   if (dHV10 == 0) return;
   dHV10 = 1/dHV10;

   double dHV1 = HV1 - HV;
   double dHV0 = HV  - HV0;

   WORD Max = 0xFFFF;
   for (int i = 0; i < m_FitStruct.m_Header.W*m_FitStruct.m_Header.H; i++){
      double V = dHV10 * ((double) AddrOut0[i] * dHV1 + (double) AddrOut1[i] * dHV0);

      if (V < 0)
         AddrOut [i] = 0;
      else if (V <= Max)
         AddrOut [i] = (WORD) V;
      else if (V > Max)
         AddrOut [i] = Max;
   }

};
//-----------------------------------------------------------------------------
