//-----------------------------------------------------------------------------
//    Novikov V
//    date 24.04.05
//    ver 1.00
//-----------------------------------------------------------------------------
#ifndef  __CCD_SUPPRESS__
#define  __CCD_SUPPRESS__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//-----------------------------------------------------------------------------
#include "Word_Image.h"
#include <math.h>
//-----------------------------------------------------------------------------
class   CCD_Suppress
{
   public:
      enum Return {OK=0, ER=1, OPEN_ER=2, RW_ER=3, ALLOC_ERROR=99, UNKNOWN_ERROR=100}; 

      Word_Image m_Img;
      Word_Image m_ImgAver;
      Word_Image m_ImgRms;
                           // Y = P0 + P1 * X      <-- Line
      double m_P0_Noise;   
      double m_P1_Noise;   
      double m_NoiseMarn;

      double m_Coeff1;
      double m_Coeff2;
      int    m_Coeff3;

      CCD_Suppress ();
      ~CCD_Suppress ();
      
      void Create ();
      void Clear  ();
   
      void SetNoise  (double *x, double *y, int N, double NoiseMagn = 1);
      void SetNoise  (double P0, double P1){ m_P0_Noise = P0;  m_P1_Noise = P1; };
      void SetCoeff  (double Coeff1, double Coeff2){ m_Coeff1 = Coeff1; m_Coeff2 = Coeff2; };

      void Act       (WORD *AddrIn, int Nx, int Ny, WORD *AddrOut);
};
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#endif