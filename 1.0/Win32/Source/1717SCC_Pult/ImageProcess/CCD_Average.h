

//#include <Windows.h>
//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 29.11.04
//    ver. 2.00
//-----------------------------------------------------------------------------
#ifndef _CCD_AVERAGE_H_
#define _CCD_AVERAGE_H_

#include <map>
#include <new.h>
#include <Windows.h>
#include "Posiz_Rect.h"


using namespace std;


class  CCD_Object  
{
	public:
		enum Return {OK = 0,ER = 1,OPEN_ER = 2,RW_ER = 3,CLBR_ER1 = 4,CLBR_ER2 = 5,ALLOC_ERROR = 99,UNKNOWN_ERROR = 100}; 
};

class   CCD_Average : public CCD_Object
{
	public:
		CCD_Average();
		CCD_Average(const int Width,const int Height);

		virtual ~CCD_Average();

	public:
		enum {NUMB_POINTS = 50};

		struct CAverageHeader
		{
			int Key;
			int Points;
			int SuppressLevel;
			time_t Time;
			Posix_Rect RectRms;
			Posix_Rect RectAmpl;
			
			void Clear()
			{
				memset(this,0,sizeof(CAverageHeader));
			}
		};

		struct CAverageImage
		{
			int Err;
			double Rms_Single;
			double Rms_Aver;
			double Ampl_Aver;
			int Statistic;

			void Clear()
			{
				memset(this,0,sizeof(CAverageImage));
			}
		};

	public:
		void Clear();

		int GetNumPointPathes() const { return m_Points; };

		void SetImageSize(const int Width,const int Height)
		{
			m_Width = Width;
			m_Height = Height;
		}

		void SetRectMean(const int l,const int t,const int r,const int b)
		{
			m_RectMean.SetRect(l,t,r,b);
		}

		void SetRectRMS(const int l,const int t,const int r,const int b)
		{
			m_RectRms.SetRect(l,t,r,b);
		}

		int  LoadFileOfFile(const char * const FileName);
		
		int Average(const char * const PathOut,const double P0_Noise = 0,const double P1_Noise = 0,const double Coeff1 = 2,const double Coeff2 = 3);
		
		int PutSumm(const char * const NameFile,const double P0_Noise,const double P1_Noise,const double Coeff1,const double Coeff2);
		void PutSumm(const WORD * const  AddrIn);

		int SaveSumm(const char * const NameOut);
		int CloseSumm(const char * const NameCfg);		
		
		void OpenSumm();
		void ClearSumm();

		static void FulfilMeanRms(const WORD *  AddrIn,const int Width,const int Height,const Posix_Rect * const Rect,double * const Mean,double * const Rms);
		
		int Write(const char * const Name);
		int Write(HANDLE HF);

		int Read(const char * const Name);
		int Read(HANDLE HF);

		void SetArrayOfRMS();

		double FindRMS(const double Mean);
		
		void PrintHeader();
		void Print();
		void Save();
		void SaveDerivative();

	protected:
		bool FindFirstImage(const char * const PathIn,char * const NameFile);
		bool FindNextImage(const char * const PathIn,char * const NameFile);

	public:
		int m_Points;
		char m_PointPath[NUMB_POINTS][_MAX_PATH];
		char m_FileCfg[_MAX_PATH];

		HANDLE m_hFindInDir;

		int m_Width;
		int m_Height;
		
		double* m_AddrSumm;

		Posix_Rect m_RectRms;
		Posix_Rect m_RectMean;

		typedef map<double,CAverageImage>  MAP_DOUBLE_AVERIMG;

		CAverageHeader m_Header;
		CAverageImage m_AverImg;

		MAP_DOUBLE_AVERIMG m_Map;

		int m_MapSize;
		
		double* m_X;
		double* m_Y;
};

#endif
