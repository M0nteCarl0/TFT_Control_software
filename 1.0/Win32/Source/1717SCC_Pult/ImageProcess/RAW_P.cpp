#include "RAW_P.h"

/************************************************/
RAW_P::RAW_P(void)
{
	InitializeCriticalSection(&_CriticalSection);
	_ImageBuff = NULL;
}
/************************************************/
RAW_P::~RAW_P(void)
{
	EnterCriticalSection(&_CriticalSection);
	if (_FIO.is_open())
	{
		_FIO.flush();
		_FIO.close();

	}

	if (_ImageBuff)
	{
		delete[] _ImageBuff;
	}
	LeaveCriticalSection(&_CriticalSection);
	DeleteCriticalSection(&_CriticalSection);
	
}
/************************************************/
void RAW_P::SetImageSize(uint64_t Width, uint64_t _Height, bool AutoDetectReolution)
{
	if (!AutoDetectReolution)
	{
		_Height = _Height;
		_Width = Width;
	}
	else
	{

		_Height = 0;
		_Width =  0;
	}
	
	
}
/************************************************/
bool RAW_P::Open(const char * FileName)
{
	EnterCriticalSection(&_CriticalSection);
	bool Flag = false;

	_FIO = fstream(FileName, std::ios::in | std::ios::binary | std::ios::ate);
	if (!_FIO.fail())
	{
		_FileName = FileName;
		_Size = GetSize();
		_FIO.close();
		_FIO = fstream(_FileName, std::ios::in | std::ios::binary);
	
		if (_Height == 0 && _Width == 0)
		{
			_Height = (size_t)sqrt((double)_Size / sizeof(uint16_t));
			_Width = _Height;

		}
		
	
		Flag = true;
		_ImageBuff = new uint16_t[_Width * _Height];
		ReadData(_ImageBuff, _Size);
		
	}
	LeaveCriticalSection(&_CriticalSection);
	return Flag;
}
/************************************************/
void RAW_P::Close(void)
{
	EnterCriticalSection(&_CriticalSection);
	if (_FIO.is_open())
	{
		_FIO.flush();
		_FIO.close();
	}

	if (_ImageBuff)
	{
		delete[] _ImageBuff;
	}

	LeaveCriticalSection(&_CriticalSection);
}
/************************************************/
void RAW_P::WriteData(uint16_t * Data, size_t Datasize)
{
	_FIO.write((char*)Data, Datasize);
}
/************************************************/
void RAW_P::ReadData(uint16_t * Data, size_t Datasize)
{
	_FIO.read((char*)Data, Datasize);
}
/************************************************/
uint16_t * RAW_P::GetData(void)
{
	return _ImageBuff;
}
/************************************************/
size_t RAW_P::GetSize(void)
{
	 return(size_t)_FIO.tellg();
}
/************************************************/
bool  RAW_P::Make(const char* Filename,uint16_t* Data,size_t Datasize)
{

	bool flag = true;
		EnterCriticalSection(&_CriticalSection);
		string Fout = Filename;
		Fout  +=".raw";
		uint16_t* ChData = new uint16_t[Datasize/2];
		_FIO.open(Fout,std::ios::out|std::ios::binary);
		WriteData(Data,Datasize);
		_FIO.flush();
		_FIO.close();
		_FIO.open(Fout,std::ios::in|std::ios::binary);
		ReadData(ChData,Datasize);
		_FIO.close();
		if(!Check(ChData,Data,Datasize/2))
		{

			flag = false;
		}
		delete [] ChData;
		LeaveCriticalSection(&_CriticalSection);
		return flag;



}
/************************************************/
	template<typename T>  bool  RAW_P:: Check(T* In,T* Out,size_t Count)
	{
		bool Res = true;
		size_t ERC = 0;
		for(size_t i = 0;i<Count;i++)
		{
			if(In[i]!=Out[i])
			{

				ERC++;
			}
		
       }
		if(ERC!=0)
		{
			Res = false;

		}
		return Res;
	}