
//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 29.11.04
//    ver. 2.00
//-----------------------------------------------------------------------------

#ifndef __CCD_FITSTRUCT_H__
#define __CCD_FITSTRUCT_H__

#pragma warning(disable:4786)

#include <new>
#include <new.h>
#include "CCD_Average.h"

#define LENGTH_OF_WORD 0x10000

using namespace std;

class  CCD_FitStruct : public CCD_Object
{
	public:
		enum {NUM_POL = 2};

		const int M_CALIBR_KEY;

		struct CFitHeader
		{
			int      Key;
			int      X0;
			int      Y0;
			int      W;
			int      H;
			int      Fokus;      // 0 - Small,   1 - Big
			int      Binning;
			float    HV;
			float    BkAverage;
			time_t   Time;
			
			void Clear() { memset(this,0,sizeof(CFitHeader)); }
		};

		struct CFitParam
		{
			float Par[NUM_POL];
		};

	public:
		char m_FileName [_MAX_PATH];
		
		HANDLE m_HF;

		CCD_Average m_Aver;
		CFitHeader m_Header;
		
		CFitParam* m_Param;

		double* m_InlSumm;
		
		int* m_InlNum ;
		int* m_InlAmpl;

   public:
		CCD_FitStruct ();
		CCD_FitStruct (int Width, int Height);

		virtual ~CCD_FitStruct();

		void Constructor(int Width,int Height);
		void Clear ();
		void AllocMemory();
		void FreeMemory();

		CCD_Average* GetAverage() { return &m_Aver; }
		CFitParam* GetFitParam() { return m_Param; }

		void SetBkAverage(float Ampl) { m_Header.BkAverage = Ampl; }
		float GetBkAverage() { return m_Header.BkAverage; }
		int* GetInlAddr() { return m_InlAmpl; }

		int OpenRead(char* Name);
		int OpenRead(){ return OpenRead(m_FileName); }
		int OpenWrite(char *Name);
		void Close();

		int ReadHeader();
		void GetHeaderHW(int* Width,int* Height);

		int Write();
		int Write(char* NameFileCalibr);
		int Read();
		int Read(char* NameFileCalibr);
      
		void PrintHeader();

	public:
		void ClearINL();
		void AccumulateINL(double* X,double* Y,int Points,float P1,int Bk);
		void AverageINL();
		void RepairINL(WORD* Addr);
		void SaveInlPar(char* Name);
		void ReadInlPar(char* Name);
};

#endif
