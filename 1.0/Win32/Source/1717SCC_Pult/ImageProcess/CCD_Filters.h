//-----------------------------------------------------------------------------
//    Novikov V
//    date 03.03.05
//    ver 1.00
//-----------------------------------------------------------------------------
#ifndef  __CCD_FILTERS__
#define  __CCD_FILTERS__




#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//-----------------------------------------------------------------------------
#include "Word_Image.h"
#include <math.h>
//-----------------------------------------------------------------------------
class    CCD_Filters
{
   public:
      enum Return {OK=0, ER=1, OPEN_ER=2, RW_ER=3, ALLOC_ERROR=99, UNKNOWN_ERROR=100}; 
      typedef WORD pixel;

      Word_Image m_Img;
      Word_Image m_ImgAver;
      Word_Image m_ImgRms;
                           // Y = P0 + P1 * X      <-- Line
      double m_P0_Noise;   
      double m_P1_Noise;   
      double m_NoiseMarn;

      double m_Y0_Quench;
      double m_Y1_Quench;
      double m_Z0_Quench;
      double m_Z1_Quench;
                           // Y = P0 + P1 * X      <-- Line
      double m_P0_Quench;
      double m_P1_Quench;

      int m_Smooth;

      CCD_Filters ();
      ~CCD_Filters ();
      
      void Create ();
      void Clear  ();
   
      void SetLowQuench  (double Y0){ m_Y0_Quench = Y0;};
      void SetHighQuench (double Y1){ m_Y1_Quench = Y1;};
      void SetNoise      (double *x, double *y, int N, double NoiseMagn = 1);
      bool SetQuench     (double X1, double Y1, double X2, double Y2, double *A, double *B);

      void Act    (WORD *AddrIn, int Nx, int Ny, WORD *AddrOut);

      void AdaptAverage    (Word_Image *ImgOut);
      void AdaptAverage    (int K, int X0, int X1, int Y0, int Y1, Word_Image *ImgOut);

      void Smooth       (int K, Word_Image *ImgIn, Word_Image *ImgOut);
      void ImageExtend  (Word_Image *ImgIn, int Cells, Word_Image *ImgOut);

};
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#endif