#pragma once
class Posix_Rect
{

public:

	Posix_Rect();
    ~Posix_Rect(void);
	Posix_Rect(size_t l, size_t t, size_t r, size_t b);
	void SetRect(size_t l, size_t t, size_t r, size_t b);
	size_t top;
	size_t bottom;
	size_t right;
	size_t left;
   
};

