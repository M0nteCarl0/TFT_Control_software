//-----------------------------------------------------------------------------
//    Novikov V
//    date 03.03.05
//    ver 1.00
//-----------------------------------------------------------------------------

#include "CCD_Filters.h"
#include "CCD_ImgFile.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
CCD_Filters::CCD_Filters ()
{
    m_P0_Noise  = 0;   
    m_P1_Noise  = 0;

    m_Y0_Quench = 0;
    m_Y1_Quench = 0;
    m_Z0_Quench = 1;
    m_Z1_Quench = 4;

    m_P0_Quench = 0;
    m_P1_Quench = 0;

    m_Smooth = 3;
};
//-----------------------------------------------------------------------------
CCD_Filters::~CCD_Filters ()
{
   Clear ();
};
//-----------------------------------------------------------------------------
void CCD_Filters::Clear ()
{
};
//-----------------------------------------------------------------------------
void CCD_Filters::Create ()
{
   Clear ();
};
//-----------------------------------------------------------------------------
void CCD_Filters::SetNoise (double *x, double *y, int N, double NoiseMagn)
{
   m_P1_Noise = 0;
   m_P0_Noise = 1;

   if (N <= 0 || x == NULL || y == NULL) return;

   if (N == 1){
      m_P1_Noise = 0;
      m_P0_Noise = y[0];
   }

   double X  = 0;
   double X2 = 0;
   double Y  = 0;
   double YX = 0;
   double S  = 0;

   for (int i = 0; i < N; i++){
      y[i] *= NoiseMagn;
   }
   int i;
   for (i = 0; i < N; i++){
      X  += x[i];
      X2 += x[i] * x[i];
      Y  += y[i];
      YX += y[i] * x[i];
      S++;
   }

   double Det  = X2 * S - X * X;
   double MinA = YX * S - Y * X;
   double MinB = X2 * Y - X * YX;

   if (Det){
      m_P1_Noise = MinA / Det;
      m_P0_Noise = MinB / Det;
   }
};
//-----------------------------------------------------------------------------
bool CCD_Filters::SetQuench (double X1, double Y1, double X2, double Y2, double *A, double *B)
{
   *A = 0;
   *B = 0;

   if (!(X2-X1)) return false;

   *A = (Y2-Y1)/(X2-X1);
   *B = Y1 - (*A)*X1;

   return true;
};
//-----------------------------------------------------------------------------
void CCD_Filters::Act (WORD *AddrIn, int Nx, int Ny, WORD *AddrOut)
{
   m_Img.Create (Nx, Ny, AddrIn);

   Word_Image *ImgOut = new Word_Image;
   ImgOut->Create (Nx, Ny, AddrOut);

   Word_Image *ImgZoom = new Word_Image;
   ImgZoom->CopyZoom (2, &m_Img);

   ImageExtend   (ImgZoom, m_Smooth, &m_ImgAver);
   m_ImgRms.Copy (&m_ImgAver);

   Smooth (m_Smooth, &m_ImgAver, &m_ImgAver);

   m_ImgRms.SubtractAbs (&m_ImgAver);
   
   Smooth (2, &m_ImgRms, &m_ImgRms);

   AdaptAverage (ImgOut);

   delete ImgZoom;

   delete ImgOut;
};
//-----------------------------------------------------------------------------
void CCD_Filters::AdaptAverage (Word_Image *ImgOut)
{
   if (!SetQuench (m_Z0_Quench, m_Y0_Quench, m_Z1_Quench, m_Y1_Quench, &m_P1_Quench, &m_P0_Quench))
      return;

   AdaptAverage (1, m_Smooth+1, m_ImgAver.m_Nx - m_Smooth-1, 
                    m_Smooth+1, m_ImgAver.m_Ny - m_Smooth-1,
                    ImgOut);

   AdaptAverage (0, m_Smooth, m_ImgAver.m_Nx-m_Smooth, 
                    m_Smooth, m_Smooth+1,
                    ImgOut);

   AdaptAverage (0, m_Smooth, m_ImgAver.m_Nx-m_Smooth, 
                    m_ImgAver.m_Ny-m_Smooth-1, m_ImgAver.m_Ny-m_Smooth,
                    ImgOut);

   AdaptAverage (0, m_Smooth, m_Smooth+1, 
                    m_Smooth, m_ImgAver.m_Ny-m_Smooth,
                    ImgOut);

   AdaptAverage (0, m_ImgAver.m_Nx-m_Smooth-1, m_ImgAver.m_Nx-m_Smooth, 
                    m_Smooth, m_ImgAver.m_Ny-m_Smooth,
                    ImgOut);

};
//-----------------------------------------------------------------------------
void CCD_Filters::AdaptAverage (int K, int X0, int X1, int Y0, int Y1, Word_Image *ImgOut)
{
   Word_Image *Img = &m_Img;

   const int  N = (K * 2 + 1) * (K * 2 + 1);
   
   int  Summ;
   for (int y = Y0; y < Y1; y++){
      for (int x = X0; x < X1; x++){

         double RmsF = m_P1_Noise * (double) m_ImgAver.m_Addr [y][x] + m_P0_Noise;
         double Rms0 = m_ImgRms.m_Addr [y][x];
         double Z = 0;
         if (RmsF)
            Z = Rms0 / RmsF;
         if (Z < 1)  
            Z = 1;
         int W = m_P1_Quench * Z + m_P0_Quench;
         if (W > 1000) W = 1000;
         int Norma = (N-1) + W;

                                         //---
         int X = (x-m_Smooth)*2;
         int Y = (y-m_Smooth)*2;

//if (X == 526 && Y == 540)
//x = x;

         Summ = 0;
         for (int k = -K; k <= K; k++)
            for (int m = -K; m <= K; m++)
              Summ += (int) Img->m_Addr [Y+k][X+m];

         Summ += (int) Img->m_Addr [Y][X] * (W-1);
         ImgOut->m_Addr[Y][X] = Summ / Norma;
                                         //---
         X++;
         Summ = 0;
		 int k;
         for (k = -K; k <= K; k++)
            for (int m = -K; m <= K; m++)
              Summ += (int) Img->m_Addr [Y+k][X+m];

         Summ += (int) Img->m_Addr [Y][X] * (W-1);
         ImgOut->m_Addr[Y][X] = Summ / Norma;
                                         //---
         Y++;
         Summ = 0;
         for (k = -K; k <= K; k++)
            for (int m = -K; m <= K; m++)
              Summ += (int) Img->m_Addr [Y+k][X+m];

         Summ += (int) Img->m_Addr [Y][X] * (W-1);
         ImgOut->m_Addr[Y][X] = Summ / Norma;
                                         //---
         X--;
         Summ = 0;
         for (k = -K; k <= K; k++)
            for (int m = -K; m <= K; m++)
              Summ += (int) Img->m_Addr [Y+k][X+m];

         Summ += (int) Img->m_Addr [Y][X] * (W-1);
         ImgOut->m_Addr[Y][X] = Summ / Norma;
      }
   }
};
//-----------------------------------------------------------------------------
void CCD_Filters::Smooth (int K, Word_Image *ImgIn, Word_Image *ImgOut)
{
   int N = 2*K + 1;
   int N2 = N * N;

   Word_Image *ImgTmp = new Word_Image (ImgIn->m_Nx, ImgIn->m_Ny);
   ImgTmp->Clear ();

   int x;
   int y;
   for (y = 0; y < ImgIn->m_Ny; y++){
      int Summ = 0;
      for (int i = 0; i < N; i++)   Summ += (int) ImgIn->m_Addr [y][i];
      ImgTmp->m_Addr[y][K] = Summ/N;

      for (int x = K+1; x < ImgIn->m_Nx-K; x++){
         Summ -= ImgIn->m_Addr [y][x-K-1];
         Summ += ImgIn->m_Addr [y][x+K];

         ImgTmp->m_Addr[y][x] = Summ/N;
      }
   }
   for (y = 0; y < ImgIn->m_Ny; y++)
      for (int x = 0; x < K; x++){
         ImgTmp->m_Addr[y][x] = ImgTmp->m_Addr[y][2*K-x];
         int X  = ImgIn->m_Nx - 2*K - 1 + x;
         int X1 = ImgIn->m_Nx-1-x;
         ImgTmp->m_Addr[y][X1] = ImgTmp->m_Addr[y][X];
      }
                                                         //---
   for (x = 0; x < ImgIn->m_Nx; x++){
      int Summ = 0;
      for (int i = 0; i < N; i++) Summ += (int) ImgTmp->m_Addr [i][x];
      ImgOut->m_Addr[K][x] = Summ / N;

      for (int y = K+1; y < ImgIn->m_Ny-K; y++){
         Summ -= ImgTmp->m_Addr [y-K-1][x];
         Summ += ImgTmp->m_Addr [y+K][x];

         ImgOut->m_Addr[y][x] = Summ / N;
      }
   }

   for (x = 0; x < ImgIn->m_Nx; x++)
      for (int y = 0; y < K; y++){
         ImgOut->m_Addr[y][x] = ImgOut->m_Addr[2*K-y][x];
         int Y  = ImgIn->m_Ny - 2*K - 1 + y;
         int Y1 = ImgIn->m_Ny - 1 -y;
         ImgOut->m_Addr[Y1][x] = ImgOut->m_Addr[Y][x];
      }

   delete ImgTmp;
};
//-----------------------------------------------------------------------------
void CCD_Filters::ImageExtend (Word_Image *ImgIn, int Cells, Word_Image *ImgOut)
{
   ImgIn->Check ();

   int Nx = ImgIn->m_Nx + 2 * Cells;
   int Ny = ImgIn->m_Ny + 2 * Cells;

   ImgOut->Erase ();
   ImgOut->Create (Nx, Ny);

   int y;
   for (y = 0; y < ImgIn->m_Ny; y++){
      for (int x = 0; x < ImgIn->m_Nx; x++){
         ImgOut->m_Addr [y+Cells][x+Cells] = ImgIn->m_Addr [y][x];
      }
   }
   // -X-
   for (y = 0; y < ImgIn->m_Ny; y++){
      for (int x = 0; x < Cells; x++){
         ImgOut->m_Addr [y+Cells][Cells-x-1] = ImgIn->m_Addr [y][x];
         ImgOut->m_Addr [y+Cells][Cells+ImgIn->m_Nx+x] = ImgIn->m_Addr [y][ImgIn->m_Nx-x-1];
      }
   }
   // -Y-
   for (y = 0; y < Cells; y++){
      for (int x = 0; x < ImgIn->m_Nx; x++){
         ImgOut->m_Addr [Cells-y-1]          [x+Cells]   = ImgIn->m_Addr [y]              [x];
         ImgOut->m_Addr [Cells+ImgIn->m_Ny+y][x+Cells]   = ImgIn->m_Addr [ImgIn->m_Ny-y-1][x];
      }
   }
   // -X LeftUp, RightUp -
   for (y = 0; y < Cells; y++){
      for (int x = 0; x < Cells; x++){
         ImgOut->m_Addr [y][Cells-x-1] = ImgOut->m_Addr [y][Cells+x];
         ImgOut->m_Addr [y][Cells+ImgIn->m_Nx+x] = ImgOut->m_Addr [y][Cells+ImgIn->m_Nx-x-1];
      }
   }
   // -X LeftDown, RightDown -
   for (y = 0; y < Cells; y++){
      for (int x = 0; x < Cells; x++){
         ImgOut->m_Addr [y+Cells+ImgIn->m_Ny][Cells-x-1]           = ImgOut->m_Addr [y+Cells+ImgIn->m_Ny][Cells+x];
         ImgOut->m_Addr [y+Cells+ImgIn->m_Ny][Cells+ImgIn->m_Nx+x] = ImgOut->m_Addr [y+Cells+ImgIn->m_Ny][Cells+ImgIn->m_Nx-x-1];
      }
   }
};
//-----------------------------------------------------------------------------









