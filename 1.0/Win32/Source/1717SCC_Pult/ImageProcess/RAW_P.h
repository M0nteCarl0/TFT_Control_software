#pragma once
#include <fstream>
#include <stdint.h>
#include <Windows.h>
using namespace std;
#ifndef _RAW_POSIX_
#define _RAW_POSIX_
class RAW_P
{
public:
	RAW_P(void);
	~RAW_P(void);
	void SetImageSize(uint64_t Width, uint64_t _Height, bool AutoDetectReolution = true);
	bool Open(const char* FileName);
	bool Make(const char* Filename,uint16_t* Data,size_t Datasize);
	void Close(void);
	void WriteData(uint16_t * Data, size_t Datasize);
	void ReadData(uint16_t* Data, size_t Datasize);
	uint16_t* GetData(void);
	size_t GetSize(void);
private:
	uint64_t _Width;
	uint64_t _Height;
	size_t _Size;
	uint16_t* _ImageBuff;
	fstream  _FIO;
	string	 _FileName;
	CRITICAL_SECTION _CriticalSection;
	template<typename T>  bool  Check(T* In,T* Out,size_t Count);
};
#endif
