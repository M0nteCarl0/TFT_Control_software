//-----------------------------------------------------------------------------
//    Novikov V
//    date 24.04.05
//    ver 1.00
//-----------------------------------------------------------------------------

#include "CCD_Suppress.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
CCD_Suppress::CCD_Suppress ()
{
    m_P0_Noise  = 0;   
    m_P1_Noise  = 0;

    m_Coeff1 = 2;
    m_Coeff2 = 3;
    m_Coeff3 = 3;
};
//-----------------------------------------------------------------------------
CCD_Suppress::~CCD_Suppress ()
{
   Clear ();
};
//-----------------------------------------------------------------------------
void CCD_Suppress::Clear ()
{
};
//-----------------------------------------------------------------------------
void CCD_Suppress::Create ()
{
   Clear ();
};
//-----------------------------------------------------------------------------
void CCD_Suppress::SetNoise (double *x, double *y, int N, double NoiseMagn)
{
   m_P1_Noise = 0;
   m_P0_Noise = 1;

   if (N <= 0 || x == NULL || y == NULL) return;

   if (N == 1){
      m_P1_Noise = 0;
      m_P0_Noise = y[0];
   }

   double X  = 0;
   double X2 = 0;
   double Y  = 0;
   double YX = 0;
   double S  = 0;

   for (int i = 0; i < N; i++){
      y[i] *= NoiseMagn;
   }
   for (int i = 0; i < N; i++){
      X  += x[i];
      X2 += x[i] * x[i];
      Y  += y[i];
      YX += y[i] * x[i];
      S++;
   }

   double Det  = X2 * S - X * X;
   double MinA = YX * S - Y * X;
   double MinB = X2 * Y - X * YX;

   if (Det){
      m_P1_Noise = MinA / Det;
      m_P0_Noise = MinB / Det;
   }
};
//-----------------------------------------------------------------------------
void CCD_Suppress::Act (WORD *AddrIn, int Nx, int Ny, WORD *AddrOut)
{
   m_Img.Create (Nx, Ny, AddrIn);

   Word_Image *ImgOut = new Word_Image;
   ImgOut->Create (Nx, Ny, AddrOut);
   ImgOut->Copy (&m_Img);

   int Border = 3;
   int A3x3[8], A7x7[24];
   int i;
   int Summ;
   double Rms;
   for (int y = Border; y < Ny - Border; y++){
      for (int x = Border; x < Nx - Border; x++){

//if (x == 384 && y == 512){
//   x = x;
//}

         int A0 = (int) m_Img.m_Addr [y][x];
         if (  A0 > m_Img.m_Addr [y-1][x-1] &&
               A0 > m_Img.m_Addr [y-1][x]   &&
               A0 > m_Img.m_Addr [y-1][x+1] &&
               A0 > m_Img.m_Addr [y]  [x-1] &&
               A0 > m_Img.m_Addr [y]  [x+1] &&
               A0 > m_Img.m_Addr [y+1][x-1] &&
               A0 > m_Img.m_Addr [y+1][x]   &&
               A0 > m_Img.m_Addr [y+1][x+1]){                           // <--- IF-1
                                                         // 3x3   Maybe cluster
            Summ = 0;
            int J = 0;
            for (i = -1; i <= 1; i++) A3x3[J++] = m_Img.m_Addr [y-1][x+i];
            for (i = -1; i <= 1; i++) A3x3[J++] = m_Img.m_Addr [y+1][x+i];
            A3x3[J++] = m_Img.m_Addr [y][x-1];
            A3x3[J++] = m_Img.m_Addr [y][x+1];

            int Summ33 = 0;
            for (i = 0; i < 8; i++) Summ33 += A3x3[i];
            int A33 = (Summ33 + A0) / 9;
                                                         // 5x5   
            Summ = 0;
            for (i = -2; i <= 2; i++) Summ += m_Img.m_Addr [y-2][x+i];
            for (i = -2; i <= 2; i++) Summ += m_Img.m_Addr [y+2][x+i];
            for (i = -1; i <= 1; i++) Summ += m_Img.m_Addr [y+i][x-2];
            for (i = -1; i <= 1; i++) Summ += m_Img.m_Addr [y+i][x+2];

            int A55 = Summ / 16;

            Rms = m_P1_Noise * (double) A55 + m_P0_Noise;
            int Coeff_Rms = (int) (m_Coeff1 * Rms);

            if (A33 > A55 + Coeff_Rms){                              // <--- IF-2
                                                         // 7x7
               J = 0;
               for (i = -3; i <= 3; i++) A7x7[J++] = m_Img.m_Addr [y-3][x+i];
               for (i = -3; i <= 3; i++) A7x7[J++] = m_Img.m_Addr [y+3][x+i];

               for (i = -2; i <= 2; i++) A7x7[J++] = m_Img.m_Addr [y+i][x-3];
               for (i = -2; i <= 2; i++) A7x7[J++] = m_Img.m_Addr [y+i][x+3];

               Summ = 0;
               for (i = 0; i < 24; i++)   Summ += A7x7[i];
               int A77 = Summ / 24;

               Summ = 0;
               for (i = 0; i < 24; i++)   Summ += abs (A77 - A7x7[i]);
               int Rms77 = Summ / 24;

               Rms = m_P1_Noise * (double) A77 + m_P0_Noise;
               Coeff_Rms = (int) (m_Coeff2 * Rms);

               if (Rms77 < Coeff_Rms){                                  // <--- IF-3
                  int AmplLimit = A77 + (m_Coeff3 * Rms);

                  for (int j = -2; j <= 2; j++){
                     for (i = -2; i <= 2; i++){
                        if ((int) m_Img.m_Addr [y+j][x+i] > AmplLimit){    // <--- IF-4
                           ImgOut->m_Addr [y+j][x+i] = A77;
                        }
                     }
                  }
               }
               else{
               }
            }
            else{
               A33 = Summ33 / 8;
               Rms = m_P1_Noise * (double) A33 + m_P0_Noise;
               int Coeff_Rms = (int) (m_Coeff1 * Rms * 4);

               if (A0 > A33 + Coeff_Rms){
                  Summ = 0;
                  for (i = 0; i < 8; i++) Summ += abs (A3x3[i] - A33);
                  int Rms33 = Summ / 8;

                  int Coeff_Rms = (int) (m_Coeff2 * Rms);
                  if (Rms33 < Coeff_Rms){
                     ImgOut->m_Addr [y][x] = A33;
                  }
               }
            }
         }
         else{
         }
      }
   }

   delete ImgOut;
};
//-----------------------------------------------------------------------------










