//#include "StdAfx.h"

//-----------------------------------------------------------------------------
//    Novikov  V
//    Date 29.11.04
//    ver. 2.00
//-----------------------------------------------------------------------------

#include "CCD_Average.h"
#include "CCD_ImgFile.h"
//#include "CCD_Suppress.h"

#include <stdio.h>
#include <math.h>
#include <time.h>

#include "Shlwapi.h"
//-----------------------------------------------------------------------------
CCD_Average::CCD_Average()
 :m_MapSize(0),
  m_X(0),
  m_Y(0),
  m_hFindInDir(0)
{
	SetImageSize(0,0);

	SetRectMean(0,0,0,0);
	SetRectRMS(0,0,0,0);

	Clear();
}

//-----------------------------------------------------------------------------
CCD_Average::CCD_Average(const int Width,const int Height)
 :m_MapSize(0),
  m_X(0),
  m_Y(0),
  m_hFindInDir(0)
{
	SetImageSize(Width,Height);

	SetRectMean(0,0,0,0);
	SetRectRMS(0,0,0,0);

	m_MapSize = 0;
	m_X = 0;
	m_Y = 0;

	m_hFindInDir = 0;

	Clear ();
}

//-----------------------------------------------------------------------------
CCD_Average::~CCD_Average ()
{
	Clear ();

	if(m_X)
	{
		delete [] m_X;
		m_X = nullptr;
	}

	if(m_Y)
	{
		delete [] m_Y;
		m_Y = nullptr;
	}

	if(m_hFindInDir)
	{
		FindClose(m_hFindInDir);
		m_hFindInDir = 0;
	}
}

//-----------------------------------------------------------------------------
void CCD_Average::Clear ()
{
	m_AverImg.Clear();
	m_Header.Clear();
	m_Map.clear();
}

//-----------------------------------------------------------------------------
int CCD_Average::LoadFileOfFile(const char * const FileName)
{
	if(FileName == nullptr)
		return OPEN_ER;

	m_Points = 0;

	FILE * const f = fopen(FileName,"r");

	if(f == nullptr)
		return OPEN_ER;

	while(fgets(m_PointPath[m_Points],_MAX_PATH - 1,f) != nullptr)
	{
		const int Len = strlen(m_PointPath[m_Points]);

		if (Len > 1)
		{
			m_PointPath[m_Points][Len - 1] = 0;

			m_Points += 1;

			if(m_Points >= NUMB_POINTS)
				break;
		}
	}

	fclose(f);

	return OK;
}

//-----------------------------------------------------------------------------
int CCD_Average::Average(const char * const PathOut,const double P0_Noise,const double P1_Noise,const double Coeff1,const double Coeff2)
{
	int Ret = OK;

	char NameSingleImg[_MAX_PATH];

	OpenSumm();// allocate array for summ of image                          // 1)

	for(int i=0;i < m_Points;i++)
	{
		char NameOutImg[_MAX_PATH];

		strcpy(NameOutImg,PathOut);
		strcat(NameOutImg,"\\");
		strcat(NameOutImg,PathFindFileName(m_PointPath[i]));
		strcat(NameOutImg,".flr");

		//printf("NameOutImg: %s\n",NameOutImg);

		ClearSumm();// set initil summ = 0                                 // 2)
		
		//printf("PATH: %s\n",m_PointPath[i]);

		char DIRWithImagesForAveraging[_MAX_PATH];
		strcpy(DIRWithImagesForAveraging,PathOut);
		strcat(DIRWithImagesForAveraging,"\\");
		strcat(DIRWithImagesForAveraging,PathFindFileName(m_PointPath[i]));

		bool bMore = FindFirstImage(DIRWithImagesForAveraging, NameSingleImg);

		while(bMore)
		{
			//printf("	%s\n",NameSingleImg);

			PutSumm(NameSingleImg,P0_Noise,P1_Noise,Coeff1,Coeff2);         // 3)

			bMore = FindNextImage(DIRWithImagesForAveraging/*m_PointPath[i]*/,NameSingleImg);
		}

		if(Ret = SaveSumm(NameOutImg))                                      // 4)
			break;
	}

	char NameAverageCfg[_MAX_PATH];
    string NameOut;
	strcpy(NameAverageCfg,PathOut);
	strcat(NameAverageCfg,"\\");
	strcat(NameAverageCfg,PathFindFileName(PathOut));
	strcat(NameAverageCfg,".ARM");
    NameOut = PathOut;
    NameOut+="\\";
    NameOut = PathOut;
    NameOut+=".ARM";

	CloseSumm(NameAverageCfg);                                              // 5)
	FindClose(m_hFindInDir);
	return Ret;
}

//-----------------------------------------------------------------------------
void CCD_Average::OpenSumm()
{
	const int Size = m_Width * m_Height;
	m_AddrSumm = new double[Size];

	memset(m_AddrSumm,0,Size * sizeof(double));

	Clear();

	m_Header.RectAmpl = m_RectMean;
	m_Header.RectRms = m_RectRms;  
}

//-----------------------------------------------------------------------------
int CCD_Average::CloseSumm(const char * const NameCfg)
{
	delete [] m_AddrSumm;

	return Write(NameCfg);
}

//-----------------------------------------------------------------------------
void CCD_Average::ClearSumm()
{
	const int Size = m_Width * m_Height;
   
	memset(m_AddrSumm,0,Size * sizeof(double));

	m_AverImg.Clear();
}

//-----------------------------------------------------------------------------
void CCD_Average::FulfilMeanRms(const WORD *  AddrIn,const int Width,const int Height,const Posix_Rect * const Rect,double * const Mean,double * const Rms)
{
	const int RmsSize = 30;
	const int RmsStep = 10;
   
	// int X0 = Rect->right; // Novik
	const int X0 = Rect->left;
	const int Y0 = Rect->top;
	
	const int RoiW = Rect->right - Rect->left;
	const int RoiH = Rect->bottom - Rect->top;

	*Mean = 0;
	*Rms  = 0;
	
	if(RmsStep == 0 || RmsSize > RoiW || RmsSize > RoiW )
		return;

	const int Nx = (RoiW - RmsSize) / RmsStep;
	const int Ny = (RoiH - RmsSize) / RmsStep;

	double SummRms  = 0;
	double SummMean = 0;;
	double RoiRms;
	double RoiMean;
	
	for(int y=0;y < Ny;y++)
		for(int x=0;x < Nx;x++)
		{
			int X = X0 + x * RmsStep;
			int Y = Y0 + y * RmsStep;
			
			Posix_Rect Rect(X,Y,X + RmsSize,Y + RmsSize);
			
			RECT ROI;
			ROI.bottom = Rect.bottom;
			ROI.left = Rect.left;
			ROI.right = Rect.right;
			ROI.top = Rect.top;

			MakeMeanRms((WORD*)AddrIn,Width,Height,&ROI,&RoiMean,&RoiRms);
			
			SummRms  += RoiRms;
			SummMean += RoiMean;
		}

	double S = (double) Nx * Ny;
	
	if(S)
	{
		*Mean = SummMean / S;
		*Rms  = SummRms  / S;
	}
}

//-----------------------------------------------------------------------------
void CCD_Average::PutSumm(const WORD * const AddrIn)
{
	double Mean;
	double Rms;

	FulfilMeanRms(AddrIn,m_Width,m_Height,&m_RectRms,&Mean,&Rms);

	m_AverImg.Rms_Single += Rms;
	m_AverImg.Statistic += 1;
    
	const int Size = m_Width * m_Height;

	for(int i=0;i < Size;i++)
		m_AddrSumm [i] += (double)AddrIn[i];
}

//-----------------------------------------------------------------------------
int CCD_Average::PutSumm(const char * const FileName,const double P0_Noise,const double P1_Noise,const double Coeff1,const double Coeff2)
// 0 - Ok
// 1 - ER
// 3 - Some file in  open or read with error
{
	const int Size = m_Width * m_Height;
	
	WORD * const AddrIn   = new WORD[Size];
	WORD * const AddrOut  = new WORD[Size];

	int Ret = OK;

	CCD_ImgFile pImgFile;

	pImgFile.SetOffset(0,0);
	pImgFile.SetFragmNum(1);
	pImgFile.SetWidthHeight(m_Width,m_Height);

	if(pImgFile.OpenRead((char*)FileName))
	{
		//printf("  %s: was open\n",FileName);

		if(!pImgFile.ReadHeader())
		{ 
			Ret = ER + 2;
			goto LABEL_RETURN;
		}

		//printf("  %s: header was read\n",FileName);
		//pImgFile.PrintFlrHeader();

		if(!pImgFile.ReadImg (AddrIn))
		{
			//printf("  %s: cannot read image\n",FileName);
			
			Ret = ER + 2;
			goto LABEL_RETURN;
		}

		//printf("  %s: image was read\n",FileName);

		/*CCD_Suppress Suppress;
      
		Suppress.SetNoise(P0_Noise,P1_Noise);
		Suppress.SetCoeff(Coeff1,Coeff2);
		Suppress.Act(AddrIn,m_Width,m_Height,AddrOut);

		PutSumm(AddrOut);*/ //Novik

		PutSumm (AddrIn);
	}
	else
	{
		//printf("Error: Can't read %s\n",FileName);
		Ret = ER + 2;
	}

LABEL_RETURN:

	pImgFile.Close();

	delete [] AddrIn;
	delete [] AddrOut;

	return Ret;
}

//-----------------------------------------------------------------------------
int CCD_Average::SaveSumm(const char * const NameOut)
{
	const int Size = m_Width * m_Height;

	WORD * const AddrOut  = new WORD[Size];

	memset(AddrOut,0,Size * sizeof(WORD));

	if(m_AverImg.Statistic)
	{
		for(int i=0;i < Size;i++)
			AddrOut[i] = (WORD)(m_AddrSumm[i] / (double)m_AverImg.Statistic);

		CCD_ImgFile pImgFile;

		pImgFile.SetOffset(0,0);
		pImgFile.SetFragmNum(1);
		pImgFile.SetWidthHeight(m_Width, m_Height);

		if(!pImgFile.OpenWrite((char*)NameOut))
		{
			pImgFile.Close();
			
			delete [] AddrOut;
			
			return ER + 3;
		}

		if(!pImgFile.WriteImg(AddrOut))
		{
			pImgFile.Close();
			
			delete [] AddrOut;

			return ER + 3;
		}

		double Mean;
		double Rms;
		
		FulfilMeanRms(AddrOut,m_Width,m_Height,&m_RectRms,&Mean,&Rms);

		m_AverImg.Rms_Aver = Rms;

		// MakeMean(AddrOut,m_Width,m_Height,&m_RectMean,&Mean);
		m_AverImg.Ampl_Aver = Mean;

		m_AverImg.Rms_Single /= (double)m_AverImg.Statistic;

		m_Header.Points += 1;

		m_Map[m_AverImg.Ampl_Aver] = m_AverImg;
	}

	delete [] AddrOut;

	return OK;
}

//-----------------------------------------------------------------------------
int CCD_Average::Write(const char * const Name)
{
	strcpy(m_FileCfg,Name);

	HANDLE HF = CreateFile(Name,GENERIC_WRITE,
					0,
					NULL,
					CREATE_ALWAYS,
					FILE_ATTRIBUTE_NORMAL,
					NULL);

	if(INVALID_HANDLE_VALUE == HF)
		return OPEN_ER;

	const int Ret = Write(HF);

	CloseHandle(HF);

	return Ret;
}

//-----------------------------------------------------------------------------
int  CCD_Average::Write(HANDLE HF)
{
	unsigned long Rev;

	_tzset();

	time(&m_Header.Time);

	WriteFile(HF,&m_Header,sizeof(CAverageHeader),&Rev,nullptr);

	if(Rev != sizeof(CAverageHeader))
		return RW_ER;

	for(MAP_DOUBLE_AVERIMG::iterator It = m_Map.begin();It != m_Map.end();It++)
	{
		WriteFile(HF,&It->second,sizeof(CAverageImage),&Rev,nullptr);

		if(Rev != sizeof(CAverageImage))
			return RW_ER;
	}

	return OK;
}

//-----------------------------------------------------------------------------
int CCD_Average::Read(const char * const Name)
{
	Clear ();

	strcpy(m_FileCfg,Name);

	HANDLE HF = CreateFile(Name,GENERIC_READ,
					0,
					NULL,
					OPEN_EXISTING,
					FILE_ATTRIBUTE_NORMAL,
					NULL);

	if(INVALID_HANDLE_VALUE == HF)
		return OPEN_ER;

	const int Ret = Read(HF);

	CloseHandle(HF);

	return Ret;
}

//-----------------------------------------------------------------------------
int CCD_Average::Read(HANDLE HF)
{
	Clear();

	unsigned long Rev;

	ReadFile(HF,&m_Header,sizeof(CAverageHeader),&Rev,nullptr);

	if(Rev != sizeof(CAverageHeader))
	{
		m_Header.Clear();
		m_Map.clear();

		return RW_ER;
	}

	CAverageImage Average;

	for(int i=0;i < m_Header.Points;i++)
	{
		ReadFile(HF,&Average,sizeof(CAverageImage),&Rev,nullptr);

		if(Rev != sizeof(CAverageImage))
		{
			m_Header.Clear();
			m_Map.clear();

			return RW_ER;
		}

		m_Map[Average.Ampl_Aver] = Average;
	}

	SetArrayOfRMS();

	return OK;
}

//-----------------------------------------------------------------------------
void CCD_Average::SetArrayOfRMS()
{
	m_MapSize = m_Map.size();

	if(m_X)
	{
		delete [] m_X;
		m_X = nullptr;
	}

	m_X = new double[m_MapSize];

	if(m_Y)
	{
		delete [] m_Y;
		m_Y = nullptr;
	}

	m_Y = new double[m_MapSize];

	int J = 0;

	for(MAP_DOUBLE_AVERIMG::iterator It = m_Map.begin();It != m_Map.end(); It++)
	{
		m_Y[J] = It->second.Rms_Single;

		m_Y[J] = m_Y[J] * m_Y[J];
		m_X[J] = It->first;

		J += 1;
	}
}

//-----------------------------------------------------------------------------
double CCD_Average::FindRMS(const double Mean)
{
	double X0 = 0;
	double X1 = 0;
	double Y0 = 0;
	double Y1 = 0;

	if(!m_MapSize)
		return 0;

	const double RMS_Bk = m_Y[0];
	
	for(int i=1;i < m_MapSize;i++)
	{
		Y1 = m_Y[i];
		X1 = m_X[i];
		Y0 = m_Y[i-1];
		X0 = m_X[i-1];

		if(Mean < X1)
			break;
	}

	const double DX = X1 - X0;
	
	if(!DX)
		return RMS_Bk;

	const double A = (Y1-Y0) / (X1-X0);
	const double B = Y1 - A * X1;

	double RMS = A * Mean + B;

	if(RMS < RMS_Bk)
		RMS = RMS_Bk;

	return RMS;
}

//-----------------------------------------------------------------------------
void CCD_Average::PrintHeader()
{
	printf("--- Average Header ---\n");
	printf("Suppress Level = %d\n",m_Header.SuppressLevel);
	printf("Time           = %s\n",ctime(&m_Header.Time));
	printf("Rect. RMS  = <%d, %d, %d, %d>\n", m_Header.RectRms.left, 
											  m_Header.RectRms.top,
											  m_Header.RectRms.right,
											  m_Header.RectRms.bottom);
	printf ("Rect. AMPL = <%d, %d, %d, %d>\n",m_Header.RectAmpl.left, 
											  m_Header.RectAmpl.top,
											  m_Header.RectAmpl.right,
											  m_Header.RectAmpl.bottom);
	printf ("Points   = %d\n", m_Header.Points);
};
//-----------------------------------------------------------------------------
void CCD_Average::Print()
{
	PrintHeader();

	int J = 0;
	
	for(MAP_DOUBLE_AVERIMG::iterator It = m_Map.begin();It != m_Map.end();It++)
		printf("[%d] Single: <Rms=%7.3f> Average: <Ampl=%6.1f Rms=%7.3f> Stat %d\n", 
					J++, 
					It->second.Rms_Single,
					It->second.Ampl_Aver,
					It->second.Rms_Aver,
					It->second.Statistic);
}

//-----------------------------------------------------------------------------
void CCD_Average::Save()
{
	FILE * const hFile = fopen("Average.txt", "w");

	char Txt[128];
	
	int J = 0;
	
	for(MAP_DOUBLE_AVERIMG::iterator It = m_Map.begin();It != m_Map.end();It++)
	{
		sprintf (Txt, "%d \t %7.3f \t %7.1f \t %7.3f \t %d\n", 
					J++, 
					It->second.Rms_Single,
					It->second.Ampl_Aver,
					It->second.Rms_Aver,
					It->second.Statistic );

		fprintf (hFile,Txt);
	}

	fclose (hFile);
}

//-----------------------------------------------------------------------------
void CCD_Average::SaveDerivative()
{
   double Rad[] = {
  1070.0, 
  2390.6,
  3711.2, 
  5031.8, 
  6352.6, 
  7672.9, 
  8994.0, 
 10314.3, 
 11635.2, 
 12955.8 
   };
/*
   double Rad[] = {
  2482.6, 
  2891.7, 
  3352.0, 
  3826.0, 
  4291.3, 
  4740.4, 
  5116.6, 
  5749.6, 
  6188.7, 
  6741.7, 
  7102.9, 
  8271.2, 
  9296.1, 
 10225.3 
   };
*/
/*
   double Rad[] = {
0,
6.032,
12.67,
19.27,
25.94,
32.23,
38.03,
46.19,
52.24,
59.91,
64.91,
81.04,
95.18,
108.
   };
*/

	FILE * const hFile = fopen("Linear.txt", "w");

	int J = 0;

	MAP_DOUBLE_AVERIMG::iterator It;
	
	It = m_Map.begin();
	
	double Prev = It->second.Ampl_Aver;
	
	It++;
	
	char Txt[128];

	for(;It != m_Map.end();It++)
	{  
		double A = ((double)It->second.Ampl_Aver - Prev) / (Rad[J + 1] - Rad[J]); 
		
		Prev = It->second.Ampl_Aver;

		sprintf(Txt,"%d \t %8.3f \t %8.4f\n",J,It->second.Ampl_Aver,A);

		fprintf(hFile,Txt);

		J += 1;
	}

	fclose (hFile);
}

//-----------------------------------------------------------------------------
bool CCD_Average::FindFirstImage(const char * const PathIn,char * const FileName)
{
	WIN32_FIND_DATA finddata;

	char strPath[_MAX_PATH];

	strcpy(strPath,PathIn);
	strcat(strPath,"\\*.flr");

	if(m_hFindInDir)
	{
		FindClose(m_hFindInDir);
		m_hFindInDir = 0;
	}

	m_hFindInDir = ::FindFirstFile(strPath,&finddata);

	const bool bMore = (m_hFindInDir != (HANDLE) -1);
	
	if(bMore)
	{
		strcpy(FileName,PathIn);
		strcat(FileName,"\\");
		strcat(FileName,finddata.cFileName);
	}

	return bMore;
}

//-----------------------------------------------------------------------------
bool CCD_Average::FindNextImage(const char * const PathIn,char * const FileName)
{
	WIN32_FIND_DATA finddata;

	const bool bMore = ::FindNextFile(m_hFindInDir,&finddata);

	if(bMore)
	{
		strcpy(FileName,PathIn);
		strcat(FileName,"\\");
		strcat(FileName,finddata.cFileName);
	}

	return bMore;
}
