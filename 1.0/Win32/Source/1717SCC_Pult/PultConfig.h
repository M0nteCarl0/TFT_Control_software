#pragma once
#include <stdio.h>
class PultConfig
{
public:
    PultConfig(void);
    ~PultConfig(void);
    bool Open(void);
	void Close(void);
    void ReadConfig(void);
    void WriteConfig(void);
	void InitDefault(void);
   char*  GetPDavinchyPath(void);
   float  GetPCoffTime(void);
   float  GetPCoffHV(void);
   float  GetPCoffAnnodeCurrent(void);

   void  SetPDavinchyPath( char* Path);
   void  SetPCoffTime(float* Coff);
   void  SetPCoffHV(float* Coff);
   void  SetPCoffAnnodeCurrent(float* Coff);


private:
FILE* _Config;
float PCoffTime;
float PCoffHV;
float PCoffAnnodeCurrent;
};

