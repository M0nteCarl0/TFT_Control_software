#include "CalibrationConfig.h"


CalibrationConfig::CalibrationConfig(void)
{

	CmAsBegin = 0;
	CmAsEnd = 0;
	CmAsSteps = 0;
	CHV = 0;
	CPrecurent = 0;
	CCurent = 0;
	CTimeLimit = 0;
	CTypeFocuse = 0;
	CStatic = 0;
	CDellayBeatweanSnapshots = 0;
	_Config = nullptr;

	if (!Open())
	{
		InitDefault();
	}

}


CalibrationConfig::~CalibrationConfig(void)
{
}


void CalibrationConfig::SetCDellayBeatweanSnapshots(int* val)
{

	CDellayBeatweanSnapshots = *val;

}


int CalibrationConfig::GetCDellayBeatweanSnapshots(void)
{

	return CDellayBeatweanSnapshots;

}

bool CalibrationConfig::Open(void)
{
	bool Result = false;
	
	_Config =  fopen("Calibration.cfg", "r");
	if (_Config!=NULL)
	{
		Result = true;
	}

	return Result;
}

void CalibrationConfig::Close(void)
{
	if (_Config)
	{
		fclose(_Config);
	}
	
}

void CalibrationConfig::InitDefault(void)
{
	CmAsBegin = 4.00;
	CmAsEnd = 9.00;
	CmAsSteps = 7;
	CHV = 70;
	CPrecurent = 120;
	CCurent = 400;
	CTimeLimit = 555;
	CTypeFocuse = 1;
	CStatic = 2;
	CDellayBeatweanSnapshots = 1000;
	_Config = fopen("Calibration.cfg", "w");
	fprintf(_Config, "%.2f %.2f %i %i %i %i %i %i %i %i\n", CmAsBegin, CmAsEnd, CmAsSteps, CHV, CPrecurent, CCurent, CTimeLimit, CTypeFocuse, CStatic,CDellayBeatweanSnapshots);
	fflush(_Config);
	fclose(_Config);

	//WriteConfig();

}

void CalibrationConfig::ReadConfig(void)
{
	if (_Config)
	{
	
		fscanf(_Config, "%f %f %i %i %i %i %i %i %i %i\n",&CmAsBegin, &CmAsEnd, &CmAsSteps, &CHV, &CPrecurent, &CCurent,&CTimeLimit, &CTypeFocuse, &CStatic,&CDellayBeatweanSnapshots);

	}
}

void CalibrationConfig::WriteConfig(void)
{
	if (_Config)
	{
		Close();
		_Config = fopen("Calibration.cfg", "w");
		fprintf(_Config, "%.2f %.2f %i %i %i %i %i %i %i %i\n", CmAsBegin, CmAsEnd, CmAsSteps, CHV, CPrecurent, CCurent, CTimeLimit, CTypeFocuse, CStatic,CDellayBeatweanSnapshots);
		fflush(_Config);
	}
}

int CalibrationConfig::GetCStatic(void)
{
	return CStatic;
}

float CalibrationConfig::GetCmAsBegin(void)
{
	return CmAsBegin;
}

float CalibrationConfig::GetCmAsEnd(void)
{
	return CmAsEnd;
}

int CalibrationConfig::GetCmAsSteps(void)
{
	return CmAsSteps;
}

int CalibrationConfig::GetCHV(void)
{
	return CHV;
}

int CalibrationConfig::GetCPrecurent(void)
{
	return CPrecurent;
}

int CalibrationConfig::GetCCurent(void)
{
	return CCurent;
}

int CalibrationConfig::GetCTimeLimit(void)
{
	return CTimeLimit;
}

int CalibrationConfig::GetCTypeFocuse(void)
{
	return CTypeFocuse;
}

void CalibrationConfig::SetCmAsBegin(float * val)
{
	CmAsBegin = *val;
}

void CalibrationConfig::SetCmAsEnd(float * val)
{
	CmAsEnd = *val;
}

void CalibrationConfig::SetCmAsSteps(int * val)
{
	CmAsSteps = *val;
}

void CalibrationConfig::SetCHV(int * val)
{
	CHV = *val;
}

void CalibrationConfig::SetCPrecurent(int * val)
{
	CPrecurent = *val;
}

void CalibrationConfig::SetCCurent(int * val)
{
	CCurent = *val;
}

void CalibrationConfig::SetCTimeLimit(int * val)
{
	CTimeLimit = *val;
}

void CalibrationConfig::SetCTypeFocuse(int * val)
{
	CTypeFocuse = *val;
}

void CalibrationConfig::SetCStatic(int* val)
{
	CStatic = *val;

}