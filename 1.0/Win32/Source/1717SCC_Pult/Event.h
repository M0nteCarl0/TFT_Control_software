#pragma once
#include <Windows.h>

//
//  autor: Alexander Molotaliev, molotaliev@roentgenprom.ru
//
//  date: 10.11.2015
//
#ifndef _EVENT_
#define _EVENT_
class   Event
{
public:
	Event(void);
	Event(const char* SymbolicName);
	~Event(void);
	DWORD Wait(void);
	DWORD Wait(const int Timeout);
	bool Set(void);
	bool Reset(void);
	bool IsEmited(void);
private:
	HANDLE _Event;
	bool   _Emited;
};
#endif
