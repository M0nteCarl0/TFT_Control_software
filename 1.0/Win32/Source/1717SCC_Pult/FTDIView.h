#pragma once
#include <Windows.h>
#include "MXCOMM.H"
namespace Roentgepropm {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� FTDIView
	/// </summary>
	public ref class FTDIView : public System::Windows::Forms::Form
	{
	public:
		FTDIView(void)
		{
			InitializeComponent();
            SelectedDeviceID = 0;
			listBox1->Items->Clear();
			//
			//TODO: �������� ��� ������������
			//
		}


        void SetMXComm(  MxComm* MXCOMM)
        {

         _MXCOMM = MXCOMM;
         EnumFTDIDevices();

        }


        void EnumFTDIDevices(void)
        {


         size_t DeviceCount;
         listBox1->Items->Clear();
         DeviceLIst  = _MXCOMM->EnumerateFTDIDevices(&DeviceCount);

		 for (size_t i = 0; i < DeviceCount; i++)
		 {
			 listBox1->Items->Add(gcnew String( DeviceLIst[i].Description) + "\\"  + DeviceLIst[i].LocId.ToString());

		 }



        }





        void ShowDeviceInfoDetail(int ID_Device, FT_DEVICE_LIST_INFO_NODE* _DeviceLIst )
        {


			FTDi_ID->Text = _DeviceLIst[ID_Device].ID.ToString();
			FTD_Desvription->Text = gcnew  String( _DeviceLIst[ID_Device].Description);
			FTDI_LocalID->Text = _DeviceLIst[ID_Device].LocId.ToString();
            SelectedDeviceID =  _DeviceLIst[ID_Device].LocId;
            if( _MXCOMM->ConfigureByID(_DeviceLIst[ID_Device].LocId,nSpeed,nDataBits,nParity,nStopBits))
            {

             _MXCOMM->Close();
             Bussy->Checked = false;

            }
            else
            {
             Bussy->Checked = true;

            }



			

        }

		DWORD GetSelectedDeviceID(void)
		{
			return SelectedDeviceID;
		}


	protected:
     
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~FTDIView()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::Label^  label1;
    protected: 
    private: System::Windows::Forms::Label^  label2;
    private: System::Windows::Forms::Label^  label3;
    private: System::Windows::Forms::TextBox^  Baudrate;
    private: System::Windows::Forms::TextBox^  DataBit;

    private: System::Windows::Forms::TextBox^  StopBit;



    private: System::Windows::Forms::Button^  Ok;

    private: System::Windows::Forms::Button^  Cancel;

    UINT    nSpeed;   
    UINT    nDataBits; 
    UINT    nParity;   
    UINT    nStopBits;
    MxComm* _MXCOMM;
    FT_DEVICE_LIST_INFO_NODE* DeviceLIst;
	int SelectedDeviceID;
    private: System::Windows::Forms::ListBox^  listBox1;
    private: System::Windows::Forms::Label^  label4;
    private: System::Windows::Forms::GroupBox^  groupBox1;



    private: System::Windows::Forms::Label^  FTD_Desvription;

    private: System::Windows::Forms::Label^  label10;
    private: System::Windows::Forms::Label^  FTDI_LocalID;

    private: System::Windows::Forms::Label^  label8;
    private: System::Windows::Forms::Label^  FTDi_ID;



    private: System::Windows::Forms::Label^  label5;
private: System::Windows::Forms::CheckBox^  Bussy;




		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->label1 = (gcnew System::Windows::Forms::Label());
            this->label2 = (gcnew System::Windows::Forms::Label());
            this->label3 = (gcnew System::Windows::Forms::Label());
            this->Baudrate = (gcnew System::Windows::Forms::TextBox());
            this->DataBit = (gcnew System::Windows::Forms::TextBox());
            this->StopBit = (gcnew System::Windows::Forms::TextBox());
            this->Ok = (gcnew System::Windows::Forms::Button());
            this->Cancel = (gcnew System::Windows::Forms::Button());
            this->listBox1 = (gcnew System::Windows::Forms::ListBox());
            this->label4 = (gcnew System::Windows::Forms::Label());
            this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
            this->Bussy = (gcnew System::Windows::Forms::CheckBox());
            this->FTD_Desvription = (gcnew System::Windows::Forms::Label());
            this->label10 = (gcnew System::Windows::Forms::Label());
            this->FTDI_LocalID = (gcnew System::Windows::Forms::Label());
            this->label8 = (gcnew System::Windows::Forms::Label());
            this->FTDi_ID = (gcnew System::Windows::Forms::Label());
            this->label5 = (gcnew System::Windows::Forms::Label());
            this->groupBox1->SuspendLayout();
            this->SuspendLayout();
            // 
            // label1
            // 
            this->label1->AutoSize = true;
            this->label1->Location = System::Drawing::Point(13, 38);
            this->label1->Name = L"label1";
            this->label1->Size = System::Drawing::Size(55, 13);
            this->label1->TabIndex = 0;
            this->label1->Text = L"��������";
            // 
            // label2
            // 
            this->label2->AutoSize = true;
            this->label2->Location = System::Drawing::Point(13, 69);
            this->label2->Name = L"label2";
            this->label2->Size = System::Drawing::Size(65, 13);
            this->label2->TabIndex = 1;
            this->label2->Text = L"��� ������";
            // 
            // label3
            // 
            this->label3->AutoSize = true;
            this->label3->Location = System::Drawing::Point(13, 101);
            this->label3->Name = L"label3";
            this->label3->Size = System::Drawing::Size(81, 13);
            this->label3->TabIndex = 2;
            this->label3->Text = L"��������� ���";
            // 
            // Baudrate
            // 
            this->Baudrate->Location = System::Drawing::Point(100, 38);
            this->Baudrate->Name = L"Baudrate";
            this->Baudrate->Size = System::Drawing::Size(100, 20);
            this->Baudrate->TabIndex = 3;
            // 
            // DataBit
            // 
            this->DataBit->Location = System::Drawing::Point(100, 69);
            this->DataBit->Name = L"DataBit";
            this->DataBit->Size = System::Drawing::Size(100, 20);
            this->DataBit->TabIndex = 4;
            // 
            // StopBit
            // 
            this->StopBit->Location = System::Drawing::Point(100, 98);
            this->StopBit->Name = L"StopBit";
            this->StopBit->Size = System::Drawing::Size(100, 20);
            this->StopBit->TabIndex = 5;
            // 
            // Ok
            // 
            this->Ok->Location = System::Drawing::Point(12, 154);
            this->Ok->Name = L"Ok";
            this->Ok->Size = System::Drawing::Size(75, 23);
            this->Ok->TabIndex = 6;
            this->Ok->Text = L"��";
            this->Ok->UseVisualStyleBackColor = true;
            this->Ok->Click += gcnew System::EventHandler(this, &FTDIView::Ok_Click);
            // 
            // Cancel
            // 
            this->Cancel->Location = System::Drawing::Point(112, 154);
            this->Cancel->Name = L"Cancel";
            this->Cancel->Size = System::Drawing::Size(75, 23);
            this->Cancel->TabIndex = 7;
            this->Cancel->Text = L"������";
            this->Cancel->UseVisualStyleBackColor = true;
            this->Cancel->Click += gcnew System::EventHandler(this, &FTDIView::Cancel_Click);
            // 
            // listBox1
            // 
            this->listBox1->FormattingEnabled = true;
            this->listBox1->Location = System::Drawing::Point(227, 38);
            this->listBox1->Name = L"listBox1";
            this->listBox1->Size = System::Drawing::Size(132, 108);
            this->listBox1->TabIndex = 8;
            this->listBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &FTDIView::listBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this->label4->AutoSize = true;
            this->label4->Location = System::Drawing::Point(213, 22);
            this->label4->Name = L"label4";
            this->label4->Size = System::Drawing::Size(146, 13);
            this->label4->TabIndex = 9;
            this->label4->Text = L"�������� ���������� FTDI";
            // 
            // groupBox1
            // 
            this->groupBox1->Controls->Add(this->Bussy);
            this->groupBox1->Controls->Add(this->FTD_Desvription);
            this->groupBox1->Controls->Add(this->label10);
            this->groupBox1->Controls->Add(this->FTDI_LocalID);
            this->groupBox1->Controls->Add(this->label8);
            this->groupBox1->Controls->Add(this->FTDi_ID);
            this->groupBox1->Controls->Add(this->label5);
            this->groupBox1->Location = System::Drawing::Point(365, 22);
            this->groupBox1->Name = L"groupBox1";
            this->groupBox1->Size = System::Drawing::Size(359, 130);
            this->groupBox1->TabIndex = 10;
            this->groupBox1->TabStop = false;
            this->groupBox1->Text = L"���������� �� ����������";
            // 
            // Bussy
            // 
            this->Bussy->AutoCheck = false;
            this->Bussy->AutoSize = true;
            this->Bussy->Location = System::Drawing::Point(121, 18);
            this->Bussy->Name = L"Bussy";
            this->Bussy->Size = System::Drawing::Size(202, 17);
            this->Bussy->TabIndex = 8;
            this->Bussy->Text = L"������������� ������ ���������";
            this->Bussy->UseVisualStyleBackColor = true;
            // 
            // FTD_Desvription
            // 
            this->FTD_Desvription->AutoSize = true;
            this->FTD_Desvription->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->FTD_Desvription->Location = System::Drawing::Point(70, 76);
            this->FTD_Desvription->Name = L"FTD_Desvription";
            this->FTD_Desvription->Size = System::Drawing::Size(20, 15);
            this->FTD_Desvription->TabIndex = 5;
            this->FTD_Desvription->Text = L"ID";
            // 
            // label10
            // 
            this->label10->AutoSize = true;
            this->label10->Location = System::Drawing::Point(7, 76);
            this->label10->Name = L"label10";
            this->label10->Size = System::Drawing::Size(57, 13);
            this->label10->TabIndex = 4;
            this->label10->Text = L"��������";
            // 
            // FTDI_LocalID
            // 
            this->FTDI_LocalID->AutoSize = true;
            this->FTDI_LocalID->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->FTDI_LocalID->Location = System::Drawing::Point(70, 50);
            this->FTDI_LocalID->Name = L"FTDI_LocalID";
            this->FTDI_LocalID->Size = System::Drawing::Size(20, 15);
            this->FTDI_LocalID->TabIndex = 3;
            this->FTDI_LocalID->Text = L"ID";
            // 
            // label8
            // 
            this->label8->AutoSize = true;
            this->label8->Location = System::Drawing::Point(7, 47);
            this->label8->Name = L"label8";
            this->label8->Size = System::Drawing::Size(47, 13);
            this->label8->TabIndex = 2;
            this->label8->Text = L"Local ID";
            // 
            // FTDi_ID
            // 
            this->FTDi_ID->AutoSize = true;
            this->FTDi_ID->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
            this->FTDi_ID->Location = System::Drawing::Point(70, 20);
            this->FTDi_ID->Name = L"FTDi_ID";
            this->FTDi_ID->Size = System::Drawing::Size(20, 15);
            this->FTDi_ID->TabIndex = 1;
            this->FTDi_ID->Text = L"ID";
            // 
            // label5
            // 
            this->label5->AutoSize = true;
            this->label5->Location = System::Drawing::Point(7, 20);
            this->label5->Name = L"label5";
            this->label5->Size = System::Drawing::Size(18, 13);
            this->label5->TabIndex = 0;
            this->label5->Text = L"ID";
            // 
            // FTDIView
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
            this->ClientSize = System::Drawing::Size(774, 185);
            this->Controls->Add(this->groupBox1);
            this->Controls->Add(this->label4);
            this->Controls->Add(this->listBox1);
            this->Controls->Add(this->Cancel);
            this->Controls->Add(this->Ok);
            this->Controls->Add(this->StopBit);
            this->Controls->Add(this->DataBit);
            this->Controls->Add(this->Baudrate);
            this->Controls->Add(this->label3);
            this->Controls->Add(this->label2);
            this->Controls->Add(this->label1);
            this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
            this->MaximizeBox = false;
            this->Name = L"FTDIView";
            this->Text = L"��������� FTDI";
            this->groupBox1->ResumeLayout(false);
            this->groupBox1->PerformLayout();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion
    private: System::Void Ok_Click(System::Object^  sender, System::EventArgs^  e) {

                if(UINT::TryParse(Baudrate->Text,nSpeed) && UINT::TryParse(DataBit->Text,nDataBits) && UINT::TryParse(StopBit->Text ,nStopBits))
                {



                }
              

             this->DialogResult = System::Windows::Forms::DialogResult::OK;
         
}
/***********************************************************************************************/
private: System::Void Cancel_Click(System::Object^  sender, System::EventArgs^  e) {
              this->DialogResult = System::Windows::Forms::DialogResult::Cancel;
}

/***********************************************************************************************/
public:  void SetParametrsFTDI(UINT Speed,   
                               UINT  DataBits,
                               UINT  StopBitS) 
{
    nSpeed    = Speed;   
    nDataBits = DataBits; 
    nStopBits = StopBitS;


    Baudrate->Text  = nSpeed.ToString();
    DataBit->Text   = nDataBits.ToString();
    StopBit->Text   = nStopBits.ToString();



}

/***********************************************************************************************/
public: void GetParametrsFTDI( UINT% Speed,   
                               UINT%  DataBits,
                               UINT%  StopBitS)

{
   Speed    =  nSpeed ;   
   DataBits =  nDataBits; 
   StopBitS = nStopBits;


}

/***********************************************************************************************/
private: System::Void listBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	SelectedDeviceID = listBox1->SelectedIndex;
	ShowDeviceInfoDetail(SelectedDeviceID, DeviceLIst);

}
};
}
