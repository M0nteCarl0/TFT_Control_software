//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
//#include "stdafx.h"

#ifndef _FTDI_ACTION_INFO_H_
#define _FTDI_ACTION_INFO_H_

#include <Windows.h>

class SimpleActionInfo;
class FTDI_ActionInfo;

//========================================================================
//     SimpleActionInfo       SimpleActionInfo      SimpleActionInfo
//========================================================================
// this class contains an information about single FTDI action
class SimpleActionInfo
{
	public:
		SimpleActionInfo();

		SimpleActionInfo(
			const bool IncomingSuccess,
			const int IncomingNumberOfWrittenBytes,
			const int IncomingNumberOfReadBytes,
			const BYTE * const IncomingWrittenBytes,
			const BYTE * const IncomingReadBytes,
			const float IncomingActionDuration);

		SimpleActionInfo(const SimpleActionInfo & rhs);
		~SimpleActionInfo();
	
		SimpleActionInfo operator=(const SimpleActionInfo & rhs);

	public:
		bool IsSuccess;
		int NumberOfWrittenBytes;
		int NumberOfReadBytes;
		BYTE* WrittenBytes;
		BYTE* ReadBytes;
		float ActionDuration;

	private:
		void Clear();
		void Copy(const SimpleActionInfo & rhs);
		void ByDefault();
};

//========================================================================
//     FTDI_ActionInfo         FTDI_ActionInfo       FTDI_ActionInfo
//========================================================================
// This class provides an information about FTDI write actions and FTDI read actions.
class FTDI_ActionInfo
{
	public:
		FTDI_ActionInfo();

		FTDI_ActionInfo(
			const bool IncomingSuccess,
			const int IncomingNumberOfWrittenBytes,
			const int IncomingNumberOfReadBytes,
			const BYTE * const IncomingWrittenBytes,
			const BYTE * const IncomingReadBytes,
			const float IncomingActionDuration);

		FTDI_ActionInfo(const bool IncomingSuccess);

		FTDI_ActionInfo(const FTDI_ActionInfo & rhs);

		~FTDI_ActionInfo();

		FTDI_ActionInfo operator=(const FTDI_ActionInfo & rhs);
		FTDI_ActionInfo operator+(const FTDI_ActionInfo & rhs) const;
		FTDI_ActionInfo operator+(const bool & rhs) const;
		FTDI_ActionInfo operator+=(const FTDI_ActionInfo & rhs);

		operator bool() const {return IsSuccess;}
		operator int() const {return IsSuccess; }

	private:
		void Copy(const FTDI_ActionInfo & rhs);
		void Clear();
		void ByDefault();

	private:
		bool IsSuccess;
		int NumberOfActions;

		SimpleActionInfo* Actions;

	public:
		bool GetSuccess() const;

		int GetNumberOfActions() const;
		const SimpleActionInfo* GetActions() const;
		const SimpleActionInfo* GetAction(const int ActionNumber) const;
		
		bool SetActionDuration(const int ActionNumber,const float ActionDuration);
};

inline bool FTDI_ActionInfo::GetSuccess() const
{
	return IsSuccess;
}

inline int FTDI_ActionInfo::GetNumberOfActions() const
{
	return NumberOfActions;
}

inline const SimpleActionInfo* FTDI_ActionInfo::GetActions() const
{
	return Actions;
}

inline const SimpleActionInfo* FTDI_ActionInfo::GetAction(const int ActionNumber) const
{
	if(Actions != nullptr && ActionNumber < NumberOfActions)
		return Actions + ActionNumber;

	return nullptr;
}

inline bool FTDI_ActionInfo::SetActionDuration(const int ActionNumber,const float ActionDuration)
{
	if(Actions != nullptr && ActionNumber < NumberOfActions)
	{
		Actions[ActionNumber].ActionDuration = ActionDuration;
		return true;
	}

	return false;
}

bool operator==(const FTDI_ActionInfo & lhs,const FTDI_ActionInfo & rhs);
bool operator==(const bool & lhs,const FTDI_ActionInfo & rhs);
bool operator==(const FTDI_ActionInfo & lhs, const bool & rhs);

#endif //_FTDI_ACTION_INFO_H_