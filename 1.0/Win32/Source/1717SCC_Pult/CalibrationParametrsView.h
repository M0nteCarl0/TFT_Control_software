#pragma once
#include "CalibrationConfig.h"
namespace Roentgepropm {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� CalibrationParametrsView
	/// </summary>
	public ref class CalibrationParametrsView : public System::Windows::Forms::Form
	{
	public:
		CalibrationParametrsView(void)
		{
			InitializeComponent();
			Config = new CalibrationConfig();
			if (Config->Open())
			{
				Config->ReadConfig();
				CPrecurebt->Text =  Config->GetCPrecurent().ToString();
				CCurrent->Text = Config->GetCCurent().ToString();
				CAndodeHV->Text = Config->GetCHV().ToString();
				CmAsBegin->Text = Config->GetCmAsBegin().ToString();
				CmAsEnd->Text = Config->GetCmAsEnd().ToString();
			    CmAsPoints->Text = Config->GetCmAsSteps().ToString();
				CTime->Text = Config->GetCTimeLimit().ToString();
				Statists->Text = Config->GetCStatic().ToString();
				DellayBeatweanSnapshots->Text  = Config->GetCDellayBeatweanSnapshots().ToString();
				if (Config->GetCTypeFocuse())
				{

					BigFocuse->Checked = true;

				}
				else
				{
					SmallFocuse->Checked = true;
				}
				Config->Close();

			}


			//
			//TODO: �������� ��� ������������
			//
		}
	private: System::Windows::Forms::TextBox^  Statists;
	public:

	public: 
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  DellayBeatweanSnapshots;
	private: System::Windows::Forms::Label^  label9;

	protected:
		CalibrationConfig* Config;
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~CalibrationParametrsView()
		{
			if (Config)
			{
				delete Config;
			}

			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::GroupBox^  groupBox1;
    protected: 
    private: System::Windows::Forms::GroupBox^  groupBox2;
    private: System::Windows::Forms::GroupBox^  groupBox3;
    private: System::Windows::Forms::GroupBox^  groupBox4;
    private: System::Windows::Forms::GroupBox^  groupBox5;
	private: System::Windows::Forms::Button^  SaveConfig;

    private: System::Windows::Forms::Button^  button2;
    private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  CAndodeHV;
	private: System::Windows::Forms::TextBox^  CCurrent;



	private: System::Windows::Forms::TextBox^  CmAsPoints;

    private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::TextBox^  CmAsEnd;

	private: System::Windows::Forms::TextBox^  CmAsBegin;

    private: System::Windows::Forms::Label^  label6;
    private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  CTime;


	private: System::Windows::Forms::RadioButton^  SmallFocuse;

	private: System::Windows::Forms::RadioButton^  BigFocuse;

    private: System::Windows::Forms::GroupBox^  groupBox6;
	private: System::Windows::Forms::TextBox^  CPrecurebt;



	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->CAndodeHV = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->CCurrent = (gcnew System::Windows::Forms::TextBox());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->DellayBeatweanSnapshots = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->Statists = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->CmAsPoints = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->CmAsEnd = (gcnew System::Windows::Forms::TextBox());
			this->CmAsBegin = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->CTime = (gcnew System::Windows::Forms::TextBox());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->SmallFocuse = (gcnew System::Windows::Forms::RadioButton());
			this->BigFocuse = (gcnew System::Windows::Forms::RadioButton());
			this->SaveConfig = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->CPrecurebt = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->CAndodeHV);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(0, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(218, 57);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"������� ����������";
			// 
			// CAndodeHV
			// 
			this->CAndodeHV->Location = System::Drawing::Point(74, 19);
			this->CAndodeHV->Name = L"CAndodeHV";
			this->CAndodeHV->Size = System::Drawing::Size(81, 20);
			this->CAndodeHV->TabIndex = 7;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(13, 20);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(55, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"��������";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->label2);
			this->groupBox2->Controls->Add(this->CCurrent);
			this->groupBox2->Location = System::Drawing::Point(7, 242);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(251, 47);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"��� ������";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(5, 16);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(55, 13);
			this->label2->TabIndex = 9;
			this->label2->Text = L"��������";
			// 
			// CCurrent
			// 
			this->CCurrent->Location = System::Drawing::Point(67, 13);
			this->CCurrent->Name = L"CCurrent";
			this->CCurrent->Size = System::Drawing::Size(81, 20);
			this->CCurrent->TabIndex = 8;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->DellayBeatweanSnapshots);
			this->groupBox3->Controls->Add(this->label9);
			this->groupBox3->Controls->Add(this->Statists);
			this->groupBox3->Controls->Add(this->label8);
			this->groupBox3->Controls->Add(this->CmAsPoints);
			this->groupBox3->Controls->Add(this->label7);
			this->groupBox3->Controls->Add(this->CmAsEnd);
			this->groupBox3->Controls->Add(this->CmAsBegin);
			this->groupBox3->Controls->Add(this->label6);
			this->groupBox3->Controls->Add(this->label4);
			this->groupBox3->Location = System::Drawing::Point(0, 68);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(258, 115);
			this->groupBox3->TabIndex = 2;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"���";
			// 
			// DellayBeatweanSnapshots
			// 
			this->DellayBeatweanSnapshots->Location = System::Drawing::Point(165, 90);
			this->DellayBeatweanSnapshots->Name = L"DellayBeatweanSnapshots";
			this->DellayBeatweanSnapshots->Size = System::Drawing::Size(57, 20);
			this->DellayBeatweanSnapshots->TabIndex = 17;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(6, 90);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(149, 13);
			this->label9->TabIndex = 16;
			this->label9->Text = L"�������� ����� ��������";
			// 
			// Statists
			// 
			this->Statists->Location = System::Drawing::Point(74, 64);
			this->Statists->Name = L"Statists";
			this->Statists->Size = System::Drawing::Size(43, 20);
			this->Statists->TabIndex = 15;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(6, 64);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(65, 13);
			this->label8->TabIndex = 14;
			this->label8->Text = L"����������";
			// 
			// CmAsPoints
			// 
			this->CmAsPoints->Location = System::Drawing::Point(165, 16);
			this->CmAsPoints->Name = L"CmAsPoints";
			this->CmAsPoints->Size = System::Drawing::Size(43, 20);
			this->CmAsPoints->TabIndex = 13;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(122, 19);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(37, 13);
			this->label7->TabIndex = 12;
			this->label7->Text = L"�����";
			// 
			// CmAsEnd
			// 
			this->CmAsEnd->Location = System::Drawing::Point(74, 39);
			this->CmAsEnd->Name = L"CmAsEnd";
			this->CmAsEnd->Size = System::Drawing::Size(43, 20);
			this->CmAsEnd->TabIndex = 11;
			// 
			// CmAsBegin
			// 
			this->CmAsBegin->Location = System::Drawing::Point(74, 16);
			this->CmAsBegin->Name = L"CmAsBegin";
			this->CmAsBegin->Size = System::Drawing::Size(43, 20);
			this->CmAsBegin->TabIndex = 10;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(6, 39);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(55, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"��������";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 16);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(62, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"���������";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->label3);
			this->groupBox4->Controls->Add(this->CTime);
			this->groupBox4->Location = System::Drawing::Point(0, 295);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(258, 66);
			this->groupBox4->TabIndex = 3;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"�����";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 29);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(55, 13);
			this->label3->TabIndex = 10;
			this->label3->Text = L"��������";
			// 
			// CTime
			// 
			this->CTime->Location = System::Drawing::Point(74, 26);
			this->CTime->Name = L"CTime";
			this->CTime->Size = System::Drawing::Size(81, 20);
			this->CTime->TabIndex = 9;
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->SmallFocuse);
			this->groupBox5->Controls->Add(this->BigFocuse);
			this->groupBox5->Location = System::Drawing::Point(0, 367);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(258, 70);
			this->groupBox5->TabIndex = 4;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"��� ������";
			// 
			// SmallFocuse
			// 
			this->SmallFocuse->AutoSize = true;
			this->SmallFocuse->Location = System::Drawing::Point(12, 42);
			this->SmallFocuse->Name = L"SmallFocuse";
			this->SmallFocuse->Size = System::Drawing::Size(60, 17);
			this->SmallFocuse->TabIndex = 1;
			this->SmallFocuse->TabStop = true;
			this->SmallFocuse->Text = L"�����";
			this->SmallFocuse->UseVisualStyleBackColor = true;
			// 
			// BigFocuse
			// 
			this->BigFocuse->AutoSize = true;
			this->BigFocuse->Location = System::Drawing::Point(12, 19);
			this->BigFocuse->Name = L"BigFocuse";
			this->BigFocuse->Size = System::Drawing::Size(70, 17);
			this->BigFocuse->TabIndex = 0;
			this->BigFocuse->TabStop = true;
			this->BigFocuse->Text = L"�������";
			this->BigFocuse->UseVisualStyleBackColor = true;
			// 
			// SaveConfig
			// 
			this->SaveConfig->Location = System::Drawing::Point(7, 443);
			this->SaveConfig->Name = L"SaveConfig";
			this->SaveConfig->Size = System::Drawing::Size(75, 23);
			this->SaveConfig->TabIndex = 5;
			this->SaveConfig->Text = L"���������";
			this->SaveConfig->UseVisualStyleBackColor = true;
			this->SaveConfig->Click += gcnew System::EventHandler(this, &CalibrationParametrsView::SaveConfig_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(212, 445);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 6;
			this->button2->Text = L"������";
			this->button2->UseVisualStyleBackColor = true;
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->label5);
			this->groupBox6->Controls->Add(this->CPrecurebt);
			this->groupBox6->Location = System::Drawing::Point(7, 189);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(251, 47);
			this->groupBox6->TabIndex = 7;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"��� ����������";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(6, 21);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(55, 13);
			this->label5->TabIndex = 9;
			this->label5->Text = L"��������";
			// 
			// CPrecurebt
			// 
			this->CPrecurebt->Location = System::Drawing::Point(67, 21);
			this->CPrecurebt->Name = L"CPrecurebt";
			this->CPrecurebt->Size = System::Drawing::Size(81, 20);
			this->CPrecurebt->TabIndex = 8;
			// 
			// CalibrationParametrsView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(323, 480);
			this->Controls->Add(this->groupBox6);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->SaveConfig);
			this->Controls->Add(this->groupBox5);
			this->Controls->Add(this->groupBox4);
			this->Controls->Add(this->groupBox3);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Name = L"CalibrationParametrsView";
			this->Text = L"CalibrationParametrsView";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox5->ResumeLayout(false);
			this->groupBox5->PerformLayout();
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void SaveConfig_Click(System::Object^  sender, System::EventArgs^  e) {

		float mAsBegin, mAsEnd;
		int mAsPoints, AnodeHV, Current, Precurent, TimeLimit, TypeFocuse, Static,Dellay;

		float::TryParse(CmAsBegin->Text, mAsBegin);
		float::TryParse(CmAsEnd->Text, mAsEnd);

		int::TryParse(CmAsPoints->Text, mAsPoints);
		int::TryParse(CPrecurebt->Text, Precurent);
		int::TryParse(CCurrent->Text, Current);
		int::TryParse(CAndodeHV->Text, AnodeHV);
		int::TryParse(CTime->Text, TimeLimit);
		int::TryParse(Statists->Text, Static);
		int::TryParse(DellayBeatweanSnapshots->Text, Dellay);
		if (SmallFocuse->Checked)
		{

			TypeFocuse = 0;

		}

		if (BigFocuse->Checked)
		{
			TypeFocuse = 1;
		}


		if (Config->Open())
		{

			Config-> SetCmAsBegin(&mAsBegin);
			Config-> SetCmAsEnd(&mAsEnd);
			Config->SetCmAsSteps(&mAsPoints);
			Config->SetCHV(&AnodeHV);
			Config->SetCPrecurent(&Precurent);
			Config->SetCCurent(&Current);
			Config->SetCTimeLimit(&TimeLimit);
			Config->SetCTypeFocuse(&TypeFocuse);
			Config->SetCStatic(&Static);
			Config->SetCDellayBeatweanSnapshots(&Dellay);
			Config->WriteConfig();
			Config->Close();
			this->Close();


		}






	}
};
}
