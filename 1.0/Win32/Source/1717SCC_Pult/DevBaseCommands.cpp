//----------------------------------------------------------------------------
//    Create by Novikov
//    data 23.11.2011
//----------------------------------------------------------------------------
//    Modified: Kalendarev V
//    date 06.12.2012
//----------------------------------------------------------------------------
//    Modified: Kalendarev V
//    date 19.08.2013
//----------------------------------------------------------------------------
//#include "stdafx.h"

#include "DevBaseCommands.h"
#include "stopwatch_win.h"

#include <stdio.h>
#include <time.h>
#include <string.h>


CRITICAL_SECTION DevBaseCommands::CrSection;

//----------------------------------------------------------------------------
DevBaseCommands::DevBaseCommands()
	:m_Comm(nullptr),m_FlgPrint(false),WaitingIsProhibitedFlag(FALSE)
{
	InitializeCriticalSection(&CrSection);
};

//----------------------------------------------------------------------------
DevBaseCommands::~DevBaseCommands()
{
	DeleteCriticalSection(&CrSection);
};

//----------------------------------------------------------------------------
void DevBaseCommands::SetComm(MxComm * const Addr)
{
	EnterCriticalSection(&CrSection);
	
		m_Comm = Addr;

	LeaveCriticalSection(&CrSection);
};

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::WriteReg(const BYTE * const Reg) const
{
   if (m_Comm == nullptr)
	   return false;

   StopWatchWin Watch;

   EnterCriticalSection(&CrSection);

	   Watch.start();

	   FTDI_ActionInfo WriteInfo = m_Comm->Write((BYTE*) Reg, 2);
   
	   WriteInfo.SetActionDuration(0,Watch.getTime());

   LeaveCriticalSection(&CrSection);

   if (m_FlgPrint)
	   printf ("Write: Reg = %x  %x, Ret = %d, Time = %5.1f\n", Reg[0], Reg[1], WriteInfo.GetSuccess(), WriteInfo.GetAction(0)->ActionDuration);

   return WriteInfo;
};

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::WriteReg(const BYTE Reg,const BYTE Data) const
{
   if (m_Comm == nullptr)
	   return false;

   BYTE RegW[2] = {Reg, Data};

   StopWatchWin Watch;

   EnterCriticalSection(&CrSection);

	   Watch.start();

	   FTDI_ActionInfo WriteInfo = m_Comm->Write((BYTE*)RegW, 2);
   
	   WriteInfo.SetActionDuration(0,Watch.getTime());

   LeaveCriticalSection(&CrSection);

   if (m_FlgPrint)
      printf ("Write: Reg = %x  %x, Ret = %d, Time = %5.1f\n", RegW[0], RegW[1], WriteInfo.GetSuccess(), WriteInfo.GetAction(0)->ActionDuration);

   return WriteInfo;
};

FTDI_ActionInfo DevBaseCommands::WriteReg(const BYTE * const AFewBytesToWrite,const int NumberOfBytesToWrite) const
{
	if (m_Comm == nullptr || AFewBytesToWrite == nullptr)
		return false;

	StopWatchWin Watch;

	EnterCriticalSection(&CrSection);

		Watch.start();

		FTDI_ActionInfo WriteInfo = m_Comm->Write((BYTE*)AFewBytesToWrite, NumberOfBytesToWrite);
   
		WriteInfo.SetActionDuration(0,Watch.getTime());

	LeaveCriticalSection(&CrSection);

	if (m_FlgPrint)
	{
		printf("Write: Reg = ");
		
		for(int i=0;i<NumberOfBytesToWrite;i++)
			printf("%x ",AFewBytesToWrite[i]);

		printf(", Ret = %d, Time = %5.1f\n",WriteInfo.GetSuccess(),WriteInfo.GetAction(0)->ActionDuration);
	}

	return WriteInfo;
}

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::ReadReg(const BYTE Reg, BYTE * const RegR) const
{
	if (m_Comm == nullptr)
		return false;

	BYTE DataW[2] = {0x81, Reg};
	
	StopWatchWin Watch;

	EnterCriticalSection(&CrSection);

		Watch.start();
		
		FTDI_ActionInfo WriteInfo = WriteReg(DataW);

		WriteInfo.SetActionDuration(0,Watch.getTime());

		unsigned long ByteReceived = 0;

		if (WriteInfo == false)
		{
			LeaveCriticalSection(&CrSection);
			return WriteInfo;
		}

		Watch.start();

		FTDI_ActionInfo ReadInfo = m_Comm->Read(RegR, 2, &ByteReceived);

		ReadInfo.SetActionDuration(0,Watch.getTime());

	LeaveCriticalSection(&CrSection);

	const FTDI_ActionInfo SummaryInfo = WriteInfo + ReadInfo;

	if (m_FlgPrint)
		printf ("Read (%x): Reg = %x  %x, Ret = %d, Time = %5.1f\n", Reg, RegR[0], RegR[1], SummaryInfo.GetSuccess(), SummaryInfo.GetAction(0)->ActionDuration);

	return SummaryInfo;
};

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::ReadRegDat(const BYTE Reg, BYTE * const Data) const
{
   BYTE RegR[2];
   FTDI_ActionInfo ReadInfo = ReadReg(Reg, RegR);

   *Data= RegR [1];

   return ReadInfo;
};

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::SetRegBit(const BYTE Reg,const BYTE BitToSet,const bool BitValueToSet) const
{
	BYTE RegValue = 0x00;

	EnterCriticalSection(&CrSection);

		const FTDI_ActionInfo ReadRegInfo = ReadRegDat(Reg,&RegValue);

		if(ReadRegInfo == false)
		{
			LeaveCriticalSection(&CrSection);
			return ReadRegInfo;
		}

		const FTDI_ActionInfo SetRegInfo = BitValueToSet == true ? WriteReg(Reg,RegValue | BitToSet) : WriteReg(Reg,RegValue & ~BitToSet);

	LeaveCriticalSection(&CrSection);

	return ReadRegInfo + SetRegInfo;
}

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::Send(const BYTE * const WriteBuffer, const int NumberOfBytesThatAreGoingToBeSent) const
{
	if (m_Comm == nullptr)
		return false;

	if(WriteBuffer == nullptr || NumberOfBytesThatAreGoingToBeSent <= 0)
		return false;

	StopWatchWin Watch;

	EnterCriticalSection(&CrSection);

		Watch.start();

		FTDI_ActionInfo WriteInfo = m_Comm->Write((BYTE*) WriteBuffer, NumberOfBytesThatAreGoingToBeSent);
   
		WriteInfo.SetActionDuration(0,Watch.getTime());

	LeaveCriticalSection(&CrSection);

	return WriteInfo;
}

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::Receive(BYTE * const ReadBuffer, const int NumberOfBytesThatAreGoingToBeReceived) const
{
	if (m_Comm == nullptr)
		return false;

	if(ReadBuffer == nullptr)
		return false;

	unsigned long NumberOfReallyReceivedBytes = 0;
	
	StopWatchWin Watch;
	
	EnterCriticalSection(&CrSection);

		Watch.start();

		FTDI_ActionInfo ReadInfo = m_Comm->Read(ReadBuffer, NumberOfBytesThatAreGoingToBeReceived, &NumberOfReallyReceivedBytes);
	
		ReadInfo.SetActionDuration(0,Watch.getTime());

	LeaveCriticalSection(&CrSection);

	return ReadInfo + FTDI_ActionInfo(NumberOfBytesThatAreGoingToBeReceived == NumberOfReallyReceivedBytes ? true : false);
}

FTDI_ActionInfo DevBaseCommands::SendCommandAndReceiveItAsAProof(const BYTE Address,const BYTE CommandToSend) const
{
	EnterCriticalSection(&CrSection);

		const FTDI_ActionInfo WriteInfo = WriteReg(Address,CommandToSend);

		BYTE ReadBuffer[2] = {0,0};
		FTDI_ActionInfo ReadInfo = Receive(ReadBuffer,2);

	LeaveCriticalSection(&CrSection);

	FTDI_ActionInfo TheSame(true);
	if( !(ReadBuffer[0] == Address && ReadBuffer[1] == CommandToSend) )
		TheSame = false;

	return WriteInfo + ReadInfo + TheSame;
}

//----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::SendCommandAndReceiveItAsAProof(const BYTE * const CommandData,const int CommandDataLength) const
{
	if(CommandData == nullptr || CommandDataLength <= 0)
		return false;

	BYTE * const ReadBuffer = new BYTE[CommandDataLength];
	memset(ReadBuffer,0,CommandDataLength);

	EnterCriticalSection(&CrSection);

		FTDI_ActionInfo SendInfo = Send(CommandData,CommandDataLength);
		FTDI_ActionInfo ReceiveInfo = Receive(ReadBuffer,CommandDataLength);
	
	LeaveCriticalSection(&CrSection);

	FTDI_ActionInfo TheSame(true);
	for(int i = 0;i<CommandDataLength;i++)
		if(CommandData[i] != ReadBuffer[i])
		{
			TheSame = false;
			break;
		}

	delete ReadBuffer;

	return SendInfo + ReceiveInfo + TheSame;
}

//----------------------------------------------------------------------------
bool DevBaseCommands::SetReg(const BYTE * const Reg) const
{
	EnterCriticalSection(&CrSection);

		bool Ret = WriteReg(Reg);

		if(Ret == false)
		{
			LeaveCriticalSection(&CrSection);
			return Ret;
		}

		BYTE RegR[2];
		Ret = ReadReg(Reg[0], RegR); 

	LeaveCriticalSection(&CrSection);

	if (Reg[1] != RegR[1]) 
		Ret = false;

	return Ret;
};

//----------------------------------------------------------------------------
bool DevBaseCommands::SetReg(const BYTE Reg,const BYTE Data) const
{
	EnterCriticalSection(&CrSection);

		bool Ret = WriteReg(Reg, Data);
		
		if(Ret == false)
		{
			LeaveCriticalSection(&CrSection);
			return Ret;
		}

		BYTE RegR[2];
		Ret = ReadReg(Reg, RegR);

	LeaveCriticalSection(&CrSection);

	if(Data != RegR[1]) 
		Ret = false;

	return Ret;
};

//-----------------------------------------------------------------------------
bool  DevBaseCommands::SetValue37(const int Value,const BYTE Reg_LO,const BYTE Reg_HO) const
{
	const BYTE Data1 = Value & 0x07;
	const BYTE Data2 = (Value & 0x3F8) >> 3;

	EnterCriticalSection(&CrSection);

		bool Ret = SetReg(Reg_LO, Data1);
		if(Ret == true)
			Ret = SetReg(Reg_HO, Data2);

	LeaveCriticalSection(&CrSection);

	return Ret;
};

//-----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::WriteValue37(const int Value,const BYTE Reg_LO,const BYTE Reg_HO) const
{
	const BYTE Data1 = Value & 0x07;
	const BYTE Data2 = (Value & 0x3F8) >> 3;

	if (m_FlgPrint == true)
		printf ("      WriteValue37 = %d \n", Value);

	EnterCriticalSection(&CrSection);

		const FTDI_ActionInfo WriteReg3Info = WriteReg(Reg_LO, Data1);

		if (WriteReg3Info == false)
		{
			LeaveCriticalSection(&CrSection);
			return WriteReg3Info;
		}

		const FTDI_ActionInfo WriteReg7Info = WriteReg(Reg_HO, Data2);

	LeaveCriticalSection(&CrSection);

	return WriteReg3Info + WriteReg7Info;
};

//-----------------------------------------------------------------------------
FTDI_ActionInfo  DevBaseCommands::ReadValue37(int * const Value,const BYTE Reg_LO,const BYTE Reg_HO) const
{
	BYTE Data1;
	BYTE Data2;

	EnterCriticalSection(&CrSection);

		const FTDI_ActionInfo ReadReg3Info = ReadRegDat(Reg_LO, &Data1);

		if (ReadReg3Info == false)
		{
			LeaveCriticalSection(&CrSection);
			return ReadReg3Info;
		}

		const FTDI_ActionInfo ReadReg7Info = ReadRegDat(Reg_HO, &Data2);
   
	LeaveCriticalSection(&CrSection);

	if (ReadReg7Info == true)
	{
		*Value = Data1 & 0x07;
		*Value |= (Data2 & 0x7F) << 3;
	}

	if (m_FlgPrint)
		printf ("      ReadValue37 = %d \n", *Value);

	return ReadReg3Info + ReadReg7Info;
};

//-----------------------------------------------------------------------------
FTDI_ActionInfo DevBaseCommands::WriteValue77(const int Value,const BYTE Reg_LO,const BYTE Reg_HO) const
{
	const BYTE Data1 = Value & 0x7F;
	const BYTE Data2 = (Value & 0x3F80) >> 7;

	if (m_FlgPrint == true)
		printf ("      WriteValue77 = %d \n", Value);

	EnterCriticalSection(&CrSection);

		const FTDI_ActionInfo WriteReg1Info = WriteReg (Reg_LO, Data1);

		if (WriteReg1Info == false)
		{
			LeaveCriticalSection(&CrSection);
			return WriteReg1Info;
		}

		const FTDI_ActionInfo WriteReg2Info = WriteReg (Reg_HO, Data2);

	LeaveCriticalSection(&CrSection);

	return WriteReg1Info + WriteReg2Info;
};

//-----------------------------------------------------------------------------
FTDI_ActionInfo  DevBaseCommands::ReadValue77(int * const Value,const BYTE Reg_LO,const BYTE Reg_HO) const
{
	*Value = 0;

	BYTE Data1;
	BYTE Data2;

	EnterCriticalSection(&CrSection);

		const FTDI_ActionInfo ReadReg1Info = ReadRegDat(Reg_LO, &Data1);

		if (ReadReg1Info == false)
		{
			LeaveCriticalSection(&CrSection);
			return ReadReg1Info;
		}

		const FTDI_ActionInfo ReadReg2Info = ReadRegDat(Reg_HO, &Data2);

	LeaveCriticalSection(&CrSection);

	if (ReadReg2Info == true)
	{
		*Value = Data1 & 0x7F;
		*Value |= (Data2 & 0x7F) << 7;
	}

	if (m_FlgPrint)
		printf ("      ReadValue77 = %d \n", *Value);

	return ReadReg1Info + ReadReg2Info;
};

//----------------------------------------------------------------------------
bool DevBaseCommands::WaitBits(BYTE Reg, int WaitTime, BYTE Mask, int * const TimeOut,const int DelayBetweenRequests) const
{
	BYTE Data;
	bool Ret = false;
	clock_t T1;

	clock_t T0 = clock();

	int J = 0;
	do
	{
		if(WaitingIsProhibitedFlag == TRUE)
		{
			Ret = false;
			break;
		}

		Ret = ReadRegDat (Reg, &Data);

		T1 = clock ();

		if (Ret == false)
			break;
		if (Mask & Data)
			break;

		J++;

		if(DelayBetweenRequests > 0)
			Sleep(DelayBetweenRequests);
	} 
	while (T1 < T0 + WaitTime);

	if(TimeOut != nullptr)
		*TimeOut = T1 - T0;

	if(m_FlgPrint)
		printf ("J = %d  Data = %x\n", J, Data);
   
	return Ret;
};

//----------------------------------------------------------------------------
bool DevBaseCommands::WaitState(BYTE Reg, int WaitTime, BYTE NeededState, int * const TimeOut,const int DelayBetweenRequests) const
{
	BYTE Data;
	bool Ret = false;
	clock_t T1;

	clock_t T0 = clock();

	int J = 0;
	do
	{
		if(WaitingIsProhibitedFlag == TRUE)
		{
			Ret = false;
			break;
		}

		Ret = ReadRegDat(Reg, &Data);

		T1 = clock ();

		if (Ret == false)
			break;
		if (NeededState == Data)
			break;
		J++;

		if(DelayBetweenRequests > 0)
			Sleep(DelayBetweenRequests);
	} 
	while (T1 < T0 + WaitTime);

	if(TimeOut != nullptr)
		*TimeOut = T1 - T0;

	if(m_FlgPrint)
		printf ("J = %d  Data = %x\n", J, Data);
   
	return Ret;
}

//----------------------------------------------------------------------------
bool DevBaseCommands::WaitBitState(BYTE Reg, int WaitTime, BYTE Mask, bool NeededState, int * const TimeOut,const int DelayBetweenRequests) const
{
	BYTE Data;
	bool Ret = false;
	clock_t T1;

	clock_t T0 = clock();

	int J = 0;
	do
	{
		if(WaitingIsProhibitedFlag == TRUE)
		{
			Ret = false;
			break;
		}

		Ret = ReadRegDat(Reg, &Data);

		T1 = clock ();

		if (Ret == false)
			break;

		if(NeededState == true)
		{
			if (Mask & Data)
				break;
		}
		else if(!(Mask & Data))
			break;
		J++;

		if(DelayBetweenRequests > 0)
			Sleep(DelayBetweenRequests);
	} 
	while (T1 < T0 + WaitTime);

	if(TimeOut != nullptr)
		*TimeOut = T1 - T0;

	if(m_FlgPrint)
		printf ("J = %d  Data = %x\n", J, Data);
   
	return Ret;
}

//----------------------------------------------------------------------------
LONG DevBaseCommands::PermitWaiting()
{
	while(InterlockedExchange(&WaitingIsProhibitedFlag,FALSE) == TRUE);

	return WaitingIsProhibitedFlag;
}

//----------------------------------------------------------------------------
LONG DevBaseCommands::ProhibitWaiting()
{
	while(InterlockedExchange(&WaitingIsProhibitedFlag,TRUE) == FALSE);

	return WaitingIsProhibitedFlag;
}

void DevBaseCommands::Delay(const float DelayInMilliseconds)
{
	StopWatchWin Watch;

	Watch.start();

	while(Watch.getTime() < DelayInMilliseconds)
		;
}