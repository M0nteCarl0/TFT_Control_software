#include "PultConfig.h"
#include <string>
char PDavinchyPath[255];
PultConfig::PultConfig(void)
{
	_Config = NULL;
   // PDavinchyPath = NULL;
	PCoffTime = 0;
	PCoffHV = 0;
	PCoffAnnodeCurrent = 0;
	if (!Open())
	{
		InitDefault();
	}
}


PultConfig::~PultConfig(void)
{
	if(_Config)
	{
		Close();

	}
}

bool PultConfig::Open(void)
{
	bool Result = false;

	_Config = fopen("Pult.cfg", "r");
	if (_Config != NULL)
	{
		Result = true;
	}

	return Result;
}

void PultConfig::Close(void)
{
	if (_Config != NULL)
	{
		fclose(_Config);
	}
	
}

void PultConfig::ReadConfig(void)
{
	if (_Config != NULL)
	{
		fscanf(_Config, "%s\n",&PDavinchyPath);
		fscanf(_Config, "%f\n", &PCoffTime);
		fscanf(_Config, "%f\n", &PCoffHV);
		fscanf(_Config, "%f\n", &PCoffAnnodeCurrent);
	}
}

void PultConfig::WriteConfig(void)
{
	Close();
	_Config = fopen("Pult.cfg", "w");
	if (_Config != NULL)
	{
		fprintf(_Config, "%s\n", PDavinchyPath);
		fprintf(_Config, "%.2f\n", PCoffTime);
		fprintf(_Config, "%.2f\n", PCoffHV);
		fprintf(_Config, "%.2f\n", PCoffAnnodeCurrent);
		fflush(_Config);
		Close();

	}

}

void PultConfig::InitDefault(void)
{
	PCoffTime = 14.20;
	PCoffHV = 7.00;
	PCoffAnnodeCurrent = 1.00;
	strcpy(PDavinchyPath, "C:\Davinci");
	WriteConfig();
	PCoffTime = 0;
	PCoffHV = 0;
	PCoffAnnodeCurrent = 0;
	
}

char * PultConfig::GetPDavinchyPath(void)
{
	return PDavinchyPath;
}

float PultConfig::GetPCoffTime(void)
{
	return PCoffTime;
}

float PultConfig::GetPCoffHV(void)
{
	return PCoffHV;
}

float PultConfig::GetPCoffAnnodeCurrent(void)
{
	return PCoffAnnodeCurrent;
}

void PultConfig::SetPDavinchyPath(char * Path)
{
	if (Path)
	{
		strcpy(PDavinchyPath, Path);
	}

}

void PultConfig::SetPCoffTime(float* Coff)
{
	if (Coff)
	{
		PCoffTime = *Coff;
	}
}

void PultConfig::SetPCoffHV(float* Coff)
{
	if (Coff)
	{
		PCoffHV = *Coff;
	}
}

void PultConfig::SetPCoffAnnodeCurrent(float* Coff)
{
	if (Coff)
	{
		PCoffAnnodeCurrent =*Coff;
	}

}
