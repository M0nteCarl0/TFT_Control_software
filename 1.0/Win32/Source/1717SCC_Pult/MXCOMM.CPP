//----------------------------------------------------------------------------
//    Create by Novikov
//    data 29.06.97
//    last data 29.06.97
//----------------------------------------------------------------------------
//    Modified: Vitaly Kalendarev
//    date 06.12.2012
//    date 19.05.2015
//----------------------------------------------------------------------------
#include "stdafx.h"
#include <time.h>
#include "mxcomm.h"
//#include <process.h>
#include <stdio.h>
#include <string.h>


CRITICAL_SECTION MxComm::CrSection;
int MxComm::nobjects;

static FILE* flog;

static void PrintFTDI_ActionInfo(FILE * const f,const FTDI_ActionInfo & Info)
{
	if(f == nullptr)
		return;

	for(int i=0;i<Info.GetNumberOfActions();i++)
	{
		const SimpleActionInfo * const Action = Info.GetAction(i);
		
		if(Action->NumberOfWrittenBytes > 0)
		{
			fprintf(f,"  Written(%d):",Action->NumberOfWrittenBytes);
			if(Action->WrittenBytes != nullptr)
				for(int k=0;k < Action->NumberOfWrittenBytes;k++)
					fprintf(f,"\\%x",Action->WrittenBytes[k]);
			fprintf(f,", Time=%f, Success=%s\n",Action->ActionDuration,Action->IsSuccess == true ? "TRUE" : "FALSE");
		}
		else if(Action->NumberOfReadBytes > 0)
		{
			fprintf(f,"  Read(%d):",Action->NumberOfReadBytes);
			if(Action->ReadBytes != nullptr)
				for(int k=0;k < Action->NumberOfReadBytes;k++)
					fprintf(f,"\\%x",Action->ReadBytes[k]);
			fprintf(f,", Time=%f, Success=%s\n",Action->ActionDuration,Action->IsSuccess == true ? "TRUE" : "FALSE");
		}
		else
		{
			fprintf(f,"  Written(%d), Read(%d),",Action->NumberOfWrittenBytes,Action->NumberOfReadBytes);
			fprintf(f," Time=%f, Success=%s\n",Action->ActionDuration,Action->IsSuccess == true ? "TRUE" : "FALSE");
		}
	}
}

MxComm::MxComm()
	:m_FTDI(nullptr)
{
	if(nobjects == 0)
	{
   		InitializeCriticalSection(&CrSection);

		flog = fopen("FTDI-RS-485.log","w");
	}

	nobjects += 1;
}

MxComm::~MxComm()
{
	Close();
	
	nobjects -= 1;

	if(nobjects == 0)
	{
		DeleteCriticalSection(&CrSection);

		if(flog != nullptr)
		{
			fclose(flog);
			flog = nullptr;
		}
	}
}


bool MxComm::Configure(const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits)
{
	EnterCriticalSection(&CrSection);

	Close();

	FT_STATUS ftStatus;
	DWORD devIndex = 0; // first device

	ftStatus = FT_ListDevices(0, m_Buff, FT_LIST_BY_INDEX|FT_OPEN_BY_DESCRIPTION);

	if(ftStatus == FT_OK)
		fprintf(flog,"FT_OK\n"); //("The number of Devices\n");

	DWORD dwFlags =  FILE_ATTRIBUTE_NORMAL|FT_OPEN_BY_DESCRIPTION; 

	Close();

	m_FTDI = FT_W32_CreateFile(m_Buff, GENERIC_READ|GENERIC_WRITE, 0, 0,
							OPEN_EXISTING,
							dwFlags,
							0);

	if(m_FTDI == INVALID_HANDLE_VALUE)
	{
		m_FTDI = nullptr;
		goto label_FAILURE;
	}

	if(m_FTDI != nullptr)
	{
		FTDCB ftDCB;

		if (FT_W32_GetCommState (m_FTDI, &ftDCB))
		{ 
			ftDCB.BaudRate = nSpeed;
			ftDCB.ByteSize = nDataBits;
			ftDCB.Parity = nParity;
			ftDCB.StopBits = nStopBits;

			
			if(!FT_W32_SetCommState(m_FTDI,&ftDCB))
				goto label_FAILURE;
		}
		else
			goto label_FAILURE;
	}

	FT_W32_PurgeComm (m_FTDI, PURGE_RXCLEAR | PURGE_TXCLEAR );
	FT_Purge (m_FTDI, FT_PURGE_RX | FT_PURGE_TX);
	SetTimeout(120);
	LeaveCriticalSection(&CrSection);
	return true;

label_FAILURE:
	LeaveCriticalSection(&CrSection);
	return false;
}

bool MxComm::Close()
{
	EnterCriticalSection(&CrSection);

		if(m_FTDI != nullptr)
		{
			FT_W32_CloseHandle(m_FTDI);
			m_FTDI = nullptr;
		}

	LeaveCriticalSection(&CrSection);

	return true;
}

FTDI_ActionInfo MxComm::Read(BYTE * const Buffer, const DWORD BytesToRead, DWORD * const BytesReturned) const
{
	if(m_FTDI == nullptr)
		return false;

	OVERLAPPED ov = {0};

	EnterCriticalSection(&CrSection);


		const int Success = FT_W32_ReadFile(m_FTDI, Buffer, BytesToRead, BytesReturned, &ov);
	//	FT_W32_PurgeComm(m_FTDI, PURGE_RXCLEAR | PURGE_TXCLEAR);
		//FT_Purge(m_FTDI, FT_PURGE_RX | FT_PURGE_TX);



	LeaveCriticalSection(&CrSection);

	if(Success == false)
	{
		PrintFTDI_ActionInfo(flog,FTDI_ActionInfo(false,0,*BytesReturned,nullptr,Buffer,0));
		return FTDI_ActionInfo(false,0,*BytesReturned,nullptr,Buffer,0);
	}

	PrintFTDI_ActionInfo(flog,FTDI_ActionInfo(BytesToRead == *BytesReturned,0,*BytesReturned,nullptr,Buffer,0));
	return FTDI_ActionInfo(BytesToRead == *BytesReturned,0,*BytesReturned,nullptr,Buffer,0);
}

FTDI_ActionInfo MxComm::Write(BYTE * const Buffer, const DWORD BytesToWrite) const
{
	if(m_FTDI == nullptr)
		return false;

	DWORD BytesReturned = 0;
	OVERLAPPED ov = {0};

	EnterCriticalSection(&CrSection);
	
	//FT_W32_PurgeComm(m_FTDI, PURGE_RXCLEAR | PURGE_TXCLEAR);
	//FT_Purge(m_FTDI, FT_PURGE_RX | FT_PURGE_TX);


		const int Success = FT_W32_WriteFile(m_FTDI, Buffer, BytesToWrite, &BytesReturned, &ov);

	LeaveCriticalSection(&CrSection);

	if(Success == false)
	{
		PrintFTDI_ActionInfo(flog,FTDI_ActionInfo(false,0,BytesReturned,nullptr,Buffer,0));

		return FTDI_ActionInfo(false,0,BytesReturned,nullptr,Buffer,0);
	}

	PrintFTDI_ActionInfo(flog,FTDI_ActionInfo(BytesToWrite == BytesReturned,BytesReturned,0,Buffer,nullptr,0));

	return FTDI_ActionInfo(BytesToWrite == BytesReturned,BytesReturned,0,Buffer,nullptr,0);
}

void MxComm::Delay(const int Del)
{
      const int T0 = clock();

      while(clock() < T0 + Del);
}

void MxComm::PurgeTX() const
{
	if(m_FTDI == nullptr)
		return;

	EnterCriticalSection(&CrSection);

		FT_W32_PurgeComm(m_FTDI, PURGE_TXCLEAR );
		FT_Purge(m_FTDI, FT_PURGE_TX);

	LeaveCriticalSection(&CrSection);
}

void MxComm::PurgeRX() const
{
	if(m_FTDI == nullptr)
		return;

	EnterCriticalSection(&CrSection);

		FT_W32_PurgeComm(m_FTDI, PURGE_RXCLEAR);
		FT_Purge(m_FTDI, FT_PURGE_RX);

	LeaveCriticalSection(&CrSection);
}

void MxComm::SetTimeout (const DWORD Constant)
{
	if(m_FTDI == nullptr)
		return;

	const DWORD Data = 0xFFFFFFFF;
	FTTIMEOUTS commTimeOuts;

	commTimeOuts.ReadIntervalTimeout	     = Constant;
	commTimeOuts.ReadTotalTimeoutMultiplier   = 0;
	commTimeOuts.ReadTotalTimeoutConstant     = Constant;
	commTimeOuts.WriteTotalTimeoutMultiplier  = 0;
	commTimeOuts.WriteTotalTimeoutConstant    = Data;

	EnterCriticalSection(&CrSection);

	FT_W32_SetCommTimeouts(m_FTDI, &commTimeOuts);

	LeaveCriticalSection(&CrSection);
}


	bool MxComm::IsFTDIBoxAvalible(void)
	{

		bool Res = true;
		if(m_FTDI == nullptr)
		{

			 Res = false;
		}
		
		return Res;

	}

FT_DEVICE_LIST_INFO_NODE*  MxComm::EnumerateFTDIDevices(size_t* CountConectedDevices)
{
FT_STATUS State;
DWORD CountOfDevce;
*CountConectedDevices  = 0;
FT_DEVICE_LIST_INFO_NODE* ListOfDevices = NULL;

 if( FT_CreateDeviceInfoList(&CountOfDevce)  == FT_OK)
 {

 if(CountOfDevce >0 )
 {

    ListOfDevices = new FT_DEVICE_LIST_INFO_NODE[CountOfDevce];
    if(FT_GetDeviceInfoList(ListOfDevices,&CountOfDevce) == FT_OK)
    {

    *CountConectedDevices = CountOfDevce;

    }


 }



 }

return ListOfDevices;

}



bool   MxComm:: AutoConnectForFreeModule(const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits)
{
  bool Res = false;
  size_t DeviceCount;
 
  FT_DEVICE_LIST_INFO_NODE* DeviceList  = EnumerateFTDIDevices(&DeviceCount);
  if(DeviceList!=NULL)
  {

  for(size_t i = 0;i<DeviceCount;i++)
  {

    if(DeviceList[i].LocId!=0)
    {
    
     ConfigureByID(DeviceList[i].LocId  ,nSpeed,nDataBits,nParity, nStopBits);
     Res = true;
    break;
    }


  }



  }





  return Res;

}






int  MxComm:: SimpleWrite(BYTE * const Buffer, const DWORD BytesToWrite)
{

   int Result;
   if(m_FTDI == nullptr)
		return false;

	DWORD BytesReturned = 0;
	OVERLAPPED ov = {0};

	EnterCriticalSection(&CrSection);
	
	Result = FT_W32_WriteFile(m_FTDI, Buffer, BytesToWrite, &BytesReturned, &ov);
	//PurgeTX();
 
	LeaveCriticalSection(&CrSection);


    return Result;

}
 
 int MxComm:: SimpleRead(BYTE * const Buffer, const DWORD BytesToRead, DWORD * const BytesReturned)
 {


  int Result;
   if(m_FTDI == nullptr)
		return false;

	OVERLAPPED ov = {0};

	EnterCriticalSection(&CrSection);
	
       
		Result = FT_W32_ReadFile(m_FTDI, Buffer, BytesToRead, BytesReturned, &ov);
		//PurgeRX();
      
	LeaveCriticalSection(&CrSection);

    return Result;

 }




bool  MxComm:: ConfigureByID(const UINT ID  ,const UINT nSpeed, const UINT nDataBits, const UINT nParity, const UINT nStopBits)
{


	EnterCriticalSection(&CrSection);

	Close();

	FT_STATUS ftStatus;
	DWORD devIndex = 0; // first device

	//ftStatus = FT_ListDevices(( ID, m_Buff, FT_LIST_BY_INDEX|FT_OPEN_BY_DESCRIPTION);

	//if(ftStatus == FT_OK)
		//fprintf(flog,"FT_OK\n"); //("The number of Devices\n");

	DWORD dwFlags =  FILE_ATTRIBUTE_NORMAL|FT_OPEN_BY_LOCATION; 

	Close();

	m_FTDI = FT_W32_CreateFile((LPCTSTR)ID, GENERIC_READ|GENERIC_WRITE, 0, 0,
							OPEN_EXISTING,
							dwFlags,
							0);

	if(m_FTDI == INVALID_HANDLE_VALUE)
	{
		m_FTDI = nullptr;
		goto label_FAILURE;
	}

	if(m_FTDI != nullptr)
	{
		FTDCB ftDCB;

		if (FT_W32_GetCommState (m_FTDI, &ftDCB))
		{ 
			ftDCB.BaudRate = nSpeed;
			ftDCB.ByteSize = nDataBits;
			ftDCB.Parity = nParity;
			ftDCB.StopBits = nStopBits;

			
			if(!FT_W32_SetCommState(m_FTDI,&ftDCB))
				goto label_FAILURE;
		}
		else
			goto label_FAILURE;
	}

	FT_W32_PurgeComm (m_FTDI, PURGE_RXCLEAR | PURGE_TXCLEAR );
	FT_Purge (m_FTDI, FT_PURGE_RX | FT_PURGE_TX);
	SetTimeout(120);
	LeaveCriticalSection(&CrSection);
	return true;

label_FAILURE:
	LeaveCriticalSection(&CrSection);
	return false;


}




/*
MxComm * _mxComm;
//----------------------------------------------------------------------------
MxComm::MxComm (int Num, DWORD Access)
{
   char Buff[64];
   BOOL Error;
   _Num = Num;

   sprintf (Buff, "COM%d", Num);
   _hCom = CreateFile (Buff,
		       Access,
		       0,
		       NULL,
		       OPEN_EXISTING,
		       FILE_FLAG_OVERLAPPED,
		       NULL);
   if (_hCom == INVALID_HANDLE_VALUE){
      sprintf (Buff, "Error open COM%d ", Num);
      ::MessageBox (0, Buff, "Error", MB_OK);
   }

   Error = SetupComm (_hCom, COMM_BUFF, COMM_BUFF);
   if (!Error){
      sprintf (Buff, "Error function GetupComm  COM%d ", Num);
      ::MessageBox (0, Buff, "Error", MB_OK);
   }

   Error = GetCommState (_hCom, &_Dcb);
   if (!Error){
      sprintf (Buff, "Error function GetCommState  COM%d ", Num);
      ::MessageBox (0, Buff, "Error", MB_OK);
   }
   _Dcb.BaudRate  = 9600;
   _Dcb.ByteSize  = 8;
   _Dcb.Parity	  = NOPARITY;
   _Dcb.StopBits  = TWOSTOPBITS;

   Error = SetCommState (_hCom, &_Dcb);
   if (!Error){
      sprintf (Buff, "Error function SetCommState COM%d ", Num);
      ::MessageBox (0, Buff, "Error", MB_OK);
   }

   _EvOverR = CreateEvent (NULL, TRUE, FALSE, NULL);	// Disable
   _EvOverW = CreateEvent (NULL, TRUE, FALSE, NULL);	// Disable

   _OverlappedR.Offset	    = 0;
   _OverlappedR.OffsetHigh  = 0;
   _OverlappedR.hEvent	    = _EvOverR;

   _OverlappedW.Offset	    = 0;
   _OverlappedW.OffsetHigh  = 0;
   _OverlappedW.hEvent	    = _EvOverW;
};
//----------------------------------------------------------------------------
MxComm::~MxComm ()
{
   if (_hCom)
      CloseHandle (_hCom);
   if (_EvOverR)
      CloseHandle (_EvOverR);
   if (_EvOverW)
      CloseHandle (_EvOverW);
};
//--    --------------------------------------------------------------------------
BOOL MxComm::ReadOnly (char * Str, int &Len)
{
   if (Len > COMM_BUFF)
      Len = COMM_BUFF;

   BOOL Error;
   ReadFile  (_hCom, Str, Len,
	      &_NumberOfByte,
	      &_OverlappedR);

   if (Len != _NumberOfByte){
      Len = _NumberOfByte;
      Error = FALSE;
   }
   return Error;
};
//----------------------------------------------------------------------------
BOOL MxComm::Read (char * Str, int &Len)
{
   if (Len > COMM_BUFF)
      Len = COMM_BUFF;

   ReadFile  (_hCom, Str, Len,
	      &_NumberOfByte,
	      &_OverlappedR);

   BOOL Error = GetOverlappedResult (_hCom,
				    &_OverlappedR,
				    &_NumberOfByte,
				    TRUE);
   if (Len != _NumberOfByte){
      Len = _NumberOfByte;
      Error = FALSE;
   }
   return Error;
};
//----------------------------------------------------------------------------
BOOL MxComm::Wait (char * Str, int Len)
{
   if (Len > COMM_BUFF)
      Len = COMM_BUFF;

   ReadFile  (_hCom, _Buff, Len,
	      &_NumberOfByte,
	      &_OverlappedR);

   BOOL Error = GetOverlappedResult (_hCom,
				    &_OverlappedR,
				    &_NumberOfByte,
				    TRUE);

   if (Len == _NumberOfByte){
      if (!Str)
	      return Error;
      for (int i = 0; i < _NumberOfByte; i++){
	      if (Str[i] != _Buff[i]){
	         Error = FALSE;
	         break;
	      }
      }
   }
   else{
      Error = FALSE;
   }

   return Error;
};
//----------------------------------------------------------------------------
BOOL MxComm::WaitString (char * Str)
{
   int	 Len = 1;
   char  In[2];
   In[1] = 0;
   BOOL  Error;
   BOOL  Find = TRUE;
   int	 Length = strlen (Str);
   int	 i = 0;

   while (Find){
      ReadFile	(_hCom, In, Len,
		 &_NumberOfByte,
		 &_OverlappedR);

      Error = GetOverlappedResult (_hCom,
				       &_OverlappedR,
				       &_NumberOfByte,
				       TRUE);

      if (Len == _NumberOfByte){
	 if (i >= Length){
	    strcpy (_Buff, _Buff+1);
	    _Buff [Length - 1] = In [0];
	    _Buff [Length]     = 0;
         }
	 else{
	    _Buff [i]	= In [0];
	    _Buff [i+1] = 0;
	 }
	 if (!strcmp (Str, _Buff))
	    Find = FALSE;
      }
      else{
	 Error = FALSE;
	 Find  = FALSE;
      }
      i++;
   }

   return Error;
};
//----------------------------------------------------------------------------
BOOL MxComm::Write (char * Str, int Len)
{
   if (Len > COMM_BUFF)
      Len = COMM_BUFF;

   BOOL Error = WriteFile  (_hCom, Str, Len,
	                        &_NumberOfByte,
	                        &_OverlappedW);

   if (Len != _NumberOfByte){
      Error = FALSE;
   }
   return Error;
};
//----------------------------------------------------------------------------
BOOL MxComm::WriteWait (char * Str, int Len)
{
   if (Len > COMM_BUFF)
      Len = COMM_BUFF;

   WriteFile  (_hCom, Str, Len,
	       &_NumberOfByte,
	       &_OverlappedW);

   BOOL Error = GetOverlappedResult (_hCom,
				    &_OverlappedW,
				    &_NumberOfByte,
				    TRUE);
   if (Len != _NumberOfByte){
      Error = FALSE;
   }
   return Error;
};
//---------------------------------------------------------------------------
BOOL MxComm::PurgeTX ()
{
//   return (PurgeComm (_hCom, PURGE_TXABORT | PURGE_TXCLEAR));
   return (PurgeComm (_hCom, PURGE_TXCLEAR));
};
//---------------------------------------------------------------------------
BOOL MxComm::PurgeRX ()
{
//   return (PurgeComm (_hCom, PURGE_RXABORT | PURGE_RXCLEAR));
   return (PurgeComm (_hCom, PURGE_RXCLEAR));
};
//---------------------------------------------------------------------------
void MxComm::SetTimeout (DWORD Constant)
{

   DWORD Data	 = 0xFFFFFFFF;
   COMMTIMEOUTS commTimeOuts;

   commTimeOuts.ReadIntervalTimeout	     = Constant;
   commTimeOuts.ReadTotalTimeoutMultiplier   = 0;
   commTimeOuts.ReadTotalTimeoutConstant     = Constant;
   commTimeOuts.WriteTotalTimeoutMultiplier  = 0;
   commTimeOuts.WriteTotalTimeoutConstant    = Data;

   SetCommTimeouts (_hCom, &commTimeOuts);
};
//----------------------------------------------------------------------------
void MxComm::EnableEvent (BOOL FlgEv)
{
   if (FlgEv){
      SetEvent (_EvOverR);
      SetEvent (_EvOverW);
   }
   else{
      ResetEvent (_EvOverR);
      ResetEvent (_EvOverW);
   }
};
//---------------------------------------------------------------------------
*/