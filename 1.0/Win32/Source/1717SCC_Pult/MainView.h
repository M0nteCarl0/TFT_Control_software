#pragma once
#include  "FTDIView.h"
#include  "MXCOMM.H"
#include "KY5_Commands_19200.h"
#include "ExspositionParams.h"
#include "CalibrationParametrsView.h"
#include "DavinchySetup.h"
#include <RayanceFunctions.h>
#include "CalibrationConfig.h"
#include "PultConfig.h"
#include "ImageProcess\FLR_P.h"
#include "ImageProcess\CCD_Calibrate.h"
#include "ImageProcess\CCD_Average.h"
#include "ImageProcess\RAW_P.h"
#include <direct.h>
#include <time.inl>
#include <string.h> 
#include <io.h>
#include <sys\stat.h>
#include <sys\types.h>
#include "Event.h"

#include "CriticalSection.h"
namespace Roentgepropm {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>

	enum MODE
	{
		MODE_DC,
		MODE_BRIGHT,
		MODE_BRIGHT_Calibre,
		MODE_BRIGHT_Calibration,
	};

	struct MeasuredKyParams
	{
		int Time;
		int HV;
		float Current;
		float Mas;
			 
	};

	

	public ref class MainView : public System::Windows::Forms::Form
	{
	public:
	
	private: System::Windows::Forms::ToolStripMenuItem^  ConectToTFTToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  DisconectTFTToolStripMenuItem;
	private: System::Windows::Forms::ToolStripStatusLabel^  HV_Diag;
	private: System::Windows::Forms::GroupBox^  groupBox6;
	private: System::Windows::Forms::CheckBox^  FitingFrames;


	private: System::Windows::Forms::CheckBox^  AvergageFRame;

	private: System::Windows::Forms::CheckBox^  CapturingFrames;
	private: System::Windows::Forms::CheckBox^  CalibrationCompleted;
	private: System::Windows::Forms::GroupBox^  groupBox7;


	private: System::Windows::Forms::ToolStripStatusLabel^  DetectorInternalState;
	private: System::Windows::Forms::GroupBox^  FileformatgroupBox;
	private: System::Windows::Forms::ComboBox^  FileFormatcomboBox;


	public: 
  
		MainView(void)
		{
			InitializeComponent();
			_FTDI = gcnew FTDIView();
			_CalibrationSetup = gcnew  CalibrationParametrsView();
			_ExpoParamsSetup = gcnew ExspositionParams();
			_DavinchySetup = gcnew  DavinchySetup();
			_MXCOMM = new MxComm();
			ProgramConfig = new PultConfig();
			CalibreConfig = new CalibrationConfig();
			CriticalRegion = new CriticalSection();
			ImageLoadComplete = new Event("ImageLoadComplete");
		    Params = new MeasuredKyParams();

			Calibrate = new  CCD_Calibrate();
			Avergae = new CCD_Average();
			_SavedData = new FLR_P();
		    _RAWData =  new RAW_P();

			StartCalibration->Enabled = false;
			BrightFrame->Enabled = false;
			X_RayTest->Enabled = false;


			PatExe = new char[255];
			PathCalibrationDataBase = new char[255];
			TargetHVCoff = new char[255];
			FileFormatcomboBox->SelectedIndex = 0;
			InitBinaryDir();


			if (ProgramConfig->Open())
			{

				ProgramConfig->ReadConfig();
				MaleCalibrateDirectory();
				if (VADAV_MapDLL(ProgramConfig->GetPDavinchyPath()))
				{
					
					int FW, FH;
					MessageBox::Show("Davinchy ������� ���������������");
					VADAV_SetWindowsHandle(this->Handle.ToPointer());
					IP_TFT->Text = Marshal::PtrToStringAnsi((IntPtr)VADAV_GetIP());
					VADAV_GetFrameDim(&FW, &FH);
					fH = FH;
					fW = FH;
					VADAV_SetWindowsHandle(this->Handle.ToPointer());
					FrameHeigth->Text = fH.ToString();
					FrameWidth->Text = fW.ToString();

					Calibrate->SetImageSize(0,0,FH,FW);
					Calibrate->SetRectMean(FH / 4,FW / 4,FH / 2,FW / 2);

					const int RmsS = 200;
					const int Left = (FH - RmsS) / 2;
					const int Top  = (FW - RmsS) / 2;
					Avergae->SetImageSize(FH,FW);
					Avergae->SetRectRMS(Left,Top,Left + RmsS,Top + RmsS);
					Avergae->SetRectMean(FW / 2,FH / 2,FW / 2,FH / 2);
					



					FrameBuffer = new short[fH*fW];
				}
			}
			
			nSpeed = 19200;
			nDataBits = 8;
			nParity = 1;
			nStopBits = 2;
         
		}

/********************************************************************************************/
		void InitBinaryDir(void)
		{

			GetCurrentDirectory(255,PatExe);


		}
/********************************************************************************************/
		void GoToRootFolder(void)
		{
			SetCurrentDirectory(PatExe);
		}
/********************************************************************************************/
		void SetToCalibrateDir(void)
		{
			SetCurrentDirectory("Calibrate");
		}
/********************************************************************************************/
		void MakeDirmAs(float mAs)
		{
			char Path[255];
			char DirName[255];
			SetCurrentDirectory(Path);
			sprintf(DirName, "%.2f_mAs", mAs);
			mkdir(DirName);
			SetCurrentDirectory(DirName);

		}
/********************************************************************************************/
		void MakeDirHV(int HV)
		{

			char Path[255];
			char DirName[255];
			SetCurrentDirectory(Path);
			sprintf(DirName, "%i_KV", (int)HV);
			mkdir(DirName);
			SetCurrentDirectory(DirName);

		}
/********************************************************************************************/
		void MaleCalibrateDirectory(void)

		{

			WIN32_FIND_DATA FindFileData;
			HANDLE hFind = NULL;
			char Paath[255];
			hFind = FindFirstFile("Calibrate", &FindFileData);
			if (hFind && FindFileData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
			{
				FindClose(hFind);
			}
			else
			{
				CreateDirectory("Calibrate", NULL);
			}


		}


/********************************************************************************************/
		void GotoCalibrationFolder(int% HV)//1
		{
			char PathHV[255];
			char Path[255];
			GoToRootFolder();
			strcat(Path,PatExe);
			strcat(Path,"\\Calibrate");
			
		
			sprintf(PathHV,"\\%i_kV",HV);
			SetCurrentDirectory(Path);
            CreateDirectory(PathHV,NULL);
			strcat(Path,PathHV);
			SetCurrentDirectory(PathHV);
		
			
		}

/********************************************************************************************/
		void CheckAndMakeFolderForPoint(float mAs)//2
		{



		}

/********************************************************************************************/
		void GoToFoldermAs(float mAs)//3
		{




		}


/********************************************************************************************/
	virtual void WndProc(Message% m) override
		{

			switch (m.Msg)
			{
				
				case 0x10056:
				{

					StatID->Text ="�������� ������: " +   m.WParam.ToString();
					break;
				}

				case 0x10057:
				{
			
					MHV->Text = Params->HV.ToString();
					MCurrent->Text = Params->Current.ToString();
					MmAs->Text = Params->Mas.ToString();
					MTime->Text = Params->Time.ToString();
					break;
				}


				case VADAV_MessageCode_DetectorStatus:
				{
					String^ Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.WParam));
					DetectorInternalState->Text = Text;
					break;
				}


				case VADAV_MessageCode_ERR:
				{

					MessageBox::Show("������ Davinchy ���: {" + m.WParam.ToString()  + "} " + gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam)));
					break;
				}


				case VADAV_MessageCode_TRACE:
				{
					String^ Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.WParam));
					DavinchyTrace->Text = Text;
					break;

				}

				case VADAV_MessageCode_RECEIVED_PERCENTS_COMPLETE:
					{


						size_t BufferSize = 0;
						size_t W, H;
						W = H = 0;
						char Filenane[255];
						time_t Time;
						tm* TimeF;				 
						Time  =   time(NULL);
						TimeF = localtime(&Time);
					
						if(	_Mode == MODE::MODE_DC)
						{

							TFTStripStatusLabel->Text = "DC ������� �������  � ��������";
							sprintf(Filenane, "DC_%i-%i-%i-%i-%i-%i", TimeF->tm_year+1900, TimeF->tm_mon, TimeF->tm_mday, TimeF->tm_hour, TimeF->tm_min, TimeF->tm_sec);

							if(FileFormatcomboBox->SelectedIndex == 0)
							{
								_SavedData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);
							}

							if(FileFormatcomboBox->SelectedIndex == 1)
							{
								_RAWData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);

							}
							

								if(FileFormatcomboBox->SelectedIndex == 2)
							{
								_SavedData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);
								_RAWData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);

							}


							 
							
						}

						if(_Mode == MODE_BRIGHT_Calibration)
						{
						
							TFTStripStatusLabel->Text = "Calibration Point ������� �������  � ��������";
							float RmAs;
							int RTime;
							int RCurrent;
							int RHV;
							CriticalRegion->Enter();
							int CHV = _CHV;
							float CmAs = _CmAs;
							CriticalRegion->Leave();
							char FolderPath[1024];
							char HVString[255];
							char mAsFolderName[1024];
							memset(mAsFolderName,0,1024);
							memset(FolderPath,0,1024);
							memset(HVString,0,255);

							sprintf(HVString, "%i_kV", CHV);
							sprintf(mAsFolderName,"%.2f_mAs",CmAs);
							strcat(FolderPath,"Calibrate");
							strcat(FolderPath,"/");
							strcat(FolderPath,HVString);
							strcat(FolderPath,"/");
							strcat(FolderPath,mAsFolderName);
							strcat(FolderPath,"/");

							CriticalRegion->Enter();
							_KY5->GetMeasuredMAS(&RmAs);
							_KY5->GetMeasuredExpositionTime(&RTime);
							_KY5->GetMeasuredNegativeAnodeCurrent(&RCurrent);
							_KY5->GetMeasuredAnodeHV(&RHV);
							CriticalRegion->Leave();
							sprintf(Filenane, "CAL_%i-%i-%i-%i-%i-%i__%i_kV_%.2f_mAs_%i_mA_%i_ms", TimeF->tm_year+1900, TimeF->tm_mon, TimeF->tm_mday, TimeF->tm_hour, TimeF->tm_min, TimeF->tm_sec,RHV,RmAs,RCurrent,RTime);
							strcat(FolderPath,Filenane);
							_SavedData->Make(FolderPath,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);
								//_RAWData->Make((FolderPath,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize));
							ImageLoadComplete->Set();
						
						}



						if(_Mode == MODE::MODE_BRIGHT)
						{

							if(!FitingProcedure->Checked)
							{

							sprintf(Filenane, "XRAY_%i-%i-%i-%i-%i-%i__%i_kV_%.2f_mAs_%i_mA_%i_ms", TimeF->tm_year+1900, TimeF->tm_mon, TimeF->tm_mday, TimeF->tm_hour, TimeF->tm_min, TimeF->tm_sec,REHV,REmAs,RECurrent,RETime);
							

							if(FileFormatcomboBox->SelectedIndex == 0)
							{
								_SavedData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);
							}

							if(FileFormatcomboBox->SelectedIndex == 1)
							{
								_RAWData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);

							}
							

								if(FileFormatcomboBox->SelectedIndex == 2)
							{
								_SavedData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);
								_RAWData->Make(Filenane,(uint16_t*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),BufferSize);

							}
							
						
							TFTStripStatusLabel->Text = "XRAY ������� ������� � ��������";
							
							}
							else
							{
								const int PreLoadFitStructuresSuccess  = 	Calibrate->PreLoadFitStructures("./");
								if(!PreLoadFitStructuresSuccess)
								{
								
								sprintf(Filenane, "Fitted_XRAY_%i-%i-%i-%i-%i-%i__%i_kV_%.2f_mAs_%i_mA_%i_ms", TimeF->tm_year+1900, TimeF->tm_mon, TimeF->tm_mday, TimeF->tm_hour, TimeF->tm_min, TimeF->tm_sec,REHV,REmAs,RECurrent,RETime);
								float HVAtCalibration = REHV;
								const int RepairSuccess = Calibrate->Repair((WORD*)VADAV_GetFrameBufferData(&BufferSize, &W, &H),(float)HVAtCalibration,0,(WORD*)FrameBuffer,&HVAtCalibration);
								if(!RepairSuccess)
								{
									
									
							if(FileFormatcomboBox->SelectedIndex == 0)
							{
								_SavedData->Make(Filenane,(uint16_t*)FrameBuffer,BufferSize);
							}

							if(FileFormatcomboBox->SelectedIndex == 1)
							{
								_RAWData->Make(Filenane,(uint16_t*)FrameBuffer,BufferSize);

							}
							

								if(FileFormatcomboBox->SelectedIndex == 2)
							{
								_SavedData->Make(Filenane,(uint16_t*)FrameBuffer,BufferSize);
								_RAWData->Make(Filenane,(uint16_t*)FrameBuffer,BufferSize);

							}







									
									
									
									//_SavedData->Make(Filenane,(uint16_t*)FrameBuffer,BufferSize);
									 TFTStripStatusLabel->Text = "��������� ������������ ������ ������ ������� � ������ ��������";
								}
								else
								{

									TFTStripStatusLabel->Text = "��������� ������������ ������ ����������� � ������ �� ��������";

								}
								}
								else
								{
									 TFTStripStatusLabel->Text = "�� ������� COF ������ ��� ������������";

								}
							 
							}
						}
                     
					VADAV_CloseACQ();
					break;


					}

				case VADAV_MessageCode_RECEIVED_PERCENTS:
				{
					int Progress = (int)m.WParam;
					if (Progress>100)
					{
						Progress = 100;
					}

					ProgressBarLoadingFrame->Value = Progress;
					TFTStripStatusLabel->Text = "������� : " + Progress.ToString(); +" % ������";
					break;
				}

				case VADAV_MessageCode_AQCOUNT:
				{
					AqusiitionCount->Text =  gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam));
					VADAV_CloseACQ();
					break;
				}

				case VADAV_MessageCode_RECEIVED_ROWS:
				{
					break;
				}
				case VADAV_MessageCode_VERSIONS:
				{

					switch (m.WParam.ToInt32())
					{

					case 0x00011010://Firm
					{
						
						VER_FIRM->Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam));
						break;
					}

					case 0x00011011://FPGA
					{
						VER_FPGA->Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam));
						break;
					}
					
					case 0x00011012://Serial
					{

		               VER_MAIN->Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam));
						break;
					}


					case 0x00011013: //TFT
					{

						VER_TFTP->Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam));
						break;
					}

					case 0x00011014: //CSIS
					{

						VER_SCIN->Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam));
						break;
					}


					case 0x00011015: //LICENCE
					{

						VER_BOARD->Text = gcnew String(Marshal::PtrToStringAnsi((IntPtr)m.LParam));
						VADAV_CloseACQ();
						break;
					}


					case 0x00011016: //VER_IP
					{
						break;
					}

					case 0x00011017://VER_MAC 
					{					
						break;
					}

					default:
						break;
					}
					break;
				}
				case VADAV_MessageCode_TERMAL:
				{
					break;
				}


			default:
				break;
			}
			Form::WndProc(m);
		}

	protected:
		int _CHV;
	    float _CmAs;
		CriticalSection* CriticalRegion;
		CCD_Average* Avergae;
		MeasuredKyParams* Params;
		Event* ImageLoadComplete;
		KY5_Commands_19200* _KY5;
		FLR_P*             _SavedData;
		RAW_P*             _RAWData;
		short*  FrameBuffer;
		char* PatExe;
		char* PathCalibrationDataBase;
		char* TargetHVCoff;
		CCD_Calibrate*     Calibrate;
		CalibrationConfig* CalibreConfig;
		PultConfig*        ProgramConfig;
		MxComm*			  _MXCOMM;
		int REHV;
		int RECurrent;
		float REmAs;
		int fW, fH;
		int RETime;
		DWORD       SelectedDevice;
		UINT        nSpeed;
		UINT        nDataBits;
		UINT        nParity;
		UINT        nStopBits;
		bool BigFocuse;
		MODE _Mode;
		int  _CurrentHV;
		FTDIView^       _FTDI;
		CalibrationParametrsView^ _CalibrationSetup;
		ExspositionParams^   _ExpoParamsSetup;
		DavinchySetup^      _DavinchySetup;
		
		
private: System::Windows::Forms::CheckBox^  FitingProcedure;
protected:


	private: System::Windows::Forms::TabPage^  tabPage3;
private: System::Windows::Forms::ToolStripStatusLabel^  TFTStripStatusLabel;
private: System::Windows::Forms::GroupBox^  groupBox3;
private: System::Windows::Forms::Label^  AqusiitionCount;

private: System::Windows::Forms::Label^  label30;
private: System::Windows::Forms::Label^  VER_BOARD;


private: System::Windows::Forms::Label^  VER_SCIN;

private: System::Windows::Forms::Label^  VER_TFTP;


private: System::Windows::Forms::Label^  VER_MAIN;

private: System::Windows::Forms::Label^  VER_FPGA;

private: System::Windows::Forms::Label^  VER_FIRM;

private: System::Windows::Forms::Label^  label23;
private: System::Windows::Forms::Label^  label10;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::Label^  label8;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::Label^  IP_TFT;
private: System::Windows::Forms::Label^  label24;
private: System::Windows::Forms::Label^  FrameWidth;

private: System::Windows::Forms::Label^  label27;
private: System::Windows::Forms::Label^  FrameHeigth;

private: System::Windows::Forms::Label^  label25;
private: System::Windows::Forms::GroupBox^  groupBox5;
private: System::Windows::Forms::Button^  ShutdownTFT;
private: System::Windows::Forms::Button^  RebootTFT;


private: System::Windows::Forms::Button^  VersionsTFT;
private: System::Windows::Forms::Label^  StatID;


private: System::Windows::Forms::Label^  label26;
private: System::Windows::Forms::Button^  AQCount;

private: System::ComponentModel::BackgroundWorker^  CalibrationWorker;
private: System::Windows::Forms::ToolStripStatusLabel^  DavinchyTrace;
private: System::ComponentModel::BackgroundWorker^  FrameWorker;
private: System::Windows::Forms::Button^  DavinchyTestFrame;



	protected:

	

		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainView()
		{

			if(_RAWData)
			{
				delete _RAWData;

			}

			if(Params)
			{
				delete Params;
			}

			if(Avergae)
			{
				delete	Avergae;
			}

			if(PatExe)
			{
				delete [] PatExe;
			}

			if(PathCalibrationDataBase)
			{

				delete []	PathCalibrationDataBase;
			}

			if(TargetHVCoff)
			{
				delete []  TargetHVCoff;
			}

			if(_SavedData)
			{

				delete _SavedData;
			}

			if(ImageLoadComplete)
			{
				delete ImageLoadComplete;
			}

			if (_KY5)
			{
				delete _KY5;
			}

			if (_MXCOMM)
			{
				delete _MXCOMM;
			}

			if (ProgramConfig)
			{
				delete ProgramConfig;
			}

			if (CalibreConfig)
			{
				delete CalibreConfig;
			}
			
			if (Calibrate)
			{
				delete Calibrate;
			}

			if(FrameBuffer)
			{
				delete []  FrameBuffer;
			}

			if(CriticalRegion)
			{
				delete CriticalRegion;

			}

			VADAV_ReleaseDLL();

			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::TabControl^  tabControl1;
	protected:
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  DarkCurrentFrame;

	private: System::Windows::Forms::Button^  BrightFrame;

	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::GroupBox^  groupBox4;
private: System::Windows::Forms::Button^  CancelCalibratin;

private: System::Windows::Forms::Button^  StartCalibration;


private: System::Windows::Forms::Button^  X_RayTest;
private: System::Windows::Forms::Label^  ReadedCurrent;
private: System::Windows::Forms::Label^  ReadedTime;
private: System::Windows::Forms::Label^  ReadedmAs;
private: System::Windows::Forms::Label^  ReadedHV;
private: System::Windows::Forms::MenuStrip^  menuStrip1;
private: System::Windows::Forms::ToolStripMenuItem^  fTDIToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  SetupFTDIToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ConnectFTIToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  DisaconectFTDIToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  kY5ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  BigFocuseToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  SmallFocuseToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  �������������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  UserSetupToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  �����������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  CCDModeToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  TFTModetoolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  davincyToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  DavinchySetupToolStripMenuItem1;
private: System::Windows::Forms::ToolStripMenuItem^  �������������������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  CalibrationSetupToolStripMenuItem2;
private: System::Windows::Forms::Label^  MCurrent;

private: System::Windows::Forms::Label^  MTime;


private: System::Windows::Forms::Label^  MmAs;

private: System::Windows::Forms::Label^  MHV;


private: System::Windows::Forms::Label^  label19;
private: System::Windows::Forms::Label^  label20;
private: System::Windows::Forms::Label^  label21;
private: System::Windows::Forms::Label^  label22;
private: System::Windows::Forms::StatusStrip^  statusStrip1;
private: System::Windows::Forms::ToolStripStatusLabel^  FTDIStripStatusLabel;
private: System::Windows::Forms::ToolStripProgressBar^  ProgressBarLoadingFrame;
private: System::Windows::Forms::TextBox^  HVTime;
private: System::Windows::Forms::TextBox^  HVmAs;
private: System::Windows::Forms::TextBox^  HVCurrent;
private: System::Windows::Forms::TextBox^  HVPrecurent;
private: System::Windows::Forms::TextBox^  HVVoltage;
private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->FileformatgroupBox = (gcnew System::Windows::Forms::GroupBox());
			this->FileFormatcomboBox = (gcnew System::Windows::Forms::ComboBox());
			this->groupBox7 = (gcnew System::Windows::Forms::GroupBox());
			this->FitingProcedure = (gcnew System::Windows::Forms::CheckBox());
			this->X_RayTest = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->ReadedCurrent = (gcnew System::Windows::Forms::Label());
			this->ReadedTime = (gcnew System::Windows::Forms::Label());
			this->ReadedmAs = (gcnew System::Windows::Forms::Label());
			this->ReadedHV = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->HVTime = (gcnew System::Windows::Forms::TextBox());
			this->HVmAs = (gcnew System::Windows::Forms::TextBox());
			this->HVCurrent = (gcnew System::Windows::Forms::TextBox());
			this->HVPrecurent = (gcnew System::Windows::Forms::TextBox());
			this->HVVoltage = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->DarkCurrentFrame = (gcnew System::Windows::Forms::Button());
			this->BrightFrame = (gcnew System::Windows::Forms::Button());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox6 = (gcnew System::Windows::Forms::GroupBox());
			this->CalibrationCompleted = (gcnew System::Windows::Forms::CheckBox());
			this->StatID = (gcnew System::Windows::Forms::Label());
			this->FitingFrames = (gcnew System::Windows::Forms::CheckBox());
			this->AvergageFRame = (gcnew System::Windows::Forms::CheckBox());
			this->CapturingFrames = (gcnew System::Windows::Forms::CheckBox());
			this->label26 = (gcnew System::Windows::Forms::Label());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->MCurrent = (gcnew System::Windows::Forms::Label());
			this->MTime = (gcnew System::Windows::Forms::Label());
			this->MmAs = (gcnew System::Windows::Forms::Label());
			this->MHV = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->CancelCalibratin = (gcnew System::Windows::Forms::Button());
			this->StartCalibration = (gcnew System::Windows::Forms::Button());
			this->tabPage3 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox5 = (gcnew System::Windows::Forms::GroupBox());
			this->AQCount = (gcnew System::Windows::Forms::Button());
			this->VersionsTFT = (gcnew System::Windows::Forms::Button());
			this->ShutdownTFT = (gcnew System::Windows::Forms::Button());
			this->RebootTFT = (gcnew System::Windows::Forms::Button());
			this->DavinchyTestFrame = (gcnew System::Windows::Forms::Button());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->FrameWidth = (gcnew System::Windows::Forms::Label());
			this->label27 = (gcnew System::Windows::Forms::Label());
			this->FrameHeigth = (gcnew System::Windows::Forms::Label());
			this->label25 = (gcnew System::Windows::Forms::Label());
			this->IP_TFT = (gcnew System::Windows::Forms::Label());
			this->label24 = (gcnew System::Windows::Forms::Label());
			this->AqusiitionCount = (gcnew System::Windows::Forms::Label());
			this->label30 = (gcnew System::Windows::Forms::Label());
			this->VER_BOARD = (gcnew System::Windows::Forms::Label());
			this->VER_SCIN = (gcnew System::Windows::Forms::Label());
			this->VER_TFTP = (gcnew System::Windows::Forms::Label());
			this->VER_MAIN = (gcnew System::Windows::Forms::Label());
			this->VER_FPGA = (gcnew System::Windows::Forms::Label());
			this->VER_FIRM = (gcnew System::Windows::Forms::Label());
			this->label23 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fTDIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->SetupFTDIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->ConnectFTIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->DisaconectFTDIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->kY5ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->BigFocuseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->SmallFocuseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->UserSetupToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->CCDModeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->TFTModetoolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->davincyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->DavinchySetupToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->ConectToTFTToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->DisconectTFTToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->CalibrationSetupToolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->FTDIStripStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->TFTStripStatusLabel = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->ProgressBarLoadingFrame = (gcnew System::Windows::Forms::ToolStripProgressBar());
			this->DavinchyTrace = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->HV_Diag = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->DetectorInternalState = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->CalibrationWorker = (gcnew System::ComponentModel::BackgroundWorker());
			this->FrameWorker = (gcnew System::ComponentModel::BackgroundWorker());
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->FileformatgroupBox->SuspendLayout();
			this->groupBox7->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->groupBox6->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->tabPage3->SuspendLayout();
			this->groupBox5->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Controls->Add(this->tabPage3);
			this->tabControl1->Location = System::Drawing::Point(2, 36);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(684, 227);
			this->tabControl1->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->FileformatgroupBox);
			this->tabPage1->Controls->Add(this->groupBox7);
			this->tabPage1->Controls->Add(this->X_RayTest);
			this->tabPage1->Controls->Add(this->groupBox2);
			this->tabPage1->Controls->Add(this->groupBox1);
			this->tabPage1->Controls->Add(this->DarkCurrentFrame);
			this->tabPage1->Controls->Add(this->BrightFrame);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(676, 201);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"������";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// FileformatgroupBox
			// 
			this->FileformatgroupBox->Controls->Add(this->FileFormatcomboBox);
			this->FileformatgroupBox->Location = System::Drawing::Point(487, 27);
			this->FileformatgroupBox->Name = L"FileformatgroupBox";
			this->FileformatgroupBox->Size = System::Drawing::Size(166, 132);
			this->FileformatgroupBox->TabIndex = 8;
			this->FileformatgroupBox->TabStop = false;
			this->FileformatgroupBox->Text = L"������ ��������� �����";
			// 
			// FileFormatcomboBox
			// 
			this->FileFormatcomboBox->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->FileFormatcomboBox->FormattingEnabled = true;
			this->FileFormatcomboBox->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"FLR", L"RAW", L"FLR+RAW"});
			this->FileFormatcomboBox->Location = System::Drawing::Point(12, 25);
			this->FileFormatcomboBox->Name = L"FileFormatcomboBox";
			this->FileFormatcomboBox->Size = System::Drawing::Size(121, 21);
			this->FileFormatcomboBox->TabIndex = 7;
			// 
			// groupBox7
			// 
			this->groupBox7->Controls->Add(this->FitingProcedure);
			this->groupBox7->Location = System::Drawing::Point(360, 20);
			this->groupBox7->Name = L"groupBox7";
			this->groupBox7->Size = System::Drawing::Size(121, 139);
			this->groupBox7->TabIndex = 6;
			this->groupBox7->TabStop = false;
			this->groupBox7->Text = L"��������� ������";
			// 
			// FitingProcedure
			// 
			this->FitingProcedure->AutoSize = true;
			this->FitingProcedure->Location = System::Drawing::Point(6, 19);
			this->FitingProcedure->Name = L"FitingProcedure";
			this->FitingProcedure->Size = System::Drawing::Size(101, 17);
			this->FitingProcedure->TabIndex = 5;
			this->FitingProcedure->Text = L"������������";
			this->FitingProcedure->UseVisualStyleBackColor = true;
			// 
			// X_RayTest
			// 
			this->X_RayTest->Location = System::Drawing::Point(168, 172);
			this->X_RayTest->Name = L"X_RayTest";
			this->X_RayTest->Size = System::Drawing::Size(93, 23);
			this->X_RayTest->TabIndex = 4;
			this->X_RayTest->Text = L"���� ��������";
			this->X_RayTest->UseVisualStyleBackColor = true;
			this->X_RayTest->Click += gcnew System::EventHandler(this, &MainView::X_RayTest_Click);
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->ReadedCurrent);
			this->groupBox2->Controls->Add(this->ReadedTime);
			this->groupBox2->Controls->Add(this->ReadedmAs);
			this->groupBox2->Controls->Add(this->ReadedHV);
			this->groupBox2->Controls->Add(this->label14);
			this->groupBox2->Controls->Add(this->label13);
			this->groupBox2->Controls->Add(this->label12);
			this->groupBox2->Controls->Add(this->label11);
			this->groupBox2->Location = System::Drawing::Point(206, 20);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(148, 139);
			this->groupBox2->TabIndex = 3;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"���������";
			// 
			// ReadedCurrent
			// 
			this->ReadedCurrent->AutoSize = true;
			this->ReadedCurrent->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedCurrent->Location = System::Drawing::Point(93, 92);
			this->ReadedCurrent->Name = L"ReadedCurrent";
			this->ReadedCurrent->Size = System::Drawing::Size(15, 15);
			this->ReadedCurrent->TabIndex = 7;
			this->ReadedCurrent->Text = L"0";
			// 
			// ReadedTime
			// 
			this->ReadedTime->AutoSize = true;
			this->ReadedTime->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedTime->Location = System::Drawing::Point(93, 73);
			this->ReadedTime->Name = L"ReadedTime";
			this->ReadedTime->Size = System::Drawing::Size(15, 15);
			this->ReadedTime->TabIndex = 6;
			this->ReadedTime->Text = L"0";
			// 
			// ReadedmAs
			// 
			this->ReadedmAs->AutoSize = true;
			this->ReadedmAs->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedmAs->Location = System::Drawing::Point(93, 51);
			this->ReadedmAs->Name = L"ReadedmAs";
			this->ReadedmAs->Size = System::Drawing::Size(15, 15);
			this->ReadedmAs->TabIndex = 5;
			this->ReadedmAs->Text = L"0";
			// 
			// ReadedHV
			// 
			this->ReadedHV->AutoSize = true;
			this->ReadedHV->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedHV->Location = System::Drawing::Point(93, 28);
			this->ReadedHV->Name = L"ReadedHV";
			this->ReadedHV->Size = System::Drawing::Size(15, 15);
			this->ReadedHV->TabIndex = 4;
			this->ReadedHV->Text = L"0";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(15, 92);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(26, 13);
			this->label14->TabIndex = 3;
			this->label14->Text = L"���";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(15, 28);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(71, 13);
			this->label13->TabIndex = 2;
			this->label13->Text = L"����������";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(15, 51);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(28, 13);
			this->label12->TabIndex = 1;
			this->label12->Text = L"���";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(15, 73);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(40, 13);
			this->label11->TabIndex = 0;
			this->label11->Text = L"�����";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->HVTime);
			this->groupBox1->Controls->Add(this->HVmAs);
			this->groupBox1->Controls->Add(this->HVCurrent);
			this->groupBox1->Controls->Add(this->HVPrecurent);
			this->groupBox1->Controls->Add(this->HVVoltage);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(7, 20);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(193, 148);
			this->groupBox1->TabIndex = 2;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"��������� ";
			// 
			// HVTime
			// 
			this->HVTime->Location = System::Drawing::Point(95, 119);
			this->HVTime->Name = L"HVTime";
			this->HVTime->Size = System::Drawing::Size(63, 20);
			this->HVTime->TabIndex = 7;
			// 
			// HVmAs
			// 
			this->HVmAs->Location = System::Drawing::Point(95, 92);
			this->HVmAs->Name = L"HVmAs";
			this->HVmAs->Size = System::Drawing::Size(63, 20);
			this->HVmAs->TabIndex = 8;
			// 
			// HVCurrent
			// 
			this->HVCurrent->Location = System::Drawing::Point(95, 68);
			this->HVCurrent->Name = L"HVCurrent";
			this->HVCurrent->Size = System::Drawing::Size(63, 20);
			this->HVCurrent->TabIndex = 7;
			// 
			// HVPrecurent
			// 
			this->HVPrecurent->Location = System::Drawing::Point(95, 43);
			this->HVPrecurent->Name = L"HVPrecurent";
			this->HVPrecurent->Size = System::Drawing::Size(63, 20);
			this->HVPrecurent->TabIndex = 6;
			// 
			// HVVoltage
			// 
			this->HVVoltage->Location = System::Drawing::Point(95, 17);
			this->HVVoltage->Name = L"HVVoltage";
			this->HVVoltage->Size = System::Drawing::Size(63, 20);
			this->HVVoltage->TabIndex = 5;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(46, 122);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(40, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"�����";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(58, 92);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(28, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"���";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(18, 73);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(68, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"���  ������";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(0, 50);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(89, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"��� ����������";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(18, 20);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(71, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"����������";
			// 
			// DarkCurrentFrame
			// 
			this->DarkCurrentFrame->Location = System::Drawing::Point(87, 172);
			this->DarkCurrentFrame->Name = L"DarkCurrentFrame";
			this->DarkCurrentFrame->Size = System::Drawing::Size(75, 23);
			this->DarkCurrentFrame->TabIndex = 1;
			this->DarkCurrentFrame->Text = L"DC";
			this->DarkCurrentFrame->UseVisualStyleBackColor = true;
			this->DarkCurrentFrame->Click += gcnew System::EventHandler(this, &MainView::DarkCurrentFrame_Click);
			// 
			// BrightFrame
			// 
			this->BrightFrame->Location = System::Drawing::Point(6, 172);
			this->BrightFrame->Name = L"BrightFrame";
			this->BrightFrame->Size = System::Drawing::Size(75, 23);
			this->BrightFrame->TabIndex = 0;
			this->BrightFrame->Text = L"������";
			this->BrightFrame->UseVisualStyleBackColor = true;
			this->BrightFrame->Click += gcnew System::EventHandler(this, &MainView::BrightFrame_Click);
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->groupBox6);
			this->tabPage2->Controls->Add(this->groupBox4);
			this->tabPage2->Controls->Add(this->CancelCalibratin);
			this->tabPage2->Controls->Add(this->StartCalibration);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(676, 201);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"����������";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// groupBox6
			// 
			this->groupBox6->Controls->Add(this->CalibrationCompleted);
			this->groupBox6->Controls->Add(this->StatID);
			this->groupBox6->Controls->Add(this->FitingFrames);
			this->groupBox6->Controls->Add(this->AvergageFRame);
			this->groupBox6->Controls->Add(this->CapturingFrames);
			this->groupBox6->Controls->Add(this->label26);
			this->groupBox6->Location = System::Drawing::Point(163, 18);
			this->groupBox6->Name = L"groupBox6";
			this->groupBox6->Size = System::Drawing::Size(259, 141);
			this->groupBox6->TabIndex = 7;
			this->groupBox6->TabStop = false;
			this->groupBox6->Text = L"���� ����������";
			// 
			// CalibrationCompleted
			// 
			this->CalibrationCompleted->AutoCheck = false;
			this->CalibrationCompleted->AutoSize = true;
			this->CalibrationCompleted->Location = System::Drawing::Point(15, 112);
			this->CalibrationCompleted->Name = L"CalibrationCompleted";
			this->CalibrationCompleted->Size = System::Drawing::Size(149, 17);
			this->CalibrationCompleted->TabIndex = 3;
			this->CalibrationCompleted->Text = L"����������  ���������";
			this->CalibrationCompleted->UseVisualStyleBackColor = true;
			// 
			// StatID
			// 
			this->StatID->AutoSize = true;
			this->StatID->Location = System::Drawing::Point(75, 16);
			this->StatID->Name = L"StatID";
			this->StatID->Size = System::Drawing::Size(37, 13);
			this->StatID->TabIndex = 6;
			this->StatID->Text = L"StatID";
			// 
			// FitingFrames
			// 
			this->FitingFrames->AutoCheck = false;
			this->FitingFrames->AutoSize = true;
			this->FitingFrames->Location = System::Drawing::Point(15, 89);
			this->FitingFrames->Name = L"FitingFrames";
			this->FitingFrames->Size = System::Drawing::Size(130, 17);
			this->FitingFrames->TabIndex = 2;
			this->FitingFrames->Text = L"����������  ������";
			this->FitingFrames->UseVisualStyleBackColor = true;
			// 
			// AvergageFRame
			// 
			this->AvergageFRame->AutoCheck = false;
			this->AvergageFRame->AutoSize = true;
			this->AvergageFRame->Location = System::Drawing::Point(15, 66);
			this->AvergageFRame->Name = L"AvergageFRame";
			this->AvergageFRame->Size = System::Drawing::Size(128, 17);
			this->AvergageFRame->TabIndex = 1;
			this->AvergageFRame->Text = L"���������� ������";
			this->AvergageFRame->UseVisualStyleBackColor = true;
			// 
			// CapturingFrames
			// 
			this->CapturingFrames->AutoCheck = false;
			this->CapturingFrames->AutoSize = true;
			this->CapturingFrames->Location = System::Drawing::Point(15, 43);
			this->CapturingFrames->Name = L"CapturingFrames";
			this->CapturingFrames->Size = System::Drawing::Size(120, 17);
			this->CapturingFrames->TabIndex = 0;
			this->CapturingFrames->Text = L"��������� ������";
			this->CapturingFrames->UseVisualStyleBackColor = true;
			// 
			// label26
			// 
			this->label26->AutoSize = true;
			this->label26->Location = System::Drawing::Point(12, 16);
			this->label26->Name = L"label26";
			this->label26->Size = System::Drawing::Size(57, 13);
			this->label26->TabIndex = 5;
			this->label26->Text = L"��������";
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->MCurrent);
			this->groupBox4->Controls->Add(this->MTime);
			this->groupBox4->Controls->Add(this->MmAs);
			this->groupBox4->Controls->Add(this->MHV);
			this->groupBox4->Controls->Add(this->label19);
			this->groupBox4->Controls->Add(this->label20);
			this->groupBox4->Controls->Add(this->label21);
			this->groupBox4->Controls->Add(this->label22);
			this->groupBox4->Location = System::Drawing::Point(8, 18);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(149, 141);
			this->groupBox4->TabIndex = 4;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"���������";
			// 
			// MCurrent
			// 
			this->MCurrent->AutoSize = true;
			this->MCurrent->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->MCurrent->Location = System::Drawing::Point(94, 92);
			this->MCurrent->Name = L"MCurrent";
			this->MCurrent->Size = System::Drawing::Size(15, 15);
			this->MCurrent->TabIndex = 15;
			this->MCurrent->Text = L"0";
			// 
			// MTime
			// 
			this->MTime->AutoSize = true;
			this->MTime->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->MTime->Location = System::Drawing::Point(94, 73);
			this->MTime->Name = L"MTime";
			this->MTime->Size = System::Drawing::Size(15, 15);
			this->MTime->TabIndex = 14;
			this->MTime->Text = L"0";
			// 
			// MmAs
			// 
			this->MmAs->AutoSize = true;
			this->MmAs->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->MmAs->Location = System::Drawing::Point(94, 51);
			this->MmAs->Name = L"MmAs";
			this->MmAs->Size = System::Drawing::Size(15, 15);
			this->MmAs->TabIndex = 13;
			this->MmAs->Text = L"0";
			// 
			// MHV
			// 
			this->MHV->AutoSize = true;
			this->MHV->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->MHV->Location = System::Drawing::Point(94, 28);
			this->MHV->Name = L"MHV";
			this->MHV->Size = System::Drawing::Size(15, 15);
			this->MHV->TabIndex = 12;
			this->MHV->Text = L"0";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(16, 92);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(26, 13);
			this->label19->TabIndex = 11;
			this->label19->Text = L"���";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(16, 28);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(71, 13);
			this->label20->TabIndex = 10;
			this->label20->Text = L"����������";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(16, 51);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(28, 13);
			this->label21->TabIndex = 9;
			this->label21->Text = L"���";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(16, 73);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(40, 13);
			this->label22->TabIndex = 8;
			this->label22->Text = L"�����";
			// 
			// CancelCalibratin
			// 
			this->CancelCalibratin->Location = System::Drawing::Point(89, 165);
			this->CancelCalibratin->Name = L"CancelCalibratin";
			this->CancelCalibratin->Size = System::Drawing::Size(75, 23);
			this->CancelCalibratin->TabIndex = 2;
			this->CancelCalibratin->Text = L"������";
			this->CancelCalibratin->UseVisualStyleBackColor = true;
			this->CancelCalibratin->Click += gcnew System::EventHandler(this, &MainView::CancelCalibratin_Click);
			// 
			// StartCalibration
			// 
			this->StartCalibration->Location = System::Drawing::Point(8, 165);
			this->StartCalibration->Name = L"StartCalibration";
			this->StartCalibration->Size = System::Drawing::Size(75, 23);
			this->StartCalibration->TabIndex = 0;
			this->StartCalibration->Text = L"�����";
			this->StartCalibration->UseVisualStyleBackColor = true;
			this->StartCalibration->Click += gcnew System::EventHandler(this, &MainView::StartCalibration_Click);
			// 
			// tabPage3
			// 
			this->tabPage3->Controls->Add(this->groupBox5);
			this->tabPage3->Controls->Add(this->groupBox3);
			this->tabPage3->Location = System::Drawing::Point(4, 22);
			this->tabPage3->Name = L"tabPage3";
			this->tabPage3->Size = System::Drawing::Size(676, 201);
			this->tabPage3->TabIndex = 2;
			this->tabPage3->Text = L"Davinchy";
			this->tabPage3->UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this->groupBox5->Controls->Add(this->AQCount);
			this->groupBox5->Controls->Add(this->VersionsTFT);
			this->groupBox5->Controls->Add(this->ShutdownTFT);
			this->groupBox5->Controls->Add(this->RebootTFT);
			this->groupBox5->Controls->Add(this->DavinchyTestFrame);
			this->groupBox5->Location = System::Drawing::Point(6, 3);
			this->groupBox5->Name = L"groupBox5";
			this->groupBox5->Size = System::Drawing::Size(215, 250);
			this->groupBox5->TabIndex = 2;
			this->groupBox5->TabStop = false;
			this->groupBox5->Text = L"Service Functions";
			// 
			// AQCount
			// 
			this->AQCount->Location = System::Drawing::Point(6, 161);
			this->AQCount->Name = L"AQCount";
			this->AQCount->Size = System::Drawing::Size(123, 23);
			this->AQCount->TabIndex = 4;
			this->AQCount->Text = L"AQ Count";
			this->AQCount->UseVisualStyleBackColor = true;
			this->AQCount->Click += gcnew System::EventHandler(this, &MainView::AQCount_Click);
			// 
			// VersionsTFT
			// 
			this->VersionsTFT->Location = System::Drawing::Point(6, 132);
			this->VersionsTFT->Name = L"VersionsTFT";
			this->VersionsTFT->Size = System::Drawing::Size(123, 23);
			this->VersionsTFT->TabIndex = 3;
			this->VersionsTFT->Text = L"Versions";
			this->VersionsTFT->UseVisualStyleBackColor = true;
			this->VersionsTFT->Click += gcnew System::EventHandler(this, &MainView::VersionsTFT_Click);
			// 
			// ShutdownTFT
			// 
			this->ShutdownTFT->Location = System::Drawing::Point(6, 100);
			this->ShutdownTFT->Name = L"ShutdownTFT";
			this->ShutdownTFT->Size = System::Drawing::Size(123, 23);
			this->ShutdownTFT->TabIndex = 2;
			this->ShutdownTFT->Text = L"Shutdown";
			this->ShutdownTFT->UseVisualStyleBackColor = true;
			this->ShutdownTFT->Click += gcnew System::EventHandler(this, &MainView::ShutdownTFT_Click);
			// 
			// RebootTFT
			// 
			this->RebootTFT->Location = System::Drawing::Point(6, 63);
			this->RebootTFT->Name = L"RebootTFT";
			this->RebootTFT->Size = System::Drawing::Size(123, 23);
			this->RebootTFT->TabIndex = 1;
			this->RebootTFT->Text = L"Reboot";
			this->RebootTFT->UseVisualStyleBackColor = true;
			this->RebootTFT->Click += gcnew System::EventHandler(this, &MainView::RebootTFT_Click);
			// 
			// DavinchyTestFrame
			// 
			this->DavinchyTestFrame->Location = System::Drawing::Point(6, 29);
			this->DavinchyTestFrame->Name = L"DavinchyTestFrame";
			this->DavinchyTestFrame->Size = System::Drawing::Size(123, 23);
			this->DavinchyTestFrame->TabIndex = 0;
			this->DavinchyTestFrame->Text = L"�������� ��������";
			this->DavinchyTestFrame->UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->FrameWidth);
			this->groupBox3->Controls->Add(this->label27);
			this->groupBox3->Controls->Add(this->FrameHeigth);
			this->groupBox3->Controls->Add(this->label25);
			this->groupBox3->Controls->Add(this->IP_TFT);
			this->groupBox3->Controls->Add(this->label24);
			this->groupBox3->Controls->Add(this->AqusiitionCount);
			this->groupBox3->Controls->Add(this->label30);
			this->groupBox3->Controls->Add(this->VER_BOARD);
			this->groupBox3->Controls->Add(this->VER_SCIN);
			this->groupBox3->Controls->Add(this->VER_TFTP);
			this->groupBox3->Controls->Add(this->VER_MAIN);
			this->groupBox3->Controls->Add(this->VER_FPGA);
			this->groupBox3->Controls->Add(this->VER_FIRM);
			this->groupBox3->Controls->Add(this->label23);
			this->groupBox3->Controls->Add(this->label10);
			this->groupBox3->Controls->Add(this->label9);
			this->groupBox3->Controls->Add(this->label8);
			this->groupBox3->Controls->Add(this->label7);
			this->groupBox3->Controls->Add(this->label6);
			this->groupBox3->Location = System::Drawing::Point(227, 3);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(472, 250);
			this->groupBox3->TabIndex = 1;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"��������� ���������";
			// 
			// FrameWidth
			// 
			this->FrameWidth->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->FrameWidth->Location = System::Drawing::Point(364, 55);
			this->FrameWidth->Name = L"FrameWidth";
			this->FrameWidth->Size = System::Drawing::Size(69, 15);
			this->FrameWidth->TabIndex = 19;
			this->FrameWidth->Text = L"VER_FIRM: ";
			this->FrameWidth->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label27
			// 
			this->label27->AutoSize = true;
			this->label27->Location = System::Drawing::Point(344, 55);
			this->label27->Name = L"label27";
			this->label27->Size = System::Drawing::Size(14, 13);
			this->label27->TabIndex = 18;
			this->label27->Text = L"X";
			// 
			// FrameHeigth
			// 
			this->FrameHeigth->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->FrameHeigth->Location = System::Drawing::Point(269, 55);
			this->FrameHeigth->Name = L"FrameHeigth";
			this->FrameHeigth->Size = System::Drawing::Size(69, 15);
			this->FrameHeigth->TabIndex = 17;
			this->FrameHeigth->Text = L"VER_FIRM: ";
			this->FrameHeigth->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label25
			// 
			this->label25->AutoSize = true;
			this->label25->Location = System::Drawing::Point(183, 55);
			this->label25->Name = L"label25";
			this->label25->Size = System::Drawing::Size(36, 13);
			this->label25->TabIndex = 16;
			this->label25->Text = L"Frame";
			// 
			// IP_TFT
			// 
			this->IP_TFT->AutoSize = true;
			this->IP_TFT->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->IP_TFT->Location = System::Drawing::Point(269, 29);
			this->IP_TFT->Name = L"IP_TFT";
			this->IP_TFT->Size = System::Drawing::Size(69, 15);
			this->IP_TFT->TabIndex = 15;
			this->IP_TFT->Text = L"VER_FIRM: ";
			// 
			// label24
			// 
			this->label24->AutoSize = true;
			this->label24->Location = System::Drawing::Point(183, 29);
			this->label24->Name = L"label24";
			this->label24->Size = System::Drawing::Size(20, 13);
			this->label24->TabIndex = 14;
			this->label24->Text = L"IP:";
			// 
			// AqusiitionCount
			// 
			this->AqusiitionCount->AutoSize = true;
			this->AqusiitionCount->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->AqusiitionCount->Location = System::Drawing::Point(269, 80);
			this->AqusiitionCount->Name = L"AqusiitionCount";
			this->AqusiitionCount->Size = System::Drawing::Size(15, 15);
			this->AqusiitionCount->TabIndex = 13;
			this->AqusiitionCount->Text = L"0";
			// 
			// label30
			// 
			this->label30->AutoSize = true;
			this->label30->Location = System::Drawing::Point(183, 80);
			this->label30->Name = L"label30";
			this->label30->Size = System::Drawing::Size(83, 13);
			this->label30->TabIndex = 12;
			this->label30->Text = L"Aquisition Count";
			// 
			// VER_BOARD
			// 
			this->VER_BOARD->AutoSize = true;
			this->VER_BOARD->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->VER_BOARD->Location = System::Drawing::Point(105, 147);
			this->VER_BOARD->Name = L"VER_BOARD";
			this->VER_BOARD->Size = System::Drawing::Size(69, 15);
			this->VER_BOARD->TabIndex = 11;
			this->VER_BOARD->Text = L"VER_FIRM: ";
			// 
			// VER_SCIN
			// 
			this->VER_SCIN->AutoSize = true;
			this->VER_SCIN->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->VER_SCIN->Location = System::Drawing::Point(105, 123);
			this->VER_SCIN->Name = L"VER_SCIN";
			this->VER_SCIN->Size = System::Drawing::Size(69, 15);
			this->VER_SCIN->TabIndex = 10;
			this->VER_SCIN->Text = L"VER_FIRM: ";
			// 
			// VER_TFTP
			// 
			this->VER_TFTP->AutoSize = true;
			this->VER_TFTP->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->VER_TFTP->Location = System::Drawing::Point(105, 100);
			this->VER_TFTP->Name = L"VER_TFTP";
			this->VER_TFTP->Size = System::Drawing::Size(69, 15);
			this->VER_TFTP->TabIndex = 9;
			this->VER_TFTP->Text = L"VER_FIRM: ";
			// 
			// VER_MAIN
			// 
			this->VER_MAIN->AutoSize = true;
			this->VER_MAIN->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->VER_MAIN->Location = System::Drawing::Point(105, 78);
			this->VER_MAIN->Name = L"VER_MAIN";
			this->VER_MAIN->Size = System::Drawing::Size(69, 15);
			this->VER_MAIN->TabIndex = 8;
			this->VER_MAIN->Text = L"VER_FIRM: ";
			// 
			// VER_FPGA
			// 
			this->VER_FPGA->AutoSize = true;
			this->VER_FPGA->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->VER_FPGA->Location = System::Drawing::Point(105, 54);
			this->VER_FPGA->Name = L"VER_FPGA";
			this->VER_FPGA->Size = System::Drawing::Size(69, 15);
			this->VER_FPGA->TabIndex = 7;
			this->VER_FPGA->Text = L"VER_FIRM: ";
			// 
			// VER_FIRM
			// 
			this->VER_FIRM->AutoSize = true;
			this->VER_FIRM->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->VER_FIRM->Location = System::Drawing::Point(105, 31);
			this->VER_FIRM->Name = L"VER_FIRM";
			this->VER_FIRM->Size = System::Drawing::Size(69, 15);
			this->VER_FIRM->TabIndex = 6;
			this->VER_FIRM->Text = L"VER_FIRM: ";
			// 
			// label23
			// 
			this->label23->AutoSize = true;
			this->label23->Location = System::Drawing::Point(18, 147);
			this->label23->Name = L"label23";
			this->label23->Size = System::Drawing::Size(73, 13);
			this->label23->TabIndex = 5;
			this->label23->Text = L"VER_BOARD";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(18, 125);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(60, 13);
			this->label10->TabIndex = 4;
			this->label10->Text = L"VER_SCIN";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(19, 102);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(62, 13);
			this->label9->TabIndex = 3;
			this->label9->Text = L"VER_TFTP";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(19, 78);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(65, 13);
			this->label8->TabIndex = 2;
			this->label8->Text = L"VER_MAIN:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(18, 54);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(66, 13);
			this->label7->TabIndex = 1;
			this->label7->Text = L"VER_FPGA:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(18, 31);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(67, 13);
			this->label6->TabIndex = 0;
			this->label6->Text = L"VER_FIRM: ";
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->fTDIToolStripMenuItem, 
				this->kY5ToolStripMenuItem, this->davincyToolStripMenuItem, this->�������������������ToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(686, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fTDIToolStripMenuItem
			// 
			this->fTDIToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->SetupFTDIToolStripMenuItem, 
				this->ConnectFTIToolStripMenuItem, this->DisaconectFTDIToolStripMenuItem});
			this->fTDIToolStripMenuItem->Name = L"fTDIToolStripMenuItem";
			this->fTDIToolStripMenuItem->Size = System::Drawing::Size(43, 20);
			this->fTDIToolStripMenuItem->Text = L"FTDI";
			// 
			// SetupFTDIToolStripMenuItem
			// 
			this->SetupFTDIToolStripMenuItem->Name = L"SetupFTDIToolStripMenuItem";
			this->SetupFTDIToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->SetupFTDIToolStripMenuItem->Text = L"���������";
			this->SetupFTDIToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::SetupFTDIToolStripMenuItem_Click);
			// 
			// ConnectFTIToolStripMenuItem
			// 
			this->ConnectFTIToolStripMenuItem->Name = L"ConnectFTIToolStripMenuItem";
			this->ConnectFTIToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->ConnectFTIToolStripMenuItem->Text = L"������������";
			this->ConnectFTIToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::ConnectFTIToolStripMenuItem_Click);
			// 
			// DisaconectFTDIToolStripMenuItem
			// 
			this->DisaconectFTDIToolStripMenuItem->Name = L"DisaconectFTDIToolStripMenuItem";
			this->DisaconectFTDIToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->DisaconectFTDIToolStripMenuItem->Text = L"�����������";
			this->DisaconectFTDIToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::DisaconectFTDIToolStripMenuItem_Click);
			// 
			// kY5ToolStripMenuItem
			// 
			this->kY5ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->���������ToolStripMenuItem, 
				this->�������������ToolStripMenuItem, this->�����������ToolStripMenuItem});
			this->kY5ToolStripMenuItem->Enabled = false;
			this->kY5ToolStripMenuItem->Name = L"kY5ToolStripMenuItem";
			this->kY5ToolStripMenuItem->Size = System::Drawing::Size(39, 20);
			this->kY5ToolStripMenuItem->Text = L"KY5";
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->BigFocuseToolStripMenuItem, 
				this->SmallFocuseToolStripMenuItem});
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->���������ToolStripMenuItem->Text = L"��� ������";
			// 
			// BigFocuseToolStripMenuItem
			// 
			this->BigFocuseToolStripMenuItem->Name = L"BigFocuseToolStripMenuItem";
			this->BigFocuseToolStripMenuItem->Size = System::Drawing::Size(126, 22);
			this->BigFocuseToolStripMenuItem->Text = L"�������";
			this->BigFocuseToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::BigFocuseToolStripMenuItem_Click);
			// 
			// SmallFocuseToolStripMenuItem
			// 
			this->SmallFocuseToolStripMenuItem->Name = L"SmallFocuseToolStripMenuItem";
			this->SmallFocuseToolStripMenuItem->Size = System::Drawing::Size(126, 22);
			this->SmallFocuseToolStripMenuItem->Text = L"�����";
			this->SmallFocuseToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::SmallFocuseToolStripMenuItem_Click);
			// 
			// �������������ToolStripMenuItem
			// 
			this->�������������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->UserSetupToolStripMenuItem});
			this->�������������ToolStripMenuItem->Name = L"�������������ToolStripMenuItem";
			this->�������������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->�������������ToolStripMenuItem->Text = L"��� ����������";
			// 
			// UserSetupToolStripMenuItem
			// 
			this->UserSetupToolStripMenuItem->Name = L"UserSetupToolStripMenuItem";
			this->UserSetupToolStripMenuItem->Size = System::Drawing::Size(176, 22);
			this->UserSetupToolStripMenuItem->Text = L"����������������";
			this->UserSetupToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::UserSetupToolStripMenuItem_Click);
			// 
			// �����������ToolStripMenuItem
			// 
			this->�����������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->CCDModeToolStripMenuItem, 
				this->TFTModetoolStripMenuItem});
			this->�����������ToolStripMenuItem->Name = L"�����������ToolStripMenuItem";
			this->�����������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->�����������ToolStripMenuItem->Text = L"��� ��������";
			// 
			// CCDModeToolStripMenuItem
			// 
			this->CCDModeToolStripMenuItem->Name = L"CCDModeToolStripMenuItem";
			this->CCDModeToolStripMenuItem->Size = System::Drawing::Size(98, 22);
			this->CCDModeToolStripMenuItem->Text = L"CCD";
			this->CCDModeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::CCDModeToolStripMenuItem_Click);
			// 
			// TFTModetoolStripMenuItem
			// 
			this->TFTModetoolStripMenuItem->Name = L"TFTModetoolStripMenuItem";
			this->TFTModetoolStripMenuItem->Size = System::Drawing::Size(98, 22);
			this->TFTModetoolStripMenuItem->Text = L"TFT";
			this->TFTModetoolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::TFTModetoolStripMenuItem_Click);
			// 
			// davincyToolStripMenuItem
			// 
			this->davincyToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->DavinchySetupToolStripMenuItem1, 
				this->ConectToTFTToolStripMenuItem, this->DisconectTFTToolStripMenuItem});
			this->davincyToolStripMenuItem->Name = L"davincyToolStripMenuItem";
			this->davincyToolStripMenuItem->Size = System::Drawing::Size(61, 20);
			this->davincyToolStripMenuItem->Text = L"Davincy";
			// 
			// DavinchySetupToolStripMenuItem1
			// 
			this->DavinchySetupToolStripMenuItem1->Name = L"DavinchySetupToolStripMenuItem1";
			this->DavinchySetupToolStripMenuItem1->Size = System::Drawing::Size(194, 22);
			this->DavinchySetupToolStripMenuItem1->Text = L"���������";
			this->DavinchySetupToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MainView::DavinchySetupToolStripMenuItem1_Click);
			// 
			// ConectToTFTToolStripMenuItem
			// 
			this->ConectToTFTToolStripMenuItem->Enabled = false;
			this->ConectToTFTToolStripMenuItem->Name = L"ConectToTFTToolStripMenuItem";
			this->ConectToTFTToolStripMenuItem->Size = System::Drawing::Size(194, 22);
			this->ConectToTFTToolStripMenuItem->Text = L"������������  �  TFT";
			this->ConectToTFTToolStripMenuItem->Visible = false;
			this->ConectToTFTToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::ConectToTFTToolStripMenuItem_Click);
			// 
			// DisconectTFTToolStripMenuItem
			// 
			this->DisconectTFTToolStripMenuItem->Enabled = false;
			this->DisconectTFTToolStripMenuItem->Name = L"DisconectTFTToolStripMenuItem";
			this->DisconectTFTToolStripMenuItem->Size = System::Drawing::Size(194, 22);
			this->DisconectTFTToolStripMenuItem->Text = L"����������� �� TFT";
			this->DisconectTFTToolStripMenuItem->Visible = false;
			this->DisconectTFTToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainView::DisconectTFTToolStripMenuItem_Click);
			// 
			// �������������������ToolStripMenuItem
			// 
			this->�������������������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->CalibrationSetupToolStripMenuItem2});
			this->�������������������ToolStripMenuItem->Name = L"�������������������ToolStripMenuItem";
			this->�������������������ToolStripMenuItem->Size = System::Drawing::Size(85, 20);
			this->�������������������ToolStripMenuItem->Text = L"����������";
			// 
			// CalibrationSetupToolStripMenuItem2
			// 
			this->CalibrationSetupToolStripMenuItem2->Name = L"CalibrationSetupToolStripMenuItem2";
			this->CalibrationSetupToolStripMenuItem2->Size = System::Drawing::Size(138, 22);
			this->CalibrationSetupToolStripMenuItem2->Text = L"���������";
			this->CalibrationSetupToolStripMenuItem2->Click += gcnew System::EventHandler(this, &MainView::CalibrationSetupToolStripMenuItem2_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {this->FTDIStripStatusLabel, 
				this->TFTStripStatusLabel, this->ProgressBarLoadingFrame, this->DavinchyTrace, this->HV_Diag, this->DetectorInternalState});
			this->statusStrip1->Location = System::Drawing::Point(0, 261);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(686, 22);
			this->statusStrip1->TabIndex = 2;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// FTDIStripStatusLabel
			// 
			this->FTDIStripStatusLabel->Name = L"FTDIStripStatusLabel";
			this->FTDIStripStatusLabel->Size = System::Drawing::Size(0, 17);
			// 
			// TFTStripStatusLabel
			// 
			this->TFTStripStatusLabel->Name = L"TFTStripStatusLabel";
			this->TFTStripStatusLabel->Size = System::Drawing::Size(0, 17);
			// 
			// ProgressBarLoadingFrame
			// 
			this->ProgressBarLoadingFrame->Name = L"ProgressBarLoadingFrame";
			this->ProgressBarLoadingFrame->Size = System::Drawing::Size(100, 16);
			this->ProgressBarLoadingFrame->Step = 1;
			// 
			// DavinchyTrace
			// 
			this->DavinchyTrace->Name = L"DavinchyTrace";
			this->DavinchyTrace->Size = System::Drawing::Size(0, 17);
			// 
			// HV_Diag
			// 
			this->HV_Diag->Name = L"HV_Diag";
			this->HV_Diag->Size = System::Drawing::Size(0, 17);
			// 
			// DetectorInternalState
			// 
			this->DetectorInternalState->Name = L"DetectorInternalState";
			this->DetectorInternalState->Size = System::Drawing::Size(0, 17);
			this->DetectorInternalState->TextImageRelation = System::Windows::Forms::TextImageRelation::Overlay;
			// 
			// CalibrationWorker
			// 
			this->CalibrationWorker->WorkerReportsProgress = true;
			this->CalibrationWorker->WorkerSupportsCancellation = true;
			this->CalibrationWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MainView::CalibrationWorker_DoWork);
			this->CalibrationWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MainView::CalibrationWorker_RunWorkerCompleted);
			// 
			// FrameWorker
			// 
			this->FrameWorker->WorkerReportsProgress = true;
			this->FrameWorker->WorkerSupportsCancellation = true;
			this->FrameWorker->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MainView::FrameWorker_DoWork);
			this->FrameWorker->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MainView::FrameWorker_RunWorkerCompleted);
			// 
			// MainView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(686, 283);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip1);
			this->DoubleBuffered = true;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MainView";
			this->Text = L"1717SCC Control pult";
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->FileformatgroupBox->ResumeLayout(false);
			this->groupBox7->ResumeLayout(false);
			this->groupBox7->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->groupBox6->ResumeLayout(false);
			this->groupBox6->PerformLayout();
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->tabPage3->ResumeLayout(false);
			this->groupBox5->ResumeLayout(false);
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void SetupFTDIToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {


		if (!_FTDI->Visible)
		{
			if (_FTDI->IsDisposed)
			{
				_FTDI = gcnew FTDIView();
				_FTDI->SetParametrsFTDI(nSpeed,
					nDataBits,
					nStopBits);
				_FTDI->SetMXComm(_MXCOMM);
			}
			else
			{

				_FTDI->SetParametrsFTDI(nSpeed,
					nDataBits,
					nStopBits);
				_FTDI->SetMXComm(_MXCOMM);

			}

			if (_FTDI->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			{
				_FTDI->GetParametrsFTDI(nSpeed,
					nDataBits,
					nStopBits);


				SelectedDevice = _FTDI->GetSelectedDeviceID();

			}
		}


	}
	private: System::Void ConnectFTIToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		int HV;
		int Current;
		int Precurrent;
		float mAs;
		int Time;
		byte URPV;
		BYTE RD;
		if (_MXCOMM->ConfigureByID(SelectedDevice, nSpeed, nDataBits, nParity, nStopBits))
		{
			_MXCOMM->SetTimeout(120);
			_KY5 = new KY5_Commands_19200(_MXCOMM);
			kY5ToolStripMenuItem->Enabled = true;
			ConnectFTIToolStripMenuItem->Checked = true;
			DisaconectFTDIToolStripMenuItem->Checked = false;
			_KY5->ExpositionTimeIsShort();

			StartCalibration->Enabled = true;
			BrightFrame->Enabled = true;
			X_RayTest->Enabled = true;

	
	 _KY5->SetDeviceType(0x40);
	 _KY5->GetDeviceType(&RD);
	 if (RD == 0x40)
	 {
		
		 TFTModetoolStripMenuItem->Checked = true;
		 CCDModeToolStripMenuItem->Checked = false;
	 }	
	    _KY5->FocusIsBig();
		_KY5->ReadRegisters();

	if (_KY5->IsBigFocus())
	{
		BigFocuseToolStripMenuItem->Checked = true;
		SmallFocuseToolStripMenuItem->Checked = false;
	}
	else
	{

		BigFocuseToolStripMenuItem->Checked = false;
		SmallFocuseToolStripMenuItem->Checked = true;

	}

	 _KY5->GetAnodeHV(&HV);
	 _KY5->GetAnodeBigFocusPreCurrent(&Precurrent);
	 _KY5->GetAnodeBigFocusCurrent(&Current);
	 _KY5->GetMAS(&mAs);
	 _KY5->GetExpositionTime(&Time);
	 _KY5->GetURPVersion(&URPV);
	  HVVoltage->Text = HV.ToString();
	  HVPrecurent->Text = Precurrent.ToString();
	  HVTime->Text =  Time.ToString();
	  HVCurrent->Text  = Current.ToString();
	  HVmAs->Text =  mAs.ToString();
	  FTDIStripStatusLabel->Text = "URP Version: " + URPV.ToString();
		}
		else
		{

			MessageBox::Show("���������� ������� ��������� FTDI!");


		}


	}
 /********************************************************************************************/
	private: System::Void DisaconectFTDIToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		
		_MXCOMM->Close();
		kY5ToolStripMenuItem->Enabled = false;
		ConnectFTIToolStripMenuItem->Checked = false;
		DisaconectFTDIToolStripMenuItem->Checked = true;
		StartCalibration->Enabled = false;
		BrightFrame->Enabled = false;
		X_RayTest->Enabled = false;


	}
/********************************************************************************************/
	private: System::Void BigFocuseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

	BigFocuseToolStripMenuItem->Checked = true;
	SmallFocuseToolStripMenuItem->Checked = false;
	_KY5->FocusIsBig();
	int HV;
	int Current;
	int Precurrent;
	float mAs;
	int Time;
	_KY5->ReadRegisters();	
	_KY5->GetAnodeHV(&HV);
	_KY5->GetAnodeBigFocusPreCurrent(&Precurrent);
	_KY5->GetAnodeBigFocusCurrent(&Current);
	 _KY5->GetMAS(&mAs);
	 _KY5->GetExpositionTime(&Time);
	 HVVoltage->Text = HV.ToString();
	 HVPrecurent->Text = Precurrent.ToString();
	 HVTime->Text =  Time.ToString();
	 HVCurrent->Text  = Current.ToString();
	 HVmAs->Text =  mAs.ToString();


	}
/********************************************************************************************/
	private: System::Void SmallFocuseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		BigFocuseToolStripMenuItem->Checked = false;
		SmallFocuseToolStripMenuItem->Checked = true;
		_KY5->FocusIsSmall();
		_KY5->ReadRegisters();
	  int HV;
	  int Current;
	  int Precurrent;
	  float mAs;
	  int Time;
	 _KY5->GetAnodeHV(&HV);	
	 _KY5->GetAnodeSmallFocusPreCurrent(&Precurrent);
	 _KY5->GetAnodeSmallFocusCurrent(&Current);
	 _KY5->GetMAS(&mAs);
	 _KY5->GetExpositionTime(&Time);

	HVVoltage->Text = HV.ToString();
	HVPrecurent->Text = Precurrent.ToString();
	HVTime->Text =  Time.ToString();
	HVCurrent->Text  = Current.ToString();
	HVmAs->Text =  mAs.ToString();

	}
/********************************************************************************************/
	private: System::Void UserSetupToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		if (!_ExpoParamsSetup->Visible)
		{
			if (_ExpoParamsSetup->IsDisposed)
			{
				_ExpoParamsSetup = gcnew ExspositionParams();
			}

			_ExpoParamsSetup->Show();

		}
	}
/********************************************************************************************/
	private: System::Void CalibrationSetupToolStripMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {

		if (!_CalibrationSetup->Visible)
		{
			if (_CalibrationSetup->IsDisposed)
			{
				_CalibrationSetup = gcnew CalibrationParametrsView();
			}
			_CalibrationSetup->Show();
		}


	}
 /********************************************************************************************/
	private: System::Void DavinchySetupToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {

		if (!_DavinchySetup->Visible)
		{
			if (_DavinchySetup->IsDisposed)
			{
				_DavinchySetup = gcnew  DavinchySetup();
			}

			_DavinchySetup->Show();

		}

	}
/********************************************************************************************/
	private: System::Void X_RayTest_Click(System::Object^  sender, System::EventArgs^  e) {
		
		if (!FrameWorker->IsBusy)
		{

			_KY5->ReadRegisters();
			if (_KY5->IsDeviceReady())
			{
			HV_Diag->Text = "KY5 ����� � ������ ��������";
			_Mode = MODE::MODE_BRIGHT;
			Shoot_XRayFrame();
			FrameWorker->RunWorkerAsync();
			}
			else
			{
				HV_Diag->Text = "KY5 ������� � ������ ��������";

			}

		}
		else
		{
			MessageBox::Show("���������� ������ ��������! ��������� ����� ���������!" );
		}
		

	}
 /********************************************************************************************/
void Shoot_XRayFrame(void)
{

	
	int HV         = 0;
	int Current    = 0;
	int Precurrent = 0;
	float mAs	   = 0;
	int Time	   = 0;
	int RHV		   = 0;
	int RCurrent   = 0;
	float RmAs	   = 0;
	int RTime	   = 0;


	_KY5->ExpositionTimeIsShort();
	int::TryParse(HVVoltage->Text, HV);
	int::TryParse(HVPrecurent->Text, Precurrent);
	int::TryParse(HVTime->Text, Time);
	int::TryParse(HVCurrent->Text, Current);
	float::TryParse(HVmAs->Text, mAs);
	_KY5->ReadRegisters();

	if (_KY5->IsBigFocus())
	{

		_KY5->SetMaxAnodeBigFocusCurrent(Current);
		_KY5->SetAnodeBigFocusCurrent(Current);
		_KY5->SetMaxAnodeBigFocusPreCurrent(Precurrent);
		_KY5->SetAnodeBigFocusPreCurrent(Precurrent);

	}
	else
	{

		_KY5->SetMaxAnodeSmallFocusCurrent(Current);
		_KY5->SetAnodeSmallFocusCurrent(Current);
		_KY5->SetMaxAnodeSmallFocusPreCurrent(Precurrent);
		_KY5->SetAnodeSmallFocusPreCurrent(Precurrent);

	};

	_KY5->SetExpositionTime(Time);
	_KY5->SetMaxAnodeHV(HV);
	_KY5->SetAnodeHV(HV);
	_KY5->SetMAS(mAs);
	_KY5->WaitForMinWhileBetweenSnapshots();

}
/********************************************************************************************/

void Shoot_XRaySingle(void)
{

	
	int HV         = 0;
	int Current    = 0;
	int Precurrent = 0;
	float mAs	   = 0;
	int Time	   = 0;
	int RHV		   = 0;
	int RCurrent   = 0;
	float RmAs	   = 0;
	int RTime	   = 0;

	_KY5->ExpositionTimeIsShort();

	int::TryParse(HVVoltage->Text, HV);
	int::TryParse(HVPrecurent->Text, Precurrent);
	int::TryParse(HVTime->Text, Time);
	int::TryParse(HVCurrent->Text, Current);
	float::TryParse(HVmAs->Text, mAs);
	_KY5->ReadRegisters();

	if (_KY5->IsBigFocus())
	{

		_KY5->SetMaxAnodeBigFocusCurrent(Current);
		_KY5->SetAnodeBigFocusCurrent(Current);
		_KY5->SetMaxAnodeBigFocusPreCurrent(Precurrent);
		_KY5->SetAnodeBigFocusPreCurrent(Precurrent);

	}
	else
	{

		_KY5->SetMaxAnodeSmallFocusCurrent(Current);
		_KY5->SetAnodeSmallFocusCurrent(Current);
		_KY5->SetMaxAnodeSmallFocusPreCurrent(Precurrent);
		_KY5->SetAnodeSmallFocusPreCurrent(Precurrent);

	};

	_KY5->SetExpositionTime(Time);
	_KY5->SetMaxAnodeHV(HV);
	_KY5->SetAnodeHV(HV);
	_KY5->SetMAS(mAs);

	_KY5->Snapshot_Preparing();
	_KY5->WaitForPreparingStart(2000, 300);
	_KY5->Snapshot_HV_On();
	_KY5->WaitForSnapshotFinish(2000, 200);
	_KY5->Snapshot_HV_Off();

	_KY5->ReadRegisters();
	_KY5->GetMeasuredMAS(&RmAs);
	_KY5->GetMeasuredExpositionTime(&RTime);
	_KY5->GetMeasuredNegativeAnodeCurrent(&RCurrent);
	_KY5->GetMeasuredAnodeHV(&RHV);

	REHV = RHV;
    RECurrent = RCurrent;
    REmAs = RmAs;
    RETime = RTime;


	ReadedCurrent->Text = RCurrent.ToString();
	ReadedHV->Text = RHV.ToString();
	ReadedmAs->Text = RmAs.ToString();
	ReadedTime->Text = RTime.ToString();


}
/********************************************************************************************/
    private: System::Void TFTModetoolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
      BYTE RD;
	 _KY5->SetDeviceType(0x40);
	 _KY5->GetDeviceType(&RD);
	 if (RD == 0x40)
	 {
		
		 TFTModetoolStripMenuItem->Checked = true;
		 CCDModeToolStripMenuItem->Checked = false;
	 }


  }
 /********************************************************************************************/
private: System::Void CCDModeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
           
      BYTE RD;
	 _KY5->SetDeviceType(0x1);
	 _KY5->GetDeviceType(&RD);
	 if (RD == 0x1)
	 {
		 TFTModetoolStripMenuItem->Checked = false;
		 CCDModeToolStripMenuItem->Checked = true;

	 }

         }
/********************************************************************************************/
private: System::Void DarkCurrentFrame_Click(System::Object^  sender, System::EventArgs^  e) {
	_Mode = MODE::MODE_DC;
	TFTStripStatusLabel->Text = "DC ������ ����������";
	VADAV_StartDarkFrame();
	
}
/********************************************************************************************/
private: System::Void BrightFrame_Click(System::Object^  sender, System::EventArgs^  e) {
	


	if (!FrameWorker->IsBusy)
	{
		_Mode = MODE::MODE_BRIGHT;
		TFTStripStatusLabel->Text = "XRAY ������ ����������";
		if (VADAV_StartBrightFrame())
		{

			Shoot_XRayFrame();
			_KY5->ReadRegisters();
			if (_KY5->IsDeviceReady())
			{
				HV_Diag->Text = "KY5 ����� � ������ ��������";
				FrameWorker->RunWorkerAsync();
			}
			else
			{
				HV_Diag->Text = "KY5 ������� � ������ ��������";
			}
		}
	}
}
/********************************************************************************************/
private: System::Void RebootTFT_Click(System::Object^  sender, System::EventArgs^  e) {
	VADAV_Reboot();
}
/********************************************************************************************/
private: System::Void VersionsTFT_Click(System::Object^  sender, System::EventArgs^  e) {
	VADAV_GetFirmwareVersions();
}
/********************************************************************************************/
private: System::Void AQCount_Click(System::Object^  sender, System::EventArgs^  e) {
	VADAV_GetACQCount();
}
/********************************************************************************************/
private: System::Void ShutdownTFT_Click(System::Object^  sender, System::EventArgs^  e) {
	VADAV_Shutdown();
}

/*********************************************************************/
  bool DeleteDirectory(const char* sPath) {
    HANDLE hFind;  // file handle
    WIN32_FIND_DATA FindFileData;

    TCHAR DirPath[MAX_PATH];
    TCHAR FileName[MAX_PATH];

    strcpy(DirPath,sPath);
    strcat(DirPath,"\\*");    // searching all files
    strcpy(FileName,sPath);
    strcat(FileName,"\\");

    hFind = FindFirstFile(DirPath,&FindFileData); // find the first file
    if(hFind == INVALID_HANDLE_VALUE) return FALSE;
    strcpy(DirPath,FileName);
        
    bool bSearch = true;
    while(bSearch) { // until we finds an entry
        if(FindNextFile(hFind,&FindFileData)) {
            if(IsDots(FindFileData.cFileName)) continue;
            strcat(FileName,FindFileData.cFileName);
            if((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {

                // we have found a directory, recurse
                if(!DeleteDirectory(FileName)) { 
                    FindClose(hFind); 
                    return FALSE; // directory couldn't be deleted
                }
                RemoveDirectory(FileName); // remove the empty directory
                strcpy(FileName,DirPath);
            }
            else {
                if(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
                    _chmod(FileName, _S_IWRITE); // change read-only file mode 
                if(!DeleteFile(FileName)) {  // delete the file
                    FindClose(hFind); 
                    return FALSE; 
                }                 
                strcpy(FileName,DirPath);
            }
        }
        else {
            if(GetLastError() == ERROR_NO_MORE_FILES) // no more files there
            bSearch = false;
            else {
                // some error occured, close the handle and return FALSE
                FindClose(hFind); 
                return FALSE;
            }

        }

    }
    FindClose(hFind);  // closing file handle
 
    return RemoveDirectory(sPath); // remove the empty directory

}
/*********************************************************************/
bool  IsDots(const TCHAR* str) {
    if(strcmp(str,".") && strcmp(str,"..")) return FALSE;
    return TRUE;
}


/********************************************************************************************/
private: System::Void CalibrationWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {

	char* FileFData;

	
	IntPtr^ Rf = gcnew IntPtr();
	Rf = (IntPtr^) e->Argument;
	HWND Receivwer = (HWND) (Rf->ToPointer());
	float It = CalibreConfig->GetCmAsBegin();
	int RHV = 0;
	int RCurrent = 0;
	float RmAs = 0;
	int RTime = 0;
    int FrameCounter = 0;
	int TargetHV;

	char FileName[255];
	char mAsString[255];
	char FileFilesContent[65535];
	char HVString[255];
	char FileCalibrStruct[MAX_PATH];
	char FolderPath[1024];
	char mAsFolderName[1024];
	FILE * FileFile;
	char Message[255];
	memset(FolderPath,0,1024);
	memset(Message,0,255);

	if (!CalibreConfig->Open())
	{
		return;
	}

	CalibreConfig->ReadConfig();
	float MasBegin=CalibreConfig->GetCmAsBegin(); 
	float MasEnd = CalibreConfig->GetCmAsEnd();
	int Steps = CalibreConfig->GetCmAsSteps() - 1;
	
	float ITMass = MasBegin;
	int Dellay = CalibreConfig->GetCDellayBeatweanSnapshots();

	float Incr = ( MasEnd - MasBegin ) /(float)Steps ;

	if (CalibreConfig->GetCTypeFocuse())
	{
		_KY5->FocusIsBig();
	}
	else
	{
		_KY5->FocusIsSmall();
	}

	CalibreConfig->Close();
	_KY5->ReadRegisters();
	

	
	_KY5->SetMaxAnodeHV(CalibreConfig->GetCHV());
	_KY5->SetAnodeHV(CalibreConfig->GetCHV());

	TargetHV = CalibreConfig->GetCHV();

     _CHV = TargetHV;
	 

	   sprintf(HVString, "%i_kV", TargetHV);
	   strcat(FolderPath,"Calibrate");
	   strcat(FolderPath,"/");
	   strcat(FolderPath,HVString);
		DeleteDirectory(FolderPath);
		CreateDirectory(FolderPath,NULL);
		
	_KY5->SetExpositionTime(100);
	if (_KY5->IsBigFocus())
	{

		_KY5->SetMaxAnodeBigFocusCurrent(200);
		_KY5->SetAnodeBigFocusCurrent(200);
		_KY5->SetMaxAnodeBigFocusPreCurrent(180);
		_KY5->SetAnodeBigFocusPreCurrent(180);

	}
	else
	{

		_KY5->SetMaxAnodeSmallFocusCurrent(200);
		_KY5->SetAnodeSmallFocusCurrent(180);
		_KY5->SetMaxAnodeSmallFocusPreCurrent(200);
		_KY5->SetAnodeSmallFocusPreCurrent(180);

	};
	    sprintf(mAsString, "%.2f_mAs\n", 0.0);
		sprintf(mAsFolderName,"%.2f_mAs",0.0);
		strcat(FileFilesContent, mAsString);
		_CmAs = 0.0;




		    memset(FolderPath,0,1024);
			strcat(FolderPath,"Calibrate");
			strcat(FolderPath,"/");
			strcat(FolderPath,HVString);
			strcat(FolderPath,"/");
			strcat(FolderPath,mAsFolderName);
			CreateDirectory(FolderPath,NULL);


	//��������� DC
   
    for( int it = 0;it<CalibreConfig->GetCStatic()*2;it++)
	{

	ImageLoadComplete->Reset();
	if (!VADAV_StartBrightFrame())
	{

		return;
	}


	    _KY5->Snapshot_Preparing();
		_KY5->WaitForPreparingStart(4000, 300);
		_KY5->Snapshot_HV_On();
		_KY5->WaitForSnapshotFinish(4000, 200);
		_KY5->Snapshot_HV_Off();
		ImageLoadComplete->Wait();
		
		_KY5->GetMeasuredMAS(&RmAs);
		_KY5->GetMeasuredExpositionTime(&RTime);
		_KY5->GetMeasuredNegativeAnodeCurrent(&RCurrent);
		_KY5->GetMeasuredAnodeHV(&RHV);
		_KY5->WaitForMinWhileBetweenSnapshots();
		Sleep(Dellay);
		
	
			
		REHV = RHV;
		RECurrent = RCurrent;
		REmAs = RmAs;
		RETime = RTime;
		Params->Current = RCurrent;
		Params->HV = RHV;
		Params->Mas = RmAs;
		Params->Time = RTime;
		
		PostMessageA(Receivwer, 0x10057, (long)Params, 0);
	}


		if (_KY5->IsBigFocus())
	{

		_KY5->SetMaxAnodeBigFocusCurrent(CalibreConfig->GetCCurent());
		_KY5->SetAnodeBigFocusCurrent(CalibreConfig->GetCCurent());
		_KY5->SetMaxAnodeBigFocusPreCurrent(CalibreConfig->GetCPrecurent());
		_KY5->SetAnodeBigFocusPreCurrent(CalibreConfig->GetCPrecurent());

	}
	else
	{

		_KY5->SetMaxAnodeSmallFocusCurrent(CalibreConfig->GetCCurent());
		_KY5->SetAnodeSmallFocusCurrent(CalibreConfig->GetCCurent());
		_KY5->SetMaxAnodeSmallFocusPreCurrent(CalibreConfig->GetCPrecurent());
		_KY5->SetAnodeSmallFocusPreCurrent(CalibreConfig->GetCPrecurent());

	};


   _KY5->SetExpositionTime(CalibreConfig->GetCTimeLimit());
	//��������� XRAY
	  for( int it = 0;it<CalibreConfig->GetCmAsSteps();it++)
{

       FrameCounter++;
		PostMessageA(Receivwer, 0x10056, FrameCounter, FrameCounter);
		CriticalRegion->Enter();
		_CmAs = ITMass;
		CriticalRegion->Leave();
	   _KY5->SetMAS(ITMass);
		

		    sprintf(mAsString, "%.2f_mAs\n", ITMass);
		    sprintf(mAsFolderName,"%.2f_mAs",ITMass);
			strcat(FileFilesContent, mAsString);
		    memset(FolderPath,0,1024);
			strcat(FolderPath,"Calibrate");
			strcat(FolderPath,"/");
			strcat(FolderPath,HVString);
			strcat(FolderPath,"/");
			strcat(FolderPath,mAsFolderName);
			CreateDirectory(FolderPath,NULL);


	 for( int it = 0;it<CalibreConfig->GetCStatic();it++)
	{


		ImageLoadComplete->Reset();
		if (!VADAV_StartBrightFrame())
		{
			return;
		}
		_KY5->Snapshot_Preparing();
		_KY5->WaitForPreparingStart(4000, 300);
		_KY5->Snapshot_HV_On();
		_KY5->WaitForSnapshotFinish(4000, 200);
		_KY5->Snapshot_HV_Off();
		 ImageLoadComplete->Wait();

		_KY5->GetMeasuredMAS(&RmAs);
		_KY5->GetMeasuredExpositionTime(&RTime);
		_KY5->GetMeasuredNegativeAnodeCurrent(&RCurrent);
		_KY5->GetMeasuredAnodeHV(&RHV);
		CriticalRegion->Enter();
		REHV = RHV;
		RECurrent = RCurrent;
		REmAs = RmAs;
		RETime = RTime;
		
		Params->Current = RCurrent;
		Params->HV = RHV;
		Params->Mas = RmAs;
		Params->Time = RTime;
		CriticalRegion->Leave();
		PostMessageA(Receivwer, 0x10057, (long)Params, 0);
	   _KY5->WaitForMinWhileBetweenSnapshots();
	   Sleep(Dellay);
	 }
		if (e->Cancel)
		{
			return;
		}
		ITMass += Incr;
}

            memset(FolderPath,0,1024);
			strcat(FolderPath,"Calibrate");
			strcat(FolderPath,"/");
			strcat(FolderPath,HVString);
			strcat(FolderPath,"/");
			strcat(FolderPath,"FileFiles.txt");

		
		  FileFile = fopen(FolderPath, "w");
		  fprintf(FileFile, "%s", FileFilesContent);
		  fflush(FileFile);
		  fclose(FileFile);
	  

		  const int LoadFileOfFileSuccess = Avergae->LoadFileOfFile(FolderPath);

		  if (!LoadFileOfFileSuccess)
		  {
			  
		  }
		  else
		  {



		  }


	   memset(FolderPath,0,1024);
	   sprintf(HVString, "%i_kV", TargetHV);
	   strcat(FolderPath,"Calibrate");
	   strcat(FolderPath,"/");
	   strcat(FolderPath,HVString);

		  const int AverageSuccess = Avergae->Average(FolderPath);

		  if (!AverageSuccess)
		  {
			  

		  }
		  else
		  {

		  }

	 
		  const int PreloadAllImagesSuccess = Calibrate->PreloadAllImages(FolderPath);

		  if (!PreloadAllImagesSuccess)
		  {

		  }
		  else
		  {

		  }

		  const int CalibrateSuccess = Calibrate->Calibrate(0, 0, (float)(TargetHV));
		
		  memset(FileCalibrStruct, '\0', MAX_PATH);
		  sprintf(FileCalibrStruct, "%d_%d.COF", TargetHV, 0);
		  const int WriteFitStructSuccess = Calibrate->WriteFitStruct(FileCalibrStruct);

		  if (!WriteFitStructSuccess)
		  {

		  }
		  else
		  {

		  }
		  Calibrate->ClearImgFileMap();
		  Calibrate->ClearFitStructMap();
	  
}
/********************************************************************************************/
private: System::Void FrameWorker_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
	
	if (_Mode == MODE::MODE_BRIGHT)
	{
		
	_KY5->Snapshot_Preparing();
	_KY5->WaitForPreparingStart(4000, 300);
	_KY5->Snapshot_HV_On();
	_KY5->WaitForSnapshotFinish(4000, 200);
	_KY5->Snapshot_HV_Off();
	}
}
 /********************************************************************************************/
private: System::Void StartCalibration_Click(System::Object^  sender, System::EventArgs^  e) {
	_Mode = MODE_BRIGHT_Calibration;
	if (!CalibreConfig->Open())
	{
		MessageBox::Show("��������� ���������� �� �������!���������� ��������!");
		return;
	};
	CalibreConfig->ReadConfig();
	CalibreConfig->Close();

	if (!CalibrationWorker->IsBusy)
	{
			_KY5->ReadRegisters();
			if (_KY5->IsDeviceReady())
			{
				CalibrationSetupToolStripMenuItem2->Enabled = false;
				StartCalibration->Enabled = false;
				BrightFrame->Enabled = false;
				X_RayTest->Enabled = false;

				HV_Diag->Text = "KY5 ����� � ������ ��������";
				StatID->Text = "������ ���������� ��� " + CalibreConfig->GetCHV() + "Kv";
				CalibrationWorker->RunWorkerAsync(this->Handle);
			}
			else
			{
				HV_Diag->Text = "KY5 ������� � ������ ��������";
				StatID->Text = "��������� ���������� KY5 � ��������� �������!";
			}
	}
	else
	{
		MessageBox::Show("�������� ��������� ����������!��������� ���� ����������� �������!");
	}

	
}
/********************************************************************************************/
private: System::Void CancelCalibratin_Click(System::Object^  sender, System::EventArgs^  e) {
		CalibrationWorker->CancelAsync();
}
/********************************************************************************************/
private: System::Void CalibrationWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {
	CalibrationSetupToolStripMenuItem2->Enabled = true;
	StartCalibration->Enabled = true;
	BrightFrame->Enabled = true;
	X_RayTest->Enabled = true;
	StatID->Text = "��������� ���������� ���������!";
         }
/********************************************************************************************/
private: System::Void ConectToTFTToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
       VADAV_ConnectForBright();

		 }
/********************************************************************************************/
private: System::Void DisconectTFTToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 VADAV_CloseACQ();
		 }
/********************************************************************************************/
private: System::Void FrameWorker_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {
		 
	int RHV = 0;
	int RCurrent = 0;
	float RmAs = 0;
	int RTime = 0;
	_KY5->ReadRegisters();
	_KY5->GetMeasuredMAS(&RmAs);
	_KY5->GetMeasuredExpositionTime(&RTime);
	_KY5->GetMeasuredNegativeAnodeCurrent(&RCurrent);
	_KY5->GetMeasuredAnodeHV(&RHV);
	REHV = RHV;
    RECurrent = RCurrent;
    REmAs = RmAs;
    RETime = RTime;
	ReadedCurrent->Text = RCurrent.ToString();
	ReadedHV->Text = RHV.ToString();
	ReadedmAs->Text = RmAs.ToString();
	ReadedTime->Text = RTime.ToString();

		 }
};
/********************************************************************************************/
}

