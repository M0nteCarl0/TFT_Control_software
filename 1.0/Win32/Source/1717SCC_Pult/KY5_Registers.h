//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
#ifndef KY5_REGISTERS_H
#define KY5_REGISTERS_H


//------registers------
#define KY5_READ_STATE_ADDRESS 0x81
#define KY5_READ_STATE_COMMAND 0x80

#define KY5_DEVICE_NUMBER_7_HO_BITS_ADDRESS 0x9A // This register contains a version number

#define KY5_SUPPLY_ADDRESS 0x80
#define KY5_SNAPSHOT_ADDRESS 0x90
#define KY5_FOCUS_ADDRESS 0x97
#define KY5_DEVICE_TYPE_ADDRESS 0x98
#define KY5_GIVEN_EXPOSITION_TIME_7_LO_BITS_ADDRESS 0xAB
#define KY5_GIVEN_EXPOSITION_TIME_7_HO_BITS_ADDRESS 0xAC
#define KY5_ELEVATOR_ADDRESS 0x91
#define KY5_GIVEN_ANODE_VOLTAGE_3_LO_BITS_ADDRESS 0x9D
#define KY5_GIVEN_ANODE_VOLTAGE_7_HO_BITS_ADDRESS 0x9E
#define KY5_GIVEN_ANODE_CURRENT_3_LO_BITS_ADDRESS 0x9F
#define KY5_GIVEN_ANODE_CURRENT_7_HO_BITS_ADDRESS 0xA0
#define KY5_GIVEN_ANODE_SMALL_FOCUS_PRECURRENT_3_LO_BITS_ADDRESS 0xA1
#define KY5_GIVEN_ANODE_SMALL_FOCUS_PRECURRENT_7_HO_BITS_ADDRESS 0xA2
#define KY5_GIVEN_ANODE_SMALL_FOCUS_CURRENT_3_LO_BITS_ADDRESS 0xA3
#define KY5_GIVEN_ANODE_SMALL_FOCUS_CURRENT_7_HO_BITS_ADDRESS 0xA4
#define KY5_GIVEN_ANODE_BIG_FOCUS_PRECURRENT_3_LO_BITS_ADDRESS 0xA5
#define KY5_GIVEN_ANODE_BIG_FOCUS_PRECURRENT_7_HO_BITS_ADDRESS 0xA6
#define KY5_GIVEN_ANODE_BIG_FOCUS_CURRENT_3_LO_BITS_ADDRESS 0xA7
#define KY5_GIVEN_ANODE_BIG_FOCUS_CURRENT_7_HO_BITS_ADDRESS 0xA8
#define KY5_GIVEN_MAS_7_LO_BITS_ADDRESS 0xA9
#define KY5_GIVEN_MAS_7_HO_BITS_ADDRESS 0xAA
#define KY5_MAX_ANODE_CURRENT_ADDRESS 0xAD
#define KY5_MAX_ANODE_VOLTAGE_ADDRESS 0xAE
#define KY5_MAX_ANODE_SMALL_FOCUS_PRECURRENT_ADDRESS 0xAF
#define KY5_MAX_ANODE_SMALL_FOCUS_CURRENT_ADDRESS 0xB0
#define KY5_MAX_ANODE_BIG_FOCUS_PRECURRENT_ADDRESS 0xB1
#define KY5_MAX_ANODE_BIG_FOCUS_CURRENT_ADDRESS 0xB2
#define KY5_DOSE_COEFFICIENT_DEPENDING_ON_VOLTAGE_ADDRESS 0xB7
#define KY5_BLOCKINGS_1_ADDRESS 0xD0
#define KY5_BLOCKINGS_2_ADDRESS 0xD1
#define KY5_BLOCKINGS_3_ADDRESS 0xD2
#define KY5_MESSAGES_ADDRESS 0xD3
#define KY5_WARNINGS_ADDRESS 0xD4
#define KY5_MEASURED_EXPOSITION_TIME_7_LO_BITS_ADDRESS 0xE8
#define KY5_MEASURED_EXPOSITION_TIME_7_HO_BITS_ADDRESS 0xE9
#define KY5_MEASURED_TEMPERATURE_OF_ENVIRONMENT_ADDRESS 0xF8
#define KY5_MEASURED_TEMPERATURE_OF_POWER_ELEMENTS_ADDRESS 0xF9
#define KY5_MEASURED_TEMPERATUE_OF_OIL 0xFA
#define KY5_MEASURED_POSITIVE_ANODE_VOLTAGE_3_LO_BITS_ADDRESS 0xE0
#define KY5_MEASURED_POSITIVE_ANODE_VOLTAGE_7_HO_BITS_ADDRESS 0xE1
#define KY5_MEASURED_POSITIVE_ANODE_CURRENT_3_LO_BITS_ADDRESS 0xE4
#define KY5_MEASURED_POSITIVE_ANODE_CURRENT_7_HO_BITS_ADDRESS 0xE5
#define KY5_MEASURED_NEGATIVE_ANODE_CURRENT_3_LO_BITS_ADDRESS 0xE6
#define KY5_MEASURED_NEGATIVE_ANODE_CURRENT_7_HO_BITS_ADDRESS 0xE7
#define KY5_MEASURED_MAS_7_LO_BITS_ADDRESS 0xEA
#define KY5_MEASURED_MAS_7_HO_BITS_ADDRESS 0xEB
#define	KY5_MEASURED_PRECURRENT_3_LO_BITS_ADDRESS 0xEE
#define	KY5_MEASURED_PRECURRENT_7_HO_BITS_ADDRESS 0xEF
#define KY5_MEASURED_CURRENT_DURING_SNAPSHOT_3_LO_BITS_ADDRESS 0xF0
#define KY5_MEASURED_CURRENT_DURING_SNAPSHOT_7_HO_BITS_ADDRESS 0xF1
#define	KY5_ANODE_ROTATION_FREQUENCY_ADDRESS 0xBA
#define KY5_MIN_ANODE_ROTATION_FREQUENCY_ADDRESS 0xBB
#define KY5_MEASURED_ANODE_ROTATION_FREQUENCY_ADDRESS 0xFB

#define KY5_DEVICE_TYPE_IS_PROSCAN_2000 0x01
#define KY5_DEVICE_TYPE_IS_PROSCAN_7000 0x02
#define KY5_DEVICE_TYPE_IS_12F9 0x04
#define KY5_DEVICE_TYPE_IS_PROGRAPH_4000 0x08
#define KY5_DEVICE_TYPE_IS_PROGRAPH_16000 0x10
#define KY5_DEVICE_TYPE_IS_UAZ_SCAN 0x20
#define KY5_DEVICE_TYPE_IS_UAZ_CCD 0x40
#define KY5_DEVICE_TYPE_IS_0x42 0x42

#define KY5_SNAPSHOT_PREPARING 0x01
#define KY5_SNAPSHOT_HV_ON 0x03
#define KY5_SNAPSHOT_HV_ON_IN_MULTIPLE_MODE 0x05
#define KY5_SNAPSHOT_HV_OFF 0x00
#define KY5_SNAPSHOT_FINISH_TIMEOUT_BY_DEFAULT 200
#define KY5_SNAPSHOT_MULTIPLE_MODE_TIMEOUT_BY_DEFAULT 200

#define KY5_FOCUS_IS_SMALL 0x00
#define KY5_FOCUS_IS_BIG 0x01

#define KY5_HV_EXPOSITION_TIME_SCALE_ADDRESS 0x99
#define KY5_EXPOSITION_TIME_IS_SHORT 0xa//0x3f
#define KY5_EXPOSITION_TIME_IS_LONG 0x2f

#define KY5_BLOCKINGS_1_NO_ANODE_ACCELERATION 0x01
#define KY5_BLOCKINGS_1_EXPOSITION_TIME_EXCESS 0x02
#define KY5_BLOCKINGS_1_DEVICE_BREAKDOWN 0x04
#define KY5_BLOCKINGS_1_INVERTING_ELEMENT_IS_NOT_READY 0x08
#define KY5_BLOCKINGS_1_PIPE_OVERHEATING 0x10
#define KY5_BLOCKINGS_1_DOOR_IS_OPEN 0x20
#define KY5_BLOCKINGS_1_POWER_ELEMENTS_OVERHEATING 0x40

#define KY5_BLOCKINGS_2_MAS_EXCESS 0x01
#define KY5_BLOCKINGS_2_NO_INCANDESCENCE 0x02
#define KY5_BLOCKINGS_2_ANODE_VOLTAGE_EXCESS 0x04
#define KY5_BLOCKINGS_2_ANODE_CURRENT_EXCESS 0x08
#define KY5_BLOCKINGS_2_INCANDESCENCE_CURRENT_EXCESS_DURING_WAITING 0x10
#define KY5_BLOCKINGS_2_INCANDESCENCE_CURRENT_EXCESS_DURING_SHOOTING 0x2
#define KY5_BLOCKINGS_2_NO_RASTER_MOVEMENT_MORE_THAN_TWO_SECONDS 0x40

#define KY5_BLOCKINGS_3_HV_CIRCUIT_CURRENT_EXCESS 0x01
#define KY5_BLOCKINGS_3_ANODE_CURRENT_ASYMMETRY_MORE_THAN_10_PERCENT 0x02
#define KY5_BLOCKINGS_3_MEASURED_ANODE_VOLTAGE_DEFLECTION_MORE_THAN_10_PERCENT 0x04
#define KY5_BLOCKINGS_3_MEASURED_ANODE_CURRENT_DEFLECTION_MORE_THAN_10_PERCENT 0x08
#define KY5_BLOCKINGS_3_POWER_GRID_OVERVOLTAGE 0x10
#define KY5_BLOCKINGS_3_POWER_GRID_UNDERVOLTAGE 0x20
#define KY5_BLOCKINGS_3_POWER_VOLTAGE_DEFLECTION 0x40

#define KY5_MESSAGES_READY 0x01
#define KY5_MESSAGES_START_SNAPSHOT_TRAILER 0x02

#define KY5_WARNINGS_HV_OFF 0x00
#define KY5_WARNINGS_PREPARING_START 0x01
#define KY5_WARNINGS_SNAPSHOT_START 0x03
#define KY5_WARNINGS_MULTIPLE_MODE_SNAPSHOT_START 0x05
#define KY5_WARNINGS_SNAPSHOT_FINISH 0x04

#define KY5_READ_DATA_SIZE 116

//------coefficients------
#define KY5_ANODE_HV_COEFFICIENT 7//1
#define KY5_MEASURED_ANODE_CURRENT_HIDDEN_COEFFICIENT 3.3f// for all modes
#define KY5_MEASURED_ANODE_CURRENT_COEFFICIENT_FOR_LONG_EXPOSITION_TIME_MODE 4.7f
#define KY5_MAS_COEFFICIENT 1//10//5//1
#define KY5_EXPOSITION_TIME_COEFFICIENT 14.29f// for "fast" mode

#endif
