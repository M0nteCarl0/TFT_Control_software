#pragma once
#include <stdio.h>
class CalibrationConfig
{
public:
    CalibrationConfig(void);
    ~CalibrationConfig(void);
    bool Open(void);
	void Close(void);
	void InitDefault(void);
    void ReadConfig(void);
    void WriteConfig(void);

    float GetCmAsBegin(void);
    float GetCmAsEnd(void);
    int GetCmAsSteps(void);
    int GetCHV(void);
    int GetCPrecurent(void);
    int GetCCurent(void);
    int GetCTimeLimit(void);
    int GetCTypeFocuse(void);
	int GetCStatic(void);
	int GetCDellayBeatweanSnapshots(void);

    void SetCmAsBegin(float* val);
    void SetCmAsEnd(float* val);
    void SetCmAsSteps(int* val);
    void SetCHV(int* val);
    void SetCPrecurent(int* val);
    void SetCCurent(int* val);
    void SetCTimeLimit(int* val);
    void SetCTypeFocuse(int* val);
	void SetCStatic(int* val);
	void SetCDellayBeatweanSnapshots(int* val);
private:
    float CmAsBegin;
    float CmAsEnd;
	int CDellayBeatweanSnapshots;
    int CmAsSteps;
    int CHV;
    int CPrecurent;
    int CCurent;
    int CTimeLimit;
    int CTypeFocuse;
	int CStatic;
    FILE* _Config;
};

