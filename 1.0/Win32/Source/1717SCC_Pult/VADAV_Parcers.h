#pragma once

#include <string>
#include <fstream>
using namespace std;
/*************************************************************************************************
*                 ������ ��� ������ � ������������� FLAT Rayence
*                 ���������� �.�(molotatliev@xprom.ru)
*                 Initial version:15.10.15
*
*
*
*
***************************************************************************************************/
class VADAV_Parcers
{
public:
	VADAV_Parcers(void);
	~VADAV_Parcers(void);
	void	Open();
	bool	Get_AcqOfsCal(void);
	bool	Get_AcqGainCal(void);
	bool	Get_AcqBadPixMap(void);
	bool	Get_AddWindowTime(void);
	bool	Get_SaveTempAcqFrame(void);
	bool	Get_SaveTempCalFrames(void);
	bool	Get_SaveTempImgProcess(void);
	bool	Get_Scramble(void);
	int		Get_NumParallelThreads(void);
	int		Get_ADCNumDescramble(void);
	int		Get_RelaxationTime(void);
	int		Get_FrameWidth(void);
	int		Get_FrameHeight(void);
	string  Get_AcqImageName(void);
	void    SetDakHWState(const  bool  State);
	void    ApplySetings(void);

private:
	bool	_AcqOfsCal;
	bool	_AcqGainCal;
	bool	_AcqBadPixMap;
	bool	_AddWindowTime;
	bool	_SaveTempAcqFrame;
	bool	_SaveTempCalFrames;
	bool	_SaveTempImgProcess;
	bool	_Scramble;
	bool    _Bit16;
	int		_NumParallelThreads;
	int		_ADCNumDescramble;
	int		_RelaxationTime;
	int		_FrameWidth;
	int		_FrameHeight;
	string  _AcqImageName;
	string  _TokenBuff;
	fstream _VADAV;
}