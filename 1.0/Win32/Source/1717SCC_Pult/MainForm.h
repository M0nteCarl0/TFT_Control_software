#pragma once

namespace KY5_Control {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::TabControl^  tabControl1;
    protected: 
    private: System::Windows::Forms::TabPage^  tabPage1;
    private: System::Windows::Forms::GroupBox^  groupBox2;
    private: System::Windows::Forms::Label^  label14;
    private: System::Windows::Forms::Label^  label13;
    private: System::Windows::Forms::Label^  label12;
    private: System::Windows::Forms::Label^  label11;
    private: System::Windows::Forms::GroupBox^  groupBox1;
    private: System::Windows::Forms::Label^  label5;
    private: System::Windows::Forms::Label^  label4;
    private: System::Windows::Forms::Label^  label3;
    private: System::Windows::Forms::Label^  label2;
    private: System::Windows::Forms::Label^  label1;
    private: System::Windows::Forms::Button^  DarkCurrentFrame;

    private: System::Windows::Forms::Button^  BrightFrame;

    private: System::Windows::Forms::TabPage^  tabPage2;
    private: System::Windows::Forms::GroupBox^  groupBox4;




    private: System::Windows::Forms::GroupBox^  groupBox3;
    private: System::Windows::Forms::Label^  label6;
    private: System::Windows::Forms::Label^  label7;
    private: System::Windows::Forms::Label^  label8;
    private: System::Windows::Forms::Label^  label9;
    private: System::Windows::Forms::Label^  label10;
    private: System::Windows::Forms::Button^  button4;

    private: System::Windows::Forms::Button^  button1;
    private: System::Windows::Forms::Button^  X_RayTest;
    private: System::Windows::Forms::Label^  ReadedCurrent;
    private: System::Windows::Forms::Label^  ReadedTime;
    private: System::Windows::Forms::Label^  ReadedmAs;
    private: System::Windows::Forms::Label^  ReadedHV;
    private: System::Windows::Forms::MenuStrip^  menuStrip1;
    private: System::Windows::Forms::ToolStripMenuItem^  fTDIToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ������������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �����������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  kY5ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �����ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �������������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ��������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ����������������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �����������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �����������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  �������2000ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ������FLATPannelToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  davincyToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem1;
    private: System::Windows::Forms::ToolStripMenuItem^  �������������������ToolStripMenuItem;
    private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem2;
    private: System::Windows::Forms::Label^  label15;
    private: System::Windows::Forms::Label^  label16;
    private: System::Windows::Forms::Label^  label17;
    private: System::Windows::Forms::Label^  label18;
    private: System::Windows::Forms::Label^  label19;
    private: System::Windows::Forms::Label^  label20;
    private: System::Windows::Forms::Label^  label21;
    private: System::Windows::Forms::Label^  label22;
    private: System::Windows::Forms::StatusStrip^  statusStrip1;
    private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;
    private: System::Windows::Forms::ToolStripProgressBar^  toolStripProgressBar1;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->X_RayTest = (gcnew System::Windows::Forms::Button());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->ReadedCurrent = (gcnew System::Windows::Forms::Label());
			this->ReadedTime = (gcnew System::Windows::Forms::Label());
			this->ReadedmAs = (gcnew System::Windows::Forms::Label());
			this->ReadedHV = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->DarkCurrentFrame = (gcnew System::Windows::Forms::Button());
			this->BrightFrame = (gcnew System::Windows::Forms::Button());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->groupBox4 = (gcnew System::Windows::Forms::GroupBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->label21 = (gcnew System::Windows::Forms::Label());
			this->label22 = (gcnew System::Windows::Forms::Label());
			this->groupBox3 = (gcnew System::Windows::Forms::GroupBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fTDIToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->kY5ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������2000ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������FLATPannelToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->davincyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->toolStripProgressBar1 = (gcnew System::Windows::Forms::ToolStripProgressBar());
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->groupBox4->SuspendLayout();
			this->groupBox3->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Location = System::Drawing::Point(2, 36);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(398, 231);
			this->tabControl1->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->X_RayTest);
			this->tabPage1->Controls->Add(this->groupBox2);
			this->tabPage1->Controls->Add(this->groupBox1);
			this->tabPage1->Controls->Add(this->DarkCurrentFrame);
			this->tabPage1->Controls->Add(this->BrightFrame);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(390, 205);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"������";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// X_RayTest
			// 
			this->X_RayTest->Location = System::Drawing::Point(171, 165);
			this->X_RayTest->Name = L"X_RayTest";
			this->X_RayTest->Size = System::Drawing::Size(72, 23);
			this->X_RayTest->TabIndex = 4;
			this->X_RayTest->Text = L"X-Ray test";
			this->X_RayTest->UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->ReadedCurrent);
			this->groupBox2->Controls->Add(this->ReadedTime);
			this->groupBox2->Controls->Add(this->ReadedmAs);
			this->groupBox2->Controls->Add(this->ReadedHV);
			this->groupBox2->Controls->Add(this->label14);
			this->groupBox2->Controls->Add(this->label13);
			this->groupBox2->Controls->Add(this->label12);
			this->groupBox2->Controls->Add(this->label11);
			this->groupBox2->Location = System::Drawing::Point(180, 20);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(148, 139);
			this->groupBox2->TabIndex = 3;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"���������";
			// 
			// ReadedCurrent
			// 
			this->ReadedCurrent->AutoSize = true;
			this->ReadedCurrent->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedCurrent->Location = System::Drawing::Point(93, 92);
			this->ReadedCurrent->Name = L"ReadedCurrent";
			this->ReadedCurrent->Size = System::Drawing::Size(43, 15);
			this->ReadedCurrent->TabIndex = 7;
			this->ReadedCurrent->Text = L"label19";
			// 
			// ReadedTime
			// 
			this->ReadedTime->AutoSize = true;
			this->ReadedTime->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedTime->Location = System::Drawing::Point(93, 73);
			this->ReadedTime->Name = L"ReadedTime";
			this->ReadedTime->Size = System::Drawing::Size(43, 15);
			this->ReadedTime->TabIndex = 6;
			this->ReadedTime->Text = L"label19";
			// 
			// ReadedmAs
			// 
			this->ReadedmAs->AutoSize = true;
			this->ReadedmAs->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedmAs->Location = System::Drawing::Point(93, 51);
			this->ReadedmAs->Name = L"ReadedmAs";
			this->ReadedmAs->Size = System::Drawing::Size(43, 15);
			this->ReadedmAs->TabIndex = 5;
			this->ReadedmAs->Text = L"label19";
			// 
			// ReadedHV
			// 
			this->ReadedHV->AutoSize = true;
			this->ReadedHV->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->ReadedHV->Location = System::Drawing::Point(93, 28);
			this->ReadedHV->Name = L"ReadedHV";
			this->ReadedHV->Size = System::Drawing::Size(43, 15);
			this->ReadedHV->TabIndex = 4;
			this->ReadedHV->Text = L"label19";
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(15, 92);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(26, 13);
			this->label14->TabIndex = 3;
			this->label14->Text = L"���";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(15, 28);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(71, 13);
			this->label13->TabIndex = 2;
			this->label13->Text = L"����������";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(15, 51);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(28, 13);
			this->label12->TabIndex = 1;
			this->label12->Text = L"���";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(15, 73);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(40, 13);
			this->label11->TabIndex = 0;
			this->label11->Text = L"�����";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(7, 20);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(158, 139);
			this->groupBox1->TabIndex = 2;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"��������� ";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(6, 115);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(40, 13);
			this->label5->TabIndex = 4;
			this->label5->Text = L"�����";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 92);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(28, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"���";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 73);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(68, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"���  ������";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(6, 51);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(89, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"��� ����������";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 28);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(71, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"����������";
			// 
			// DarkCurrentFrame
			// 
			this->DarkCurrentFrame->Location = System::Drawing::Point(90, 165);
			this->DarkCurrentFrame->Name = L"DarkCurrentFrame";
			this->DarkCurrentFrame->Size = System::Drawing::Size(75, 23);
			this->DarkCurrentFrame->TabIndex = 1;
			this->DarkCurrentFrame->Text = L"DC";
			this->DarkCurrentFrame->UseVisualStyleBackColor = true;
			// 
			// BrightFrame
			// 
			this->BrightFrame->Location = System::Drawing::Point(9, 165);
			this->BrightFrame->Name = L"BrightFrame";
			this->BrightFrame->Size = System::Drawing::Size(75, 23);
			this->BrightFrame->TabIndex = 0;
			this->BrightFrame->Text = L"������";
			this->BrightFrame->UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->groupBox4);
			this->tabPage2->Controls->Add(this->groupBox3);
			this->tabPage2->Controls->Add(this->button4);
			this->tabPage2->Controls->Add(this->button1);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(390, 205);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"����������";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this->groupBox4->Controls->Add(this->label15);
			this->groupBox4->Controls->Add(this->label16);
			this->groupBox4->Controls->Add(this->label17);
			this->groupBox4->Controls->Add(this->label18);
			this->groupBox4->Controls->Add(this->label19);
			this->groupBox4->Controls->Add(this->label20);
			this->groupBox4->Controls->Add(this->label21);
			this->groupBox4->Controls->Add(this->label22);
			this->groupBox4->Location = System::Drawing::Point(181, 15);
			this->groupBox4->Name = L"groupBox4";
			this->groupBox4->Size = System::Drawing::Size(149, 141);
			this->groupBox4->TabIndex = 4;
			this->groupBox4->TabStop = false;
			this->groupBox4->Text = L"���������";
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label15->Location = System::Drawing::Point(94, 92);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(43, 15);
			this->label15->TabIndex = 15;
			this->label15->Text = L"label19";
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label16->Location = System::Drawing::Point(94, 73);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(43, 15);
			this->label16->TabIndex = 14;
			this->label16->Text = L"label19";
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label17->Location = System::Drawing::Point(94, 51);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(43, 15);
			this->label17->TabIndex = 13;
			this->label17->Text = L"label19";
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
			this->label18->Location = System::Drawing::Point(94, 28);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(43, 15);
			this->label18->TabIndex = 12;
			this->label18->Text = L"label19";
			// 
			// label19
			// 
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(16, 92);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(26, 13);
			this->label19->TabIndex = 11;
			this->label19->Text = L"���";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(16, 28);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(71, 13);
			this->label20->TabIndex = 10;
			this->label20->Text = L"����������";
			// 
			// label21
			// 
			this->label21->AutoSize = true;
			this->label21->Location = System::Drawing::Point(16, 51);
			this->label21->Name = L"label21";
			this->label21->Size = System::Drawing::Size(28, 13);
			this->label21->TabIndex = 9;
			this->label21->Text = L"���";
			// 
			// label22
			// 
			this->label22->AutoSize = true;
			this->label22->Location = System::Drawing::Point(16, 73);
			this->label22->Name = L"label22";
			this->label22->Size = System::Drawing::Size(40, 13);
			this->label22->TabIndex = 8;
			this->label22->Text = L"�����";
			// 
			// groupBox3
			// 
			this->groupBox3->Controls->Add(this->label6);
			this->groupBox3->Controls->Add(this->label7);
			this->groupBox3->Controls->Add(this->label8);
			this->groupBox3->Controls->Add(this->label9);
			this->groupBox3->Controls->Add(this->label10);
			this->groupBox3->Location = System::Drawing::Point(6, 15);
			this->groupBox3->Name = L"groupBox3";
			this->groupBox3->Size = System::Drawing::Size(160, 141);
			this->groupBox3->TabIndex = 3;
			this->groupBox3->TabStop = false;
			this->groupBox3->Text = L"��������� ";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(6, 115);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(40, 13);
			this->label6->TabIndex = 4;
			this->label6->Text = L"�����";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(6, 92);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(28, 13);
			this->label7->TabIndex = 3;
			this->label7->Text = L"���";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(6, 73);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(68, 13);
			this->label8->TabIndex = 2;
			this->label8->Text = L"���  ������";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(6, 51);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(89, 13);
			this->label9->TabIndex = 1;
			this->label9->Text = L"��� ����������";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(6, 28);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(71, 13);
			this->label10->TabIndex = 0;
			this->label10->Text = L"����������";
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(91, 162);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(75, 23);
			this->button4->TabIndex = 2;
			this->button4->Text = L"������";
			this->button4->UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(8, 162);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"�����";
			this->button1->UseVisualStyleBackColor = true;
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->fTDIToolStripMenuItem, 
				this->kY5ToolStripMenuItem, this->davincyToolStripMenuItem, this->�������������������ToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(402, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fTDIToolStripMenuItem
			// 
			this->fTDIToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->���������ToolStripMenuItem, 
				this->������������ToolStripMenuItem, this->�����������ToolStripMenuItem});
			this->fTDIToolStripMenuItem->Name = L"fTDIToolStripMenuItem";
			this->fTDIToolStripMenuItem->Size = System::Drawing::Size(43, 20);
			this->fTDIToolStripMenuItem->Text = L"FTDI";
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->���������ToolStripMenuItem->Text = L"���������";
			// 
			// ������������ToolStripMenuItem
			// 
			this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
			this->������������ToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->������������ToolStripMenuItem->Text = L"������������";
			// 
			// �����������ToolStripMenuItem
			// 
			this->�����������ToolStripMenuItem->Name = L"�����������ToolStripMenuItem";
			this->�����������ToolStripMenuItem->Size = System::Drawing::Size(156, 22);
			this->�����������ToolStripMenuItem->Text = L"�����������";
			// 
			// kY5ToolStripMenuItem
			// 
			this->kY5ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->���������ToolStripMenuItem, 
				this->�������������ToolStripMenuItem, this->�����������ToolStripMenuItem});
			this->kY5ToolStripMenuItem->Name = L"kY5ToolStripMenuItem";
			this->kY5ToolStripMenuItem->Size = System::Drawing::Size(39, 20);
			this->kY5ToolStripMenuItem->Text = L"KY5";
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->�������ToolStripMenuItem, 
				this->�����ToolStripMenuItem});
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->���������ToolStripMenuItem->Text = L"��� ������";
			// 
			// �������ToolStripMenuItem
			// 
			this->�������ToolStripMenuItem->Name = L"�������ToolStripMenuItem";
			this->�������ToolStripMenuItem->Size = System::Drawing::Size(126, 22);
			this->�������ToolStripMenuItem->Text = L"�������";
			// 
			// �����ToolStripMenuItem
			// 
			this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
			this->�����ToolStripMenuItem->Size = System::Drawing::Size(126, 22);
			this->�����ToolStripMenuItem->Text = L"�����";
			// 
			// �������������ToolStripMenuItem
			// 
			this->�������������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->������ToolStripMenuItem, 
				this->��������ToolStripMenuItem, this->����������������ToolStripMenuItem});
			this->�������������ToolStripMenuItem->Name = L"�������������ToolStripMenuItem";
			this->�������������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->�������������ToolStripMenuItem->Text = L"��� ����������";
			// 
			// ������ToolStripMenuItem
			// 
			this->������ToolStripMenuItem->Name = L"������ToolStripMenuItem";
			this->������ToolStripMenuItem->Size = System::Drawing::Size(176, 22);
			this->������ToolStripMenuItem->Text = L"������";
			// 
			// ��������ToolStripMenuItem
			// 
			this->��������ToolStripMenuItem->Name = L"��������ToolStripMenuItem";
			this->��������ToolStripMenuItem->Size = System::Drawing::Size(176, 22);
			this->��������ToolStripMenuItem->Text = L"��������";
			// 
			// ����������������ToolStripMenuItem
			// 
			this->����������������ToolStripMenuItem->Name = L"����������������ToolStripMenuItem";
			this->����������������ToolStripMenuItem->Size = System::Drawing::Size(176, 22);
			this->����������������ToolStripMenuItem->Text = L"����������������";
			// 
			// �����������ToolStripMenuItem
			// 
			this->�����������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->�����������ToolStripMenuItem, 
				this->�������2000ToolStripMenuItem, this->������FLATPannelToolStripMenuItem});
			this->�����������ToolStripMenuItem->Name = L"�����������ToolStripMenuItem";
			this->�����������ToolStripMenuItem->Size = System::Drawing::Size(163, 22);
			this->�����������ToolStripMenuItem->Text = L"��� ��������";
			// 
			// �����������ToolStripMenuItem
			// 
			this->�����������ToolStripMenuItem->Name = L"�����������ToolStripMenuItem";
			this->�����������ToolStripMenuItem->Size = System::Drawing::Size(193, 22);
			this->�����������ToolStripMenuItem->Text = L"�� ���������";
			// 
			// �������2000ToolStripMenuItem
			// 
			this->�������2000ToolStripMenuItem->Name = L"�������2000ToolStripMenuItem";
			this->�������2000ToolStripMenuItem->Size = System::Drawing::Size(193, 22);
			this->�������2000ToolStripMenuItem->Text = L"������� 2000";
			// 
			// ������FLATPannelToolStripMenuItem
			// 
			this->������FLATPannelToolStripMenuItem->Name = L"������FLATPannelToolStripMenuItem";
			this->������FLATPannelToolStripMenuItem->Size = System::Drawing::Size(193, 22);
			this->������FLATPannelToolStripMenuItem->Text = L"��� ���/FLAT Pannel";
			// 
			// davincyToolStripMenuItem
			// 
			this->davincyToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->���������ToolStripMenuItem1});
			this->davincyToolStripMenuItem->Name = L"davincyToolStripMenuItem";
			this->davincyToolStripMenuItem->Size = System::Drawing::Size(61, 20);
			this->davincyToolStripMenuItem->Text = L"Davincy";
			// 
			// ���������ToolStripMenuItem1
			// 
			this->���������ToolStripMenuItem1->Name = L"���������ToolStripMenuItem1";
			this->���������ToolStripMenuItem1->Size = System::Drawing::Size(138, 22);
			this->���������ToolStripMenuItem1->Text = L"���������";
			// 
			// �������������������ToolStripMenuItem
			// 
			this->�������������������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->���������ToolStripMenuItem2});
			this->�������������������ToolStripMenuItem->Name = L"�������������������ToolStripMenuItem";
			this->�������������������ToolStripMenuItem->Size = System::Drawing::Size(85, 20);
			this->�������������������ToolStripMenuItem->Text = L"����������";
			// 
			// ���������ToolStripMenuItem2
			// 
			this->���������ToolStripMenuItem2->Name = L"���������ToolStripMenuItem2";
			this->���������ToolStripMenuItem2->Size = System::Drawing::Size(138, 22);
			this->���������ToolStripMenuItem2->Text = L"���������";
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->toolStripStatusLabel1, 
				this->toolStripProgressBar1});
			this->statusStrip1->Location = System::Drawing::Point(0, 266);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(402, 22);
			this->statusStrip1->TabIndex = 2;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
			this->toolStripStatusLabel1->Size = System::Drawing::Size(118, 17);
			this->toolStripStatusLabel1->Text = L"toolStripStatusLabel1";
			// 
			// toolStripProgressBar1
			// 
			this->toolStripProgressBar1->Name = L"toolStripProgressBar1";
			this->toolStripProgressBar1->Size = System::Drawing::Size(100, 16);
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(402, 288);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->tabControl1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MainForm";
			this->Text = L"1717SCC Control pult";
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->groupBox4->ResumeLayout(false);
			this->groupBox4->PerformLayout();
			this->groupBox3->ResumeLayout(false);
			this->groupBox3->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}

