#pragma once
#include <tchar.h>?
#include <afxwin.h>
using namespace System::Runtime::InteropServices;
enum  DavinchyTypeFrame
{
	DavinchyTypeFrame_Bright       = 1,
	DavinchyTypeFrame_Dark         = 2,
	DavinchyTypeFrame_RecentFrame  = 3,
};

struct DavinchyWraper_CallbackAqusition
{

	int     rFlags,            // combination of cVDACQ_Fxxxx
			rType,             // cVDACQ_ETxxx
			rEvent,            // cVDACQ_Exxx
			rSocket;           // 0:no socket relation; otherwise socket's ID >0 
	TCHAR   rMsg[256];         // message (trace, wrn, err)
	int     rFrameWidth,       // full frame width
			rFrameHeight;      // full frame height
	short * rFrameBuffer;      // user supplied frame buffer "AFrameBuffer"
	int     rCaptureRows,      // # of received rows
			rCapturePercent;   // received data in percents
	void	*rUserCallBackProc, // user supplied "ACallBackProc"
			*rUserParam;        // user supplied "AUserParam"
	int    rAborted;          // 1: VDACQ_Abort -1:internally
	void  *rPacketData; 

};

struct tVDC_CallBackRec
{
	int  rNumReceivedRows, // acquisition uses this field to print messages
		rComplete;        // indicates that caller terminates (by some reason) and expects "close"
						  // the possible reasons are: 'complete', 'error' or 'abort'
	int rCalMode;
	CWnd *rhWnd;

};

public delegate  void tVDC_CallBackProc (DavinchyWraper_CallbackAqusition*);

ref class DavinchyWraper
{

	[DLLimport("VADAV.dll")];

public:


	bool InitInsatnce(void);

	void SetPath(const char* string)
	{
		if (string!= NULL)
		{

			strncpy(PathToLib, string, 1);
			SetDllDirectory(PathToLib);
		}
	}


	void PrepareToCapture(int Flags);
	void StartFrame(void);
	void CancelFrame(void);
	void SHowDavinchyDialog(void);
	void RebbotDetector(void);
	void ShutdowndDetector(void);
	char* GetFirmwareVersion(void);
	char* GetSerialnumberDetector(void);
	char* GetNodellDetector(void);
	bool  AqusitionComlete(void);
	short* GetFrameData(void);

private:

	char* PathToLib;
	short* FrameBuffer;
	short FrameW;
	short FrameH;
	int LastError;

};

