#pragma once
#include <Windows.h>
class CriticalSection
{
public:
	CriticalSection(void);
	~CriticalSection(void);
	void Enter(void);
	void  Leave(void);
private:
	CRITICAL_SECTION _Sec;
};

