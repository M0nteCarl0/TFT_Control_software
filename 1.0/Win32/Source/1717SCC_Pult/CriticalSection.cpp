#include "CriticalSection.h"


CriticalSection::CriticalSection(void)
{
	InitializeCriticalSection(&_Sec);
}


CriticalSection:: ~CriticalSection(void)
{

	DeleteCriticalSection(&_Sec);

}

void CriticalSection::Enter(void)
{

	EnterCriticalSection(&_Sec);

}

void CriticalSection:: Leave(void)
{
	LeaveCriticalSection(&_Sec);
}

