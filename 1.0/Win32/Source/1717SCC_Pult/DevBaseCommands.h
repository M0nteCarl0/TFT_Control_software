//----------------------------------------------------------------------------
//    Create by Novikov
//    data 23.11.2011
//----------------------------------------------------------------------------
//    Modified: Kalendarev V
//    date 06.12.2012
//----------------------------------------------------------------------------
//    Modified: Kalendarev V
//    date 19.08.2013
//----------------------------------------------------------------------------
//#include "stdafx.h"

#ifndef __DEV_BASE_COMMANDS_H
#define __DEV_BASE_COMMANDS_H

#include "MxComm.h"


class DevBaseCommands
{
public:
	DevBaseCommands();
	DevBaseCommands::~DevBaseCommands();

protected:
	MxComm * m_Comm;
	bool m_FlgPrint;

protected:
	static CRITICAL_SECTION CrSection;
	
protected:
	volatile LONG WaitingIsProhibitedFlag;

public:
	void SetComm(MxComm * const Addr);

	bool IsCommAvailable() const { return m_Comm != nullptr; }

	FTDI_ActionInfo WriteReg(const BYTE * const Reg) const;
    FTDI_ActionInfo WriteReg(const BYTE * const AFewBytesToWrite,const int NumberOfBytesToWrite) const;
	FTDI_ActionInfo WriteReg(const BYTE  Reg,const BYTE Data) const;

	FTDI_ActionInfo ReadReg(const BYTE  Reg, BYTE * const RegR) const;
	FTDI_ActionInfo ReadRegDat(const BYTE Reg, BYTE * const Data) const;

	FTDI_ActionInfo SetRegBit(const BYTE Reg,const BYTE BitToSet,const bool BitValueToSet) const;

	FTDI_ActionInfo Send(const BYTE * const WriteBuffer = nullptr, const int NumberOfBytesThatAreGoingToBeSent = 0) const;
	FTDI_ActionInfo Receive(BYTE * const ReadBuffer = nullptr, const int NumberOfBytesThatAreGoingToBeReceived = 0) const;

	FTDI_ActionInfo SendCommandAndReceiveItAsAProof(const BYTE Address,const BYTE CommandToSend) const;
	FTDI_ActionInfo SendCommandAndReceiveItAsAProof(const BYTE * const CommandData = nullptr,const int CommandDataLength = 0) const;

	bool SetReg(const BYTE * const Reg) const;
	bool SetReg(const BYTE Reg,const BYTE Data) const;

	bool SetValue37(const int Value,const BYTE Reg_LO,const BYTE Reg_HO) const;
      
	FTDI_ActionInfo WriteValue37(const int Value,const BYTE Reg_LO,const BYTE Reg_HO) const;
	FTDI_ActionInfo WriteValue77(const int Value,const BYTE Reg_LO,const BYTE Reg_HO) const;

	FTDI_ActionInfo ReadValue37(int * const Value,const BYTE Reg_LO,const BYTE Reg_HO) const;
	FTDI_ActionInfo ReadValue77(int * const Value,const BYTE Reg_LO,const BYTE Reg_HO) const;

	bool WaitBits(BYTE Reg, int WaitTime, BYTE Mask, int * const TimeOut = nullptr,const int DelayBetweenRequests = 0) const;
	bool WaitState(BYTE Reg, int WaitTime, BYTE NeededState, int * const TimeOut = nullptr,const int DelayBetweenRequests = 0) const;
	bool WaitBitState(BYTE Reg, int WaitTime, BYTE Mask, bool NeededState, int * const TimeOut = nullptr,const int DelayBetweenRequests = 0) const;

	LONG PermitWaiting();
	LONG ProhibitWaiting();

	static void Delay(const float DelayInMilliseconds);
};

#endif