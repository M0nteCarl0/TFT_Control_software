//
//  autor: Vitaly Kalendarev, kalendarev@roentgenprom.ru
//
//  date: 21.10.2014
//
//#include "stdafx.h"

#ifndef I_KY5_COMMANDS_H
#define I_KY5_COMMANDS_H

#include "DevBaseCommands.h"
#include "KY5_Registers.h"

#include <time.h>

	// interface IKY5_Commands
class IKY5_Commands : public DevBaseCommands
{
public:
	IKY5_Commands(MxComm * const Comm = nullptr,const int MultipleModeTimeoutToSet = KY5_SNAPSHOT_MULTIPLE_MODE_TIMEOUT_BY_DEFAULT,const bool PrintFlag = false);

public:
	FTDI_ActionInfo SetDeviceType(const BYTE DeviceTypeToSet) const;

	FTDI_ActionInfo FocusIsSmall() const;
	FTDI_ActionInfo FocusIsBig() const;

	FTDI_ActionInfo ExpositionTimeIsShort();
	FTDI_ActionInfo ExpositionTimeIsLong();

	FTDI_ActionInfo SetAnodeHV(const int AnodeHVToSet) const;
	FTDI_ActionInfo SetAnodeCurrent(const int AnodeCurrentToSet) const;
	FTDI_ActionInfo SetAnodeSmallFocusPreCurrent(const int AnodeSmallFocusPreCurrentToSet) const;
	FTDI_ActionInfo SetAnodeSmallFocusCurrent(const int AnodeSmallFocusCurrentToSet) const;
	FTDI_ActionInfo SetAnodeBigFocusPreCurrent(const int AnodeBigFocusPreCurrentToSet) const;
	FTDI_ActionInfo SetAnodeBigFocusCurrent(const int AnodeBigFocusCurrentToSet) const;
	FTDI_ActionInfo SetMAS(const float MASToSet) const;
	FTDI_ActionInfo SetExpositionTime(const int ExpositionTimeToSet) const;

	FTDI_ActionInfo SetMaxAnodeCurrent(const BYTE MaxAnodeCurrentToSet) const;
	FTDI_ActionInfo SetMaxAnodeHV(const BYTE MaxAnodeHVToSet) const;
	FTDI_ActionInfo SetMaxAnodeSmallFocusPreCurrent(const BYTE MaxAnodeSmallFocusPreCurrentToSet) const;
	FTDI_ActionInfo SetMaxAnodeSmallFocusCurrent(const BYTE MaxAnodeSmallFocusCurrentToSet) const;
	FTDI_ActionInfo SetMaxAnodeBigFocusPreCurrent(const BYTE MaxAnodeBigFocusPreCurrentToSet) const;
	FTDI_ActionInfo SetMaxAnodeBigFocusCurrent(const BYTE MaxAnodeBigFocusCurrentToSet) const;

	FTDI_ActionInfo Snapshot_Preparing() const;
	FTDI_ActionInfo Snapshot_HV_On() const;
	FTDI_ActionInfo Snapshot_HV_On_InMultipleMode() const;
	FTDI_ActionInfo Snapshot_HV_Off() const;

public:
	virtual FTDI_ActionInfo ReadRegisters();

public:
	virtual bool WaitNeededSnapshotState(const BYTE NeededSnapshotState,const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime = nullptr);

	bool WaitForPreparingStart(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime = nullptr);
	bool WaitForSnapshotStart(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime = nullptr);
	bool WaitForMultipleModeSnapshotStart(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime = nullptr);
	bool WaitForSnapshotFinish(const int MaxWaitingTime,const int DelayBetweenRequests,int * const WaitingTime = nullptr);

	clock_t GetSingleModeStartTime() const {  return SingleModeStartTime; }
	clock_t GetMultipleModeStartTime() const { return MultipleModeStartTime; }
	clock_t GetSnapshotFinishTime() const { return SnapshotFinishTime; }

	void WaitForSnapshotFinishTimeout() const { while(clock() < SnapshotFinishTime + KY5_SNAPSHOT_FINISH_TIMEOUT_BY_DEFAULT); }
	void WaitForMinWhileBetweenSnapshots() const { while(clock() < SnapshotFinishTime + MinWhileBetweenSnapshots); }//WaitForMultipleModeTimeout() - old name of this function

public:
	BYTE GetDeviceType() const { return DeviceType; }

	bool IsSmallFocus() const { return FocusMode == KY5_FOCUS_IS_SMALL; }
	bool IsBigFocus() const { return FocusMode == KY5_FOCUS_IS_BIG; }

	bool IsExpositionTimeShort() const { return ExpositionTimeMode == KY5_EXPOSITION_TIME_IS_SHORT; }
	bool IsExpositionTimeLong() const { return ExpositionTimeMode == KY5_EXPOSITION_TIME_IS_LONG; }

	bool IsNoAnodeAcceleration() const { return (Blockings_1 & KY5_BLOCKINGS_1_NO_ANODE_ACCELERATION) == KY5_BLOCKINGS_1_NO_ANODE_ACCELERATION; }
	bool IsExpositionTimeExcess() const { return (Blockings_1 & KY5_BLOCKINGS_1_EXPOSITION_TIME_EXCESS) == KY5_BLOCKINGS_1_EXPOSITION_TIME_EXCESS; }
	bool IsDeviceBreakdown() const { return (Blockings_1 & KY5_BLOCKINGS_1_DEVICE_BREAKDOWN) == KY5_BLOCKINGS_1_DEVICE_BREAKDOWN; }
	bool IsInvertingElementNotReady() const { return (Blockings_1 & KY5_BLOCKINGS_1_INVERTING_ELEMENT_IS_NOT_READY) == KY5_BLOCKINGS_1_INVERTING_ELEMENT_IS_NOT_READY; }
	bool IsPipeOverheating() const { return (Blockings_1 & KY5_BLOCKINGS_1_PIPE_OVERHEATING) == KY5_BLOCKINGS_1_PIPE_OVERHEATING; }
	bool IsDoorOpen() const { return (Blockings_1 & KY5_BLOCKINGS_1_DOOR_IS_OPEN) == KY5_BLOCKINGS_1_DOOR_IS_OPEN; }
	bool IsPowerElementOverheating() const { return (Blockings_1 & KY5_BLOCKINGS_1_POWER_ELEMENTS_OVERHEATING) == KY5_BLOCKINGS_1_POWER_ELEMENTS_OVERHEATING; }

	bool IsMASExcess() const { return (Blockings_2 & KY5_BLOCKINGS_2_MAS_EXCESS) == KY5_BLOCKINGS_2_MAS_EXCESS; }
	bool IsNoIncandescence() const { return (Blockings_2 & KY5_BLOCKINGS_2_NO_INCANDESCENCE) == KY5_BLOCKINGS_2_NO_INCANDESCENCE; }
	bool IsAnodeHVExcess() const { return (Blockings_2 & KY5_BLOCKINGS_2_ANODE_VOLTAGE_EXCESS) == KY5_BLOCKINGS_2_ANODE_VOLTAGE_EXCESS; }
	bool IsAnodeCurrentExcess() const { return (Blockings_2 & KY5_BLOCKINGS_2_ANODE_CURRENT_EXCESS) == KY5_BLOCKINGS_2_ANODE_CURRENT_EXCESS; }
	bool IsIncandescenceCurrentExcessDuringWaiting() const { return (Blockings_2 & KY5_BLOCKINGS_2_INCANDESCENCE_CURRENT_EXCESS_DURING_WAITING) == KY5_BLOCKINGS_2_INCANDESCENCE_CURRENT_EXCESS_DURING_WAITING; }
	bool IsIncandescenceCurrentExcessDuringShooting() const { return (Blockings_2 & KY5_BLOCKINGS_2_INCANDESCENCE_CURRENT_EXCESS_DURING_SHOOTING) == KY5_BLOCKINGS_2_INCANDESCENCE_CURRENT_EXCESS_DURING_SHOOTING; }
	bool IsNoRasterMovementMoreThan_2_Seconds() const { return (Blockings_2 & KY5_BLOCKINGS_2_NO_RASTER_MOVEMENT_MORE_THAN_TWO_SECONDS) == KY5_BLOCKINGS_2_NO_RASTER_MOVEMENT_MORE_THAN_TWO_SECONDS; }

	bool IsHVCircuitCurrentExcess() const { return (Blockings_3 & KY5_BLOCKINGS_3_HV_CIRCUIT_CURRENT_EXCESS) == KY5_BLOCKINGS_3_HV_CIRCUIT_CURRENT_EXCESS; }
	bool IsAnodeCurrentAsymmetryMoreThan_10_Percents() const { return (Blockings_3 & KY5_BLOCKINGS_3_ANODE_CURRENT_ASYMMETRY_MORE_THAN_10_PERCENT) == KY5_BLOCKINGS_3_ANODE_CURRENT_ASYMMETRY_MORE_THAN_10_PERCENT; }
	bool IsMeasuredAnodeHVDeflectionMoreThan_10_Percent() const { return (Blockings_3 & KY5_BLOCKINGS_3_MEASURED_ANODE_VOLTAGE_DEFLECTION_MORE_THAN_10_PERCENT) == KY5_BLOCKINGS_3_MEASURED_ANODE_VOLTAGE_DEFLECTION_MORE_THAN_10_PERCENT; }
	bool IsMeasuredAnodeCurrentDeflectionMoreThan_10_Percents() const { return (Blockings_3 & KY5_BLOCKINGS_3_MEASURED_ANODE_CURRENT_DEFLECTION_MORE_THAN_10_PERCENT) == KY5_BLOCKINGS_3_MEASURED_ANODE_CURRENT_DEFLECTION_MORE_THAN_10_PERCENT; }
	bool IsPowerGridOvervoltage() const { return (Blockings_3 & KY5_BLOCKINGS_3_POWER_GRID_OVERVOLTAGE) == KY5_BLOCKINGS_3_POWER_GRID_OVERVOLTAGE; }
	bool IsPowerGridUndervoltage() const { return (Blockings_3 & KY5_BLOCKINGS_3_POWER_GRID_UNDERVOLTAGE) == KY5_BLOCKINGS_3_POWER_GRID_UNDERVOLTAGE; }
	bool IsPowerVoltageDeflection() const { return (Blockings_3 & KY5_BLOCKINGS_3_POWER_VOLTAGE_DEFLECTION) == KY5_BLOCKINGS_3_POWER_VOLTAGE_DEFLECTION; }

	bool IsDeviceReady() const { return (Messages & KY5_MESSAGES_READY) == KY5_MESSAGES_READY; }
	bool IsSnapshotTrailerStart() const { return (Messages & KY5_MESSAGES_START_SNAPSHOT_TRAILER) == KY5_MESSAGES_START_SNAPSHOT_TRAILER; }

	bool IsHVOff() const { return Warnings == KY5_WARNINGS_HV_OFF; }
	bool IsPreparingStart() const { return Warnings == KY5_WARNINGS_PREPARING_START; }
	bool IsSnapshotStart() const { return Warnings == KY5_WARNINGS_SNAPSHOT_START; }
	bool IsSnapshotStartInMultipleMode() const { return Warnings == KY5_WARNINGS_MULTIPLE_MODE_SNAPSHOT_START; }
	bool IsSnapshotFinish() const { return Warnings == KY5_WARNINGS_SNAPSHOT_FINISH; }

	int GetGivenAnodeHV() const { return GivenAnodeHV; }
	int GetGivenAnodeCurrent() const { return GivenAnodeCurrent; }
	int GetGivenAnodeSmallFocusPreCurrent() const { return GivenAnodeSmallFocusPreCurrent; }
	int GetGivenAnodeSmallFocusCurrent() const { return GivenAnodeSmallFocusCurrent; }
	int GetGivenAnodeBigFocusPreCurrent() const { return GivenAnodeBigFocusPreCurrent; }
	int GetGivenAnodeBigFocusCurrent() const { return GivenAnodeBigFocusCurrent; }
	float GetGivenMAS() const { return GivenMAS; }
	int GetGivenExpositionTime() const { return GivenExpositionTime; }

	BYTE GetGivenMaxAnodeCurrent() const { return GivenMaxAnodeCurrent; }
	BYTE GetGivenMaxAnodeHV() const { return GivenMaxAnodeHV; }
	BYTE GetGivenMaxAnodeSmallFocusPreCurrent() const { return GivenMaxAnodeSmallFocusPreCurrent; }
	BYTE GetGivenMaxAnodeSmallFocusCurrent() const { return GivenMaxAnodeSmallFocusCurrent; }
	BYTE GetGivenMaxAnodeBigFocusPreCurrent() const { return GivenMaxAnodeBigFocusPreCurrent; }
	BYTE GetGivenMaxAnodeBigFocusCurrent() const { return GivenMaxAnodeBigFocusCurrent; }

	int GetMeasuredAnodeHV() const { return MeasuredAnodeHV; }
	int GetMeasuredAnodePreCurrent() const { return MeasuredAnodePreCurrent; }
	int GetMeasuredPositiveAnodeCurrent() const { return MeasuredPositiveAnodeCurrent; }
	int GetMeasuredNegativeAnodeCurrent() const { return MeasuredNegativeAnodeCurrent; }
	int GetMeasuredAnodeCurrentDuringSnapshot() const { return MeasuredAnodeCurrentDuringSnapshot; }
	float GetMeasuredMAS() const { return MeasuredMAS; }
	int GetMeasuredExpositionTime() const { return MeasuredExpositionTime; }

protected:
	BYTE DeviceType;

	BYTE FocusMode;
	BYTE ExpositionTimeMode;
	  
	BYTE Blockings_1;
	BYTE Blockings_2;
	BYTE Blockings_3;
	BYTE Messages;
	BYTE Warnings;

	int GivenAnodeHV;
	int GivenAnodeCurrent;
	int GivenAnodeSmallFocusPreCurrent;
	int GivenAnodeSmallFocusCurrent;
	int GivenAnodeBigFocusPreCurrent;
	int GivenAnodeBigFocusCurrent;
	float GivenMAS;
	int GivenExpositionTime;

	BYTE GivenMaxAnodeCurrent;
	BYTE GivenMaxAnodeHV;
	BYTE GivenMaxAnodeSmallFocusPreCurrent;
	BYTE GivenMaxAnodeSmallFocusCurrent;
	BYTE GivenMaxAnodeBigFocusPreCurrent;
	BYTE GivenMaxAnodeBigFocusCurrent;

	int MeasuredAnodeHV;
	int MeasuredAnodePreCurrent;
	int MeasuredPositiveAnodeCurrent;
	int MeasuredNegativeAnodeCurrent;
	int MeasuredAnodeCurrentDuringSnapshot;
	float MeasuredMAS;
	int MeasuredExpositionTime;

protected:
	float ExpositionTimeCoefficient;
	float MeasuredAnodeCurrentCoefficient;
	int MASCoefficient;

private:
	clock_t SingleModeStartTime;
	clock_t MultipleModeStartTime;
	clock_t SnapshotFinishTime;

	int MinWhileBetweenSnapshots;// minimal interval between multiple snapshots
};

#endif