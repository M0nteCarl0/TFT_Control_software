#include "Rayance_Wrap.h"
#ifndef  _RAYANCEFX_
#define _RAYANCEFX_
enum VADAV_MessageCode
{
VADAV_MessageCode_ERR = 0x10001,
VADAV_MessageCode_RECEIVED_PERCENTS,
VADAV_MessageCode_RECEIVED_PERCENTS_COMPLETE,
VADAV_MessageCode_RECEIVED_ROWS,
VADAV_MessageCode_VERSIONS,
VADAV_MessageCode_TERMAL,
VADAV_MessageCode_AQCOUNT,
VADAV_MessageCode_TRACE,
VADAV_MessageCode_TriggerReady,
VADAV_MessageCode_DetectorStatus,
};

enum VADAV_Detector_VERSIONS
{

	VADAV_Detector_VERSIONS_VER_FIRM,
	VADAV_Detector_VERSIONS_VER_FPGA,
	VADAV_Detector_VERSIONS_VER_MAIN,
	VADAV_Detector_VERSIONS_VER_TFTP,
	VADAV_Detector_VERSIONS_VER_SCIN,
	VADAV_Detector_VERSIONS_VER_BOARD,

};


 RAYANCE_WRAP_API   bool VADAV_MapDLL(const char *ADllFName);
 RAYANCE_WRAP_API   bool VADAV_ReleaseDLL(void);
 RAYANCE_WRAP_API   bool VADAV_GetFrameDim(int* Width,int* Height);
 RAYANCE_WRAP_API   void VADAV_SetWindowsHandle(void* WindowHandle);
 RAYANCE_WRAP_API   bool VADAV_Connect(int typeFrame,int ConfigID);
 RAYANCE_WRAP_API   bool VADAV_StartFrame(void);
 RAYANCE_WRAP_API   bool VADAV_StartBrightFrame(void);
 RAYANCE_WRAP_API   bool VADAV_StartDarkFrame(void);
 RAYANCE_WRAP_API   bool VADAV_ShowDavinchyDialog(void);
 RAYANCE_WRAP_API   bool VADAV_CancelFrame(void);
 RAYANCE_WRAP_API   short* VADAV_GetFrameBufferData(size_t* BufferSize,size_t* FrameW,size_t* FrameH);
 RAYANCE_WRAP_API   char* VADAV_GetIP(void);
 RAYANCE_WRAP_API   bool VADAV_ConectForAUX(void);
 RAYANCE_WRAP_API   bool VADAV_Shutdown(void);
 RAYANCE_WRAP_API   bool VADAV_Reboot(void);
 RAYANCE_WRAP_API   bool VADAV_GetFirmwareVersions(void);
 RAYANCE_WRAP_API   bool VADAV_GetACQCount(void);
 RAYANCE_WRAP_API   bool VADAV_CloseACQ(void);
 RAYANCE_WRAP_API   bool VADAV_ConnectForBright(void);




#endif
