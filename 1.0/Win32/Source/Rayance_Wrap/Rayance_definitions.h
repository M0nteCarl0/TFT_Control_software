#include <afx.h>
#ifndef VADAVINTF_H_INCLUDED
#define VADAVINTF_H_INCLUDED





typedef struct DetectorInitParamets
{
  int FCtrl;
  int IntB;
  int Om;
  int Scan;
  int Speed;
  int EOSel;
  int Ag0;
  int Ag1;
  int Ag2;
  int DA0;
  int DA1;
  int DA2;
  int DA3;
  int DA4;
  int DA5;
  int DA6;
  int DA7;
  int DA8;
  int AGI_AUTO;
  int Gate0;
  int Gate1;
  int Gate2;
  int Gate3;
  int Gate4;
  int P_DRV;
  int S_DRV;
  int Dark_Comp;
  int AT_EN;
  int AF_GATE;
  int AF_EL;
  int Ex_Time;
  int Loop_Cnt;
  int Frame_Cnt;
  int Frame_Delay;
  int EL_Delay;
  int EL_High;
  int Gate_Low;
  int Gate_High;
  int ReadOut_Count;
  int EL_High2;
  int Frame_Cnt2;
  int Loop_Cnt2;
  int EL_High3;
  int Frame_Cnt3;
  int Loop_Cnt3;
  int Sh0;
  int Sh1;
  int Gate;
  int TestPattern;
  int AF_ELRunTime;
  int AF_ELDelay;
  int AF_ELLoop;
  int AF_GateRdCnt;
  int AF_DelayCnt;
  int AF_GateRdLoop;
  int AT_Threshold;
  int AT_Delay;
  int Ready_Delay;
  int AGI_Delay;
  int Option00;
  int Option01;
  int Option02;
  int Option03;
  int Option04;
  int Option05;
  int Option06;
  int Option07;
  int Option08;
  int Option09;
  int Option0a;
  int Option0b;
  int Option0c;
  int Option0d;
  int Option0e;
  int Option0f;
  int Option10;
  int Option11;
  int Option12;
  int Option13;
  int Option14;
  int Option15;
  int Option16;
  int Option17;
  int Option18;
  int Option19;
  int Option1a;
  int Option1b;
  int Option1c;
  int Option1d;
  int Option1e;
  int Option1f;
  int Option20;
  int Option21;
  int Option22;
  int Option23;
  int Option24;
  int Option25;
  int Option26;
  int Option27;
  int Option28;
  int Option29;
  int Option2a;
  int Option2b;
  int Option2c;
  int Option2d;
  int Option2e;
  int Option2f;
  int Option30;
  int Option31;
  int Option32;
  int Option33;
  int Option34;
  int Option35;
  int Option36;
  int Option37;
  int Option38;
  int Option39;
  int Option3a;
  int Option3b;
  int Option3c;
  int Option3d;
  int Option3e;
  int Option3f;
  int Option40;
  int Option41;
  int Option42;
  int Option43;
  int Option44;
  int Option45;
  int Option46;
  int Option47;
  int Option48;
  int Option49;
  int Option4a;
  int Option4b;
  int Option4c;
  int Option4d;
  int Option4e;
  int Option4f;
  int Option50;
  int Option51;
  int Option52;
  int Option53;
  int Option54;
  int Option55;
  int Option56;
  int Option57;
  int Option58;
  int Option59;
  int Option5a;
  int Option5b;
  int Option5c;
  int Option5d;
  int Option5e;
  int Option5f;
  int Option60;
  int Option61;
  int Option62;
  int Option63;
  int Option64;
  int Option65;
  int Option66;
  int Option67;
  int Option68;
  int Option69;
  int Option6a;
  int Option6b;
  int Option6c;
  int Option6d;
  int Option6e;
  int Option6f;
  int Option70;
  int Option71;
  int Option72;
  int Option73;
  int Option74;
  int Option75;
  int Option76;
  int Option77;
  int Option78;
  int Option79;
  int Option7a;
  int Option7b;
  int Option7c;
  int Option7d;
  int Option7e;
  int Option7f;
  int Control00;
  int Control01;
  int Control02;
  int Control03;
  int Control04;
  int Control05;
  int Control06;
  int Control07;
  int Control08;
  int Control09;
  int Control0a;
  int Control0b;
  int Control0c;
  int Control0d;
  int Control0e;
  int Control0f;
  int Control10;
  int Control11;
  int Control12;
  int Control13;
  int Control14;
  int Control15;
  int Control16;
  int Control17;
  int Control18;
  int Control19;
  int Control1a;
  int Control1b;
  int Control1c;
  int ontrol22;
  int Control23;
  int Control24;
  int Control25;
  int Control26;
  int Control27;
  int Control28;
  int Control29;
  int Control2a;
  int Control2b;
  int Control2c;
  int Control2d;
  int Control2e;
  int Control2f;
  int Control30;
  int Control31;
  int Control32;
  int Control33;
  int Control34;
  int Control35;
  int Control36;
  int Control37;
  int Control38;
  int Control39;
  int Control3a;
  int Control3b;
  int Control3c;
  int Control3d;
  int Control3e;
  int Control3f;
  int Control40;
  int Control41;
  int Control42;
  int Control43;
  int Control44;
  int Control45;
  int Control46;
  int Control47;
  int Control48;
  int Control49;
  int Control4a;
  int Control4b;
  int Control4c;
  int Control4d;
  int Control4e;
  int Control4f;
  int Control50;
  int Control51;
  int Control52;
  int Control53;
  int Control54;
  int Control55;
  int Control56;
  int Control57;
  int Control58;
  int Control59;
  int Control5a;
  int Control5b;
  int Control5c;
  int Control5d;
  int Control5e;
  int Control5f;
}DetectorInitParamets;





typedef struct {
    ULONG rStatus;
    BYTE  rData[128];
  } tRecvAuxPacket;
  ////// acquisition //////
  // acquisition flags
  #define cVDACQ_FDark         0       // acquire Dark frame (dark otherwise)
  #define cVDACQ_FBright       1       // acquire bright frame (dark otherwise)
  #define cVDACQ_FTestPat      2       // acquire test pattern  /TFT only
  #define cVDACQ_FRecentFrame  4       // retrieve recent frame /TFT only
  #define cVDACQ_FBrightIgn   (1<<31)  // ignore state of the bit 'Bright' /TFT only
  //  #define cVDACQ_FI_Bright61  (1<<31)  // 'bright61' /USB only
  // acquisition event types
  #define cVDACQ_ETTrace			0
  #define cVDACQ_ETTraceT			1
  #define cVDACQ_ETErr				2
  #define cVDACQ_ETLastErr			3
  #define cVDACQ_ETWSAErr			4
  // acquisition events
  #define cVDACQ_EAbort				1
  #define cVDACQ_EClose				2
  #define cVDACQ_EDetectorStatus	7
  #define cVDACQ_ECapture			20
  #define cVDACQ_EATReady			14
  #define cVDACQ_ECapturePerc		21
  #define cVDACQ_ECaptureRecv		22
  #define cVDACQ_EAT_Preparation	26

  // acquisition feedback record   
  typedef struct {
    int    rFlags,            // combination of cVDACQ_Fxxxx
           rType,             // cVDACQ_ETxxx
           rEvent,            // cVDACQ_Exxx
           rSocket;           // 0:no socket relation; otherwise socket's ID >0 
    TCHAR  rMsg[256];         // message (trace, wrn, err)
    int    rFrameWidth,       // full frame width
           rFrameHeight;      // full frame height
    short *rFrameBuffer;      // user supplied frame buffer "AFrameBuffer"
    int    rCaptureRows,      // # of received rows
           rCapturePercent;   // received data in percents
    void  *rUserCallBackProc, // user supplied "ACallBackProc"
          *rUserParam;        // user supplied "AUserParam"
    int    rAborted;          // 1: VDACQ_Abort -1:internally
    void  *rPacketData;       // pointer to received packet; usually it is nil  
  } tVDACQ_CallBackRec;
    // socket's id #1: control-write UDP
    //             #2: control-read UDP
    //             #3: data TCP socket
    // When callback is triggered by received message from detector it has non-nil field
    // PacketData. Normally an OEM application does not check received packets directly.
    
  // callback procedure  
  typedef void (_stdcall *tVDACQ_CallBackProc)( tVDACQ_CallBackRec* );

  ////// calibration //////
  // calibration flags
  #define cVDC_FCalOffs      1
  #define cVDC_FCalGain      2
  #define cVDC_FANoisRem     4
  #define cVDC_FBadPixMap    8
  #define cVDC_FDespeckle   16
  #define cVDC_FAVSmooth    32
  #define cVDC_FChkHWDark   (1<<30)  // check if recennt dark-frame is auto-calibrated
  #define cVDC_FTempDirIP   (1<<31)  // save temporary images in default image direcotory
  // calibration event types
  #define cVDC_ETTrace       0
  #define cVDC_ETTraceT      1
  #define cVDC_ETErr         2
  // calibration events
  #define cVDC_EAbort        1
  #define cVDC_EClose        2
  #define cVDC_ECalib        20
  #define cVDC_ECalibPerc    21
  // calibration feedback record   
  typedef struct {
    int    rFlags,            // combination of cVDC_Fxxxx
           rType,             // cVDC_ETxxx
           rEvent;            // cVDC_Exxx
    TCHAR  rMsg[256];         // message (trace, wrn, err)
    int    rFrameWidth,       // full frame width
           rFrameHeight,      // full frame height
           rStoredWidth,      // stored image width
           rStoredHeight,     // stored image height
           rCalibPercent;     // processed data in %
    short *rFrameBuffer,      // received frame data
          *rImageBuffer;
    void  *rUserCallBackProc, // user supplied "ACallBackProc"
          *rUserParam;        // user supplied "AUserParam"
    int    rAborted;          // 1: set by VDC_Abort; -1:internally
  } tVDC_CallBackRec;

  typedef struct {         // Custom information record
	  int  rNumReceivedRows; // acquisition uses this field to print messages
	  int   rComplete;        // indicates that caller terminates (by some reason) and expects "close"
	  // the possible reasons are: 'complete', 'error' or 'abort'
	  int rCalMode;
	  CWnd *rhWnd;
  } tCallBackRec;

  // callback procedure  
  typedef void (_stdcall *tVDC_CallBackProc)( tVDC_CallBackRec* );

  typedef struct {
    int  rImgCutLeft,          // cut left after rotation & flip 
         rImgCutTop,           // cut top after rotation & flip
         rImgCutRight,         // cut right after rotation & flip
         rImgCutBottom,        // cut bottom after rotation & flip
         rRotation,            // 0:none, 1:90 deg CCW, 2:90 deg CW
         rFlip;                // bit #0: flip horz, bit #1: flip vert 
  } tVDC_ImgCut;

  ///// image process /////
  // img process flags
  #define cVDIP_FDespeckle    1
  #define cVDIP_FAVSmooth     2
  #define cVDIP_F3PCurve      4
  #define cVDIP_FUSMask       8
  // img process event types
  #define cVDIP_ETTrace       0
  #define cVDIP_ETTraceT      1
  #define cVDIP_ETErr         2
  // img process events
  #define cVDIP_EAbort        1
  #define cVDIP_EClose        2
  #define cVDIP_EEnh          20
  #define cVDIP_EEnhPerc      21
  // img process feedback record   
  typedef struct {
    int    rFlags;            // combination of cVDC_Fxxxx
    int      rType;             // cVDC_ETxxx
    int       rEvent;            // cVDC_Exxx
    TCHAR  rMsg[256];         // message (trace, wrn, err)
    int    rStoredWidth;      // stored image width
    int       rStoredHeight;     // stored image height
    int       rEnhPercent;       // processed data in %
    int       rModeNumber;       // img process mode
    short *rImageBuffer;      // image buffer
    void  *rUserCallBackProc; // user supplied "ACallBackProc"
    void      *rUserParam;        // user supplied "AUserParam"
    int    rAborted;          // 1: set by VDIP_Abort; -1:internally
  } tVDIP_CallBackRec;
  // callback procedure  
  typedef void (_stdcall *tVDIP_CallBackProc)( tVDIP_CallBackRec* );
     typedef struct {
      HINSTANCE rHDLL; // the library's handle
      // general UI and logs //
      BOOL (_stdcall *rVD_Dialog)( HWND ); // it has additional parameter: owner's window
      void (_stdcall *rVD_LogOpen)( const TCHAR*, int, const TCHAR* );
      void (_stdcall *rVD_LogMsg)( const TCHAR* );
      void (_stdcall *rVD_LogClose)(void);
      void (_stdcall *rVD_LogFlush)(void);
      void (_stdcall *rVD_GetHomeDirectory)( TCHAR* );
      void (_stdcall *rVD_IniProfSetSection)( const TCHAR* );
      const TCHAR* (_stdcall *rVD_IniProfGetStr)( const TCHAR*, const TCHAR* );
      ////// acquisition //////
      void (_stdcall *rVDACQ_SetFrameDim)( int, int );
      void (_stdcall *rVDACQ_GetFrameDim)( int*, int* );
      tVDACQ_CallBackRec* (_stdcall *rVDACQ_Connect)( int, tVDACQ_CallBackProc, void*, short*, int );
      BOOL (_stdcall *rVDACQ_SendCommand)( tVDACQ_CallBackRec*, DWORD );
      BOOL (_stdcall *rVDACQ_SendCommandParam)( tVDACQ_CallBackRec*, DWORD, const void*, int );
      BOOL (_stdcall *rVDACQ_StartFrame)( tVDACQ_CallBackRec* );
	  BOOL (_stdcall *rVDACQ_GetFrame)( tVDACQ_CallBackRec*, int ); 	  //// 1417P Debug Mode ////
      BOOL (_stdcall *rVDACQ_VendorCommand)( tVDACQ_CallBackRec*, int, int, int );
	  BOOL (_stdcall *rVDDBG_SendCommand)( int , int , int , int );
      BOOL (_stdcall *rVDACQ_Abort)( tVDACQ_CallBackRec* );
      void (_stdcall *rVDACQ_Close)( tVDACQ_CallBackRec* );
      void (_stdcall *rVDACQ_GetDetectorIPAddr)( TCHAR* );
      void (_stdcall *rVDACQ_SetDetectorIPAddr)( const TCHAR* );
      BOOL (_stdcall *rVDACQ_GetDetectorInfo)( int );
      ////// calibration //////
      BOOL (_stdcall *rVDC_GetImageDim)( int*, int* );
      short* (_stdcall *rVDC_CutImage)( const short*, short* );
      tVDC_CallBackRec* (_stdcall *rVDC_Process)( int, tVDC_CallBackProc, void*, short* );
      BOOL (_stdcall *rVDC_Abort)( tVDC_CallBackRec* );
      void (_stdcall *rVDC_Close)( tVDC_CallBackRec* );
      void (_stdcall *rVDC_GetCalibrationDirectory)( TCHAR* );
      BOOL (_stdcall *rVDC_SetCalibrationDirectory)( const TCHAR* );
      void (_stdcall *rVDC_GetImgCutParams)( tVDC_ImgCut* );
      void (_stdcall *rVDC_SetImgCutParams)( const tVDC_ImgCut* );
	  BOOL (_stdcall *rVDC_GenerateAuto)( );
	  int (_stdcall *rVDC_GetDark)( tVDACQ_CallBackProc, tVDC_CallBackProc );
	  int (_stdcall *rVDC_GetBright)( tVDACQ_CallBackProc, tVDC_CallBackProc );
	  BOOL (_stdcall *rVDC_GenerateBright)( );
      ///// image process /////
      tVDIP_CallBackRec* (_stdcall *rVDIP_Process)( int, tVDIP_CallBackProc, void*, short*, int );
      BOOL (_stdcall *rVDIP_Abort)( tVDIP_CallBackRec* );
      void (_stdcall *rVDIP_Close)( tVDIP_CallBackRec* );
      //// user interface /////
      void (_stdcall *rVD_Set_Acquisition)( int, short*, int, tVDACQ_CallBackProc, void* );
      void (_stdcall *rVD_Set_Calibration)( int, short*, short*, tVDC_CallBackProc, void* );
	  void (_stdcall *rVD_Get_Calibration)( int &, int &, int &, int & );
      void (_stdcall *rVD_Set_ImgProcess)( int, short*, int, tVDIP_CallBackProc, void* );
      int (_stdcall *rVD_GetImage)( HWND );
      int (_stdcall *rVD_GetImageSP)( HWND, int, int, int, int );
      void (_stdcall *rVD_GetImageCancel)(void);
	  //// Auxiliary
	  BOOL (_stdcall *rVD_ConnectRestore)(void);
    } tVADAV_InterfaceRec; 




 #endif