#include "RayanceFunctions.h"
#include "Rayance_definitions.h"
#include <afxwin.h>
#include  <string>
using namespace std;
 char _BasePath[255];
 tVADAV_InterfaceRec AIntfRec;
 tCallBackRec m_CBUParam;
 tVDACQ_CallBackRec* ACQ_Callback;
 HWND _WHandle=NULL;
 short* FrameBuffer=NULL;
 int ImgW;
 int ImgH;
 size_t FrameSize;
 char DetectorIP[255];
 FILE* TraceDabinchy;
 CRITICAL_SECTION Section;
/**************************************************************************************/
  static bool iGetPA( HINSTANCE AHDll, const TCHAR *AFuncName, FARPROC *AFuncRef )
{
    *AFuncRef = GetProcAddress( AHDll, AFuncName );
  if (*AFuncRef) 
    return TRUE;
  return FALSE;
}
/**************************************************************************************/
 static void _stdcall  CallBackProc_Acquisition( tVDACQ_CallBackRec* AR )
{
  tRecvAuxPacket *qPacket = NULL;
  tCallBackRec *qCB = (tCallBackRec*)AR->rUserParam;
  switch (AR->rType) {

  case cVDACQ_ETErr:      // -- error in the caller's program code
  case cVDACQ_ETLastErr:  // -- error from Windows API
  case cVDACQ_ETWSAErr:
  {
	  qCB->rhWnd->SendMessage(VADAV_MessageCode_ERR, AR->rType, (long)AR->rMsg);
	  fprintf(TraceDabinchy, "Callback EV:%i Message:%s\n", AR->rType, AR->rMsg);
	  fflush(TraceDabinchy);
	  AR->rUserCallBackProc = NULL;
	  break;
  }
  case cVDACQ_ETTrace:    // -- information message
  case cVDACQ_ETTraceT:   // -- information message with time-stamp in the text
	  switch (AR->rEvent) {
	  case cVDACQ_ECaptureRecv:           // - receive event
		  if (qCB->rNumReceivedRows != AR->rCaptureRows) {
			  qCB->rNumReceivedRows = AR->rCaptureRows;
			 
			  if (!(qCB->rNumReceivedRows % 100)) {
			  }
		  }
		  break;


	  case cVDACQ_EATReady:
		  {
			  qCB->rhWnd->SendMessage(VADAV_MessageCode_TriggerReady,(long)AR->rMsg);
			  break;
		  }

	  case cVDACQ_EDetectorStatus:
		  {
			  qCB->rhWnd->SendMessage(VADAV_MessageCode_DetectorStatus,(long)AR->rMsg);
			  break;
		  }
     
	  case cVDACQ_ECapturePerc:
		  qCB->rhWnd->SendMessage(VADAV_MessageCode_RECEIVED_PERCENTS, AR->rCapturePercent);
		  if (AR->rCapturePercent > 100) {
			  AR->rUserCallBackProc = NULL;
			  qCB->rComplete = 1;     
			  qCB->rhWnd->SendMessage(VADAV_MessageCode_RECEIVED_PERCENTS_COMPLETE, AR->rCapturePercent);
			
		  }
		  break;

	  default:
		  if (AR->rMsg[0])
		  {
			  if (strlen(AR->rMsg) > 0)
			  {
				  qCB->rhWnd->SendMessage(VADAV_MessageCode_TRACE, (long)AR->rMsg);
				  fprintf(TraceDabinchy, "Callback EV:%i Message:%s\n", AR->rType, AR->rMsg);
				  fflush(TraceDabinchy);
			  }

		  }
	  }

	  break;
  }

  if (AR) {
     qPacket = (tRecvAuxPacket*)AR->rPacketData;
    if (!qPacket) // ifnore all error and information messages here
      return;
  }; // if AR
  if(qPacket->rStatus == 0x00011060)
  {
	  qCB->rhWnd->SendMessage(VADAV_MessageCode_AQCOUNT, (long)qPacket->rStatus, (long)qPacket->rData);
  }

  if(qPacket->rStatus >= 0x00011010 && qPacket->rStatus <= 0x00011017)
  {
	 qCB->rhWnd->SendMessage(VADAV_MessageCode_VERSIONS, (long)qPacket->rStatus, (long)qPacket->rData);
  }

  }
 /**************************************************************************************/
 bool VADAV_MapDLL( const char *ADllFName)
 {

 bool REsult = true;
 InitializeCriticalSection(&Section);
 EnterCriticalSection(&Section);
 SetDllDirectory(ADllFName);
 char* LibName = "VADAV.dll";
 TraceDabinchy = fopen("DavincyTrace.log", "w+");
 memset(&AIntfRec,0,sizeof(AIntfRec));
 AIntfRec.rHDLL = LoadLibrary(LibName);
 if( AIntfRec.rHDLL!=NULL)
 {
  iGetPA( AIntfRec.rHDLL, _T("VD_DialogH"), (FARPROC*)&AIntfRec.rVD_Dialog );
  iGetPA( AIntfRec.rHDLL, _T("VD_LogOpen"), (FARPROC*)&AIntfRec.rVD_LogOpen );
  iGetPA( AIntfRec.rHDLL, _T("VD_LogMsg"), (FARPROC*)&AIntfRec.rVD_LogMsg );
  iGetPA( AIntfRec.rHDLL, _T("VD_LogClose"), (FARPROC*)&AIntfRec.rVD_LogClose );
  iGetPA( AIntfRec.rHDLL, _T("VD_LogFlush"), (FARPROC*)&AIntfRec.rVD_LogFlush );
  iGetPA( AIntfRec.rHDLL, _T("VD_GetHomeDirectory"), (FARPROC*)&AIntfRec.rVD_GetHomeDirectory );
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_GetDetectorIPAddr"), (FARPROC*)&AIntfRec.rVDACQ_GetDetectorIPAddr);
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_SetDetectorIPAddr"), (FARPROC*)&AIntfRec.rVDACQ_SetDetectorIPAddr);
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_SetFrameDim"), (FARPROC*)&AIntfRec.rVDACQ_SetFrameDim );
  
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_GetFrameDim"), (FARPROC*)&AIntfRec.rVDACQ_GetFrameDim );
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_Connect"), (FARPROC*)&AIntfRec.rVDACQ_Connect );
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_StartFrame"), (FARPROC*)&AIntfRec.rVDACQ_StartFrame );
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_GetFrame"), (FARPROC*)&AIntfRec.rVDACQ_GetFrame );

  iGetPA( AIntfRec.rHDLL, _T("VDACQ_GetDetectorInfo"), (FARPROC*)&AIntfRec.rVDACQ_GetDetectorInfo );
  iGetPA(AIntfRec.rHDLL, _T("VDACQ_SendCommand"), (FARPROC*)&AIntfRec.rVDACQ_SendCommand);
  iGetPA(AIntfRec.rHDLL, _T("VDACQ_SendCommandParam"), (FARPROC*)&AIntfRec.rVDACQ_SendCommandParam);
	 

  iGetPA( AIntfRec.rHDLL, _T("VDACQ_Abort"), (FARPROC*)&AIntfRec.rVDACQ_Abort );
  iGetPA( AIntfRec.rHDLL, _T("VDACQ_Close"), (FARPROC*)&AIntfRec.rVDACQ_Close );
  fprintf(TraceDabinchy, "Init Succces\n");
  fflush(TraceDabinchy);
 }
 else
 {
	 fprintf(TraceDabinchy, "Init Fail\n");
	 fflush(TraceDabinchy);

 REsult = false;

 }
 LeaveCriticalSection(&Section);
 return REsult;
 }

 /**************************************************************************************/
 bool VADAV_ReleaseDLL( void)
 {
	 
	 bool Result = false;
	 if (AIntfRec.rHDLL== NULL)
	 {
		 return false;
	 }

	 DeleteCriticalSection(&Section);
	 if (!FreeLibrary(AIntfRec.rHDLL))
	 {
		 
		
		 AIntfRec.rHDLL = NULL;
		 Result = true;
	 }
	 return Result;
  }
 /**************************************************************************************/
 bool VADAV_GetFrameDim(int* Width,int* Height)
 {
	 if (AIntfRec.rHDLL == NULL)
	 {
		 return false;
	
	 }

    AIntfRec.rVDACQ_GetFrameDim(Width,Height);
  return  (bool)*Width;
 }
 /**************************************************************************************/
 bool VADAV_Connect(int typeFrame,int ConfigID)
 {


 bool Result = true;
 if (AIntfRec.rHDLL == NULL)
 {
	 return false;
 }

 EnterCriticalSection(&Section);
 ACQ_Callback = NULL;

 memset(&m_CBUParam,0,sizeof(m_CBUParam));
 m_CBUParam.rhWnd = NULL;
 AIntfRec.rVDACQ_GetFrameDim(&ImgW,&ImgH);
 FrameBuffer = new short[ImgW * ImgH];
 FrameSize = ImgH * ImgW * 2;
 m_CBUParam.rhWnd =  CWnd::FromHandle(_WHandle);
 m_CBUParam.rCalMode = 0;
 m_CBUParam.rNumReceivedRows = -1;
 m_CBUParam.rComplete = 0;
 m_CBUParam.rCalMode = typeFrame;
 

 ACQ_Callback =  AIntfRec.rVDACQ_Connect(typeFrame,CallBackProc_Acquisition,&m_CBUParam,FrameBuffer,ConfigID);
 LeaveCriticalSection(&Section);
 if(ACQ_Callback == NULL)
 {
    Result = false;
    VADAV_CloseACQ();
	fprintf(TraceDabinchy, "Conect Fail\n");
	fflush(TraceDabinchy);
 }
    return Result;
 }
/**************************************************************************************/
 bool VADAV_StartFrame(void)
 {
	 if (AIntfRec.rHDLL == NULL)
	 {
		 return false;
	 }

	 if (ACQ_Callback == NULL)
	 {
		 return false;
	 }

	 EnterCriticalSection(&Section);
	 bool Flag = AIntfRec.rVDACQ_StartFrame(ACQ_Callback);
	 LeaveCriticalSection(&Section);
   return  Flag;
 }
 /**************************************************************************************/
 bool VADAV_CancelFrame(void)
 {
	 if (AIntfRec.rHDLL == NULL)
	 {
		 return false;
	 }

	 if (ACQ_Callback==NULL)
	 {
		 return false;
	 }
	 EnterCriticalSection(&Section);
	 bool Flag = AIntfRec.rVDACQ_Abort(ACQ_Callback);
	 LeaveCriticalSection(&Section);
   return  Flag;
 }
 /**************************************************************************************/
 bool VADAV_ShowDavinchyDialog(void)
 {
	 if (AIntfRec.rHDLL == NULL)
	 {
		 return false;
	 }
	 EnterCriticalSection(&Section);
	 bool Result = AIntfRec.rVD_Dialog(NULL);
	 LeaveCriticalSection(&Section);
  return  Result;
 }
 /**************************************************************************************/
 void VADAV_SetWindowsHandle(void* WindowHandle)
 {
     _WHandle = (HWND)(WindowHandle);
 }
 /**************************************************************************************/
 bool VADAV_CloseACQ(void)
 {
	 EnterCriticalSection(&Section);
	 if (ACQ_Callback)
	 {
	
		 AIntfRec.rVDACQ_Close(ACQ_Callback);
		
		 ACQ_Callback = NULL;
	 }
  
	if (FrameBuffer)
	{
		delete[] FrameBuffer;
		FrameBuffer = NULL;
	
	}
     LeaveCriticalSection(&Section);
    return false;
 };
 /**************************************************************************************/
  bool VADAV_StartBrightFrame(void)
  {

	  bool Result = false;
	VADAV_CloseACQ();
  if(VADAV_Connect(cVDACQ_FBright,1))
  {
	  if (VADAV_StartFrame())
	  {
		  Result = true;
	  } 

  }
  return Result;

  }
  /**************************************************************************************/
  bool VADAV_StartDarkFrame(void)
  {
	  bool Result = false;
	  VADAV_CloseACQ();
  if(VADAV_Connect(cVDACQ_FDark,1))
  {
	  if (VADAV_StartFrame())
	  {
		  Result = true;
	  }
  }
  
  return Result;

  }
  /**************************************************************************************/
  short* VADAV_GetFrameBufferData(size_t* BufferSize,size_t* FrameW,size_t* FrameH)
  {
    *BufferSize = FrameSize;
    *FrameH = ImgH;
    *FrameW = ImgW;
    return FrameBuffer;
  }
  /**************************************************************************************/
  char * VADAV_GetIP(void)
  {

	  AIntfRec.rVDACQ_GetDetectorIPAddr(DetectorIP);
	  return DetectorIP;
  }
  /**************************************************************************************/
  bool VADAV_GetFirmwareVersions(void)
  {
	  bool Result = true;
	  if (VADAV_ConectForAUX())
	  {
		  for(int i = 0;i<=VADAV_Detector_VERSIONS_VER_BOARD;i++)
		  {
			  AIntfRec.rVDACQ_GetDetectorInfo(i);
		  }
	  }
	
	  return  Result;
  }
  /**************************************************************************************/
  bool VADAV_GetACQCount(void)
  {
	   bool Result = true;
	  if (VADAV_ConectForAUX())
	  {
		  AIntfRec.rVDACQ_SendCommand(ACQ_Callback,0x10060);
		
	  }
	
	  return  Result;

  }
  /**************************************************************************************/
  bool VADAV_Shutdown(void)
  {

	  bool Result = true;
	  if (VADAV_ConectForAUX())
	  {
		  AIntfRec.rVDACQ_SendCommand(ACQ_Callback, 0x11);
		 
	  }
	 
	  return  Result;

  }
	/**************************************************************************************/
  bool VADAV_Reboot(void)
  {
	  bool Result = true;
	  if (VADAV_ConectForAUX())
	  {
		  AIntfRec.rVDACQ_SendCommand(ACQ_Callback, 0x10040);
		 
	  }
	 
	  return  Result;

  }
  /**************************************************************************************/
  bool VADAV_ConectForAUX(void)
  {
	bool Result = true;
 if (AIntfRec.rHDLL == NULL)
 {
	 return false;
 }
 EnterCriticalSection(&Section);
 memset(&m_CBUParam,0,sizeof(m_CBUParam));
 m_CBUParam.rhWnd =  CWnd::FromHandle(_WHandle);
 m_CBUParam.rCalMode = 0;
 m_CBUParam.rNumReceivedRows = -1;
 m_CBUParam.rComplete = 0;
 m_CBUParam.rCalMode = cVDACQ_FDark;
 ACQ_Callback =  AIntfRec.rVDACQ_Connect(cVDACQ_FDark,CallBackProc_Acquisition,&m_CBUParam,NULL,-1);
 LeaveCriticalSection(&Section);
 if(ACQ_Callback == NULL)
 {
    Result = false;
    VADAV_CloseACQ();

 }

 return Result;
  }
  /**************************************************************************************/
   RAYANCE_WRAP_API   bool VADAV_ConnectForBright(void)
   {
	   EnterCriticalSection(&Section);
	   bool Flag = VADAV_Connect(cVDACQ_FBright,1);
	   LeaveCriticalSection(&Section);
	   return  Flag;
   }
   /**************************************************************************************/